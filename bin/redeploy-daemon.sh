#!/bin/sh
#
# Watch app-date/redeploy.json, if it appears, delete it and run the redeploy script
# run as redeploy-daemon.sh <path to deploy script>

while [ 1 -eq 1 ]
do
    if [ -f app-data/redeploy.json ]
    then
       echo `date`
       echo "Redeploy triggered"
       rm app-data/redeploy.json
       sh $1
	fi
	sleep 5
done

