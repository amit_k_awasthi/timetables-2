#!/usr/bin/env python
import argparse
import subprocess
import os

def list_html_files(template_dir):
    command = ["find", template_dir, "-type", "f", "-name", "*.html", "-print0"]
    filenames = subprocess.check_output(command).split("\x00")

    # Ignore empty strings (from splitting on last null)
    return [filename for filename in filenames if filename]

def strip_prefix(parent_dir, filename):
    assert filename.startswith(parent_dir), ("filename: '%s' should begin "
            "with: '%s'" % (filename, parent_dir))
    filename = filename[len(parent_dir):]
    if filename.startswith("/"):
        filename = filename[1:]
    return filename

def templates(template_dir):
    template_filenames = list_html_files(template_dir)
    template_names = [strip_prefix(template_dir, filename)
            for filename in template_filenames]

    return zip(template_filenames, template_names)

def is_template_referenced(project_root_dir, template_inclusion_name, out=None):
    cmd = ["fgrep", "-r", "--exclude", "*.pyc",
            template_inclusion_name, project_root_dir]
    status = subprocess.call(cmd, stdout=out, stderr=out)
    # 0 status indicates success, which indicates that references to the 
    # inclusion name were found in a file.
    return status == 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Crudely identifies "
            "unreferenced html templates by searching for references to the "
            "paths that would be used when including them in the project.")
    parser.add_argument("template_dir", help="The directory in which the app's "
            "templates are stored.")
    parser.add_argument("project_root", help="The root directory of the "
            "project. The files in this directory will be recursively searched "
            "for references to each template.")
    args = parser.parse_args()

    templates = templates(args.template_dir)
    with open(os.devnull, "w") as devnull:
        for filename, inclusionname in templates:
            if not is_template_referenced(args.project_root, inclusionname,
                    out=devnull):
                print filename
