define(["underscore", 
        "backbone",
        "assert",
        "backbone-utils"],
        function(_, Backbone, assert, backbone_utils) {

    var superclass = backbone_utils.superclass;
    var superproto = backbone_utils.superproto;
    
    // View Model Classes (As opposed to normal Models)
    
    /**
     * A ModelAttribute represents a single attribute of a model. It allows
     * watching the field
     */
    var ModelAttribute = function ModelAttribute(model, attrName, name) {
        _.bindAll(this, "triggerAttributeChange", "triggerError");
        
        if(!model instanceof Backbone.Model) {
            throw Error("model was not an instance of Backbone.Model: " +
                    model);
        }
        
        model.on("error", this.triggerError);
        model.on("change:" + attrName, this.triggerAttributeChange);
        this.model = model;
        this.attrName = attrName;
        this.name = name || null;
    };
    _.extend(ModelAttribute.prototype, Backbone.Events, {
        
        triggerAttributeChange: function() {
            this.trigger("change", this.get(), this.getModel());
        },
        
        triggerError: function(error) {
            this.trigger("error", error, this.getModel());
        },
        
        /** Gets the value of the attribute. */
        get: function() {
            return this.model.get(this.attrName);
        },
        
        /** Sets the value of the attribute. */
        set: function(value, options) {
            return this.getModel().set(this.getAttributeName(), value, options);
        },
        
        /** Gets the name of the attribute being watched. */
        getAttributeName: function() {
            return this.attrName;
        },
        
        /** Gets the model being watched. */
        getModel: function() {
            return this.model;
        },
        
        getName: function() {
            return this.name;
        }
    });
    
    /**
     * Behaves as a ModelAttribute, except instead of watching the attribute of
     * the model passed in, it watches the attribute at the end of a chain of
     * attributes starting at the root model.
     */
    var ModelAttributePath = function ModelAttributePath(rootModel, attributes,
            name) {
        
        _.bindAll(this, "triggerAttributeChange", "triggerError", "_rebuild");
        if(!_.isArray(attributes) || attributes.length === 0) {
            throw new Error("attributes must be an array of len > 0. got: " +
                    attributes);
        }
        this.rootModel = rootModel;
        this.attributes = attributes;
        this.name = name || null;
        this._path = undefined;
        // Walk and watch the attribute chain
        this._rebuild();
    };
    _.extend(ModelAttributePath.prototype, Backbone.Events, {
        get: function get() {
            return _.last(this.path()).get();
        },
        
        set: function set() {
            var lastLink = _.last(this.path());
            return lastLink.set.apply(lastLink, arguments);
        },
        
        path: function path() {
            return this._path;
        },
        
        /** Gets the name of the attribute being watched. */
        getAttributeName: function() {
            return _.last(this.path()).getAttributeName();
        },
        
        /** Gets the model being watched. */
        getModel: function() {
            return _.last(this.path()).getModel();
        },
        
        getName: function() {
            return this.name;
        },
        
        triggerAttributeChange: function() {
            var pathEnd = _.last(this.path());
            this.trigger("change", pathEnd.get(), pathEnd.getModel());
        },

        triggerError: function(error) {
            this.trigger("error", error, _.last(this.path()).getModel());
        },
        
        /**
         * Walks up the attribute path, starting from root, constructing a 
         * ModelAttribute for each step in the path. The list of these 
         * ModelAttributes is returned.
         */
        _walkPath: function() {
            head = new ModelAttribute(this.rootModel, _.first(this.attributes));
            return _.reduce(_.tail(this.attributes), function(path, nextAttribute) {
                var last = _.last(path);
                var nextModel = last.get();
                if(nextModel) {
	                if(!(nextModel instanceof Backbone.Model)) {	                    
	                    console.error("Expected model but got:", nextModel, 
	                            "path", path);
	                    throw new Error("Expected model but got: " + nextModel);
	                }
	                path.push(new ModelAttribute(last.get(), nextAttribute));
	            }
                return path;
            }, [head]);
        },
        
        /**
         * Rebuilds the attribute path, setting up observations.
         */
        _rebuild: function() {
            // Unlink the old path
            if(this.path()) {
                _.each(_.initial(this.path()), 
                        function(step) { step.off("change", this); });
                var previousEnd = _.last(this.path());
                previousEnd.off("change", this.triggerAttributeChange);
                previousEnd.off("error", this.triggerError);
            }
            
            // Build the attribute path and listen for changes to it
            this._path = this._walkPath();
            _.each(_.initial(this.path()), function(step) { 
                step.on("change", this._rebuild);
            }, this);
            var newEnd = _.last(this.path());
            newEnd.on("change", this.triggerAttributeChange);
            newEnd.on("error", this.triggerError);
            
            // Notify listeners of a change
            this.triggerAttributeChange();
        }
    });
    ModelAttribute.isInstance = function isInstance(o) {
        return o instanceof ModelAttribute || o instanceof ModelAttributePath;
    };
    
    /**
     * Gets a watchable attribute representing the value found by repeatedly 
     * calling get(attribute) for each attribute provided, starting at model.
     * 
     * Returns a ModelAttribute* instance.
     */
    ModelAttribute.create = function create(
            /*[name], model, attribute [attribute...]*/) {
        var args = _.toArray(arguments);
        // parse args
        var name = undefined;
        if(_.isString(_.first(args))) {
            name = _.first(args);
            args = _.tail(args);
        }
        
        var model = _.first(args);
        var attributes = _.tail(args);
        
        if(!model instanceof Backbone.Model)
            throw new Error("model argument was not a Backbone Model: " +
                    model);
        if(attributes.length === 0)
            throw new Error("No attributes provided");
        if(!_.all(attributes, function(a){ return _.isString(a); }))
            throw new Error("Not all attributes were strings");
        
        if(attributes.length === 1)
            return new ModelAttribute(model, _.first(attributes), name);
        return new ModelAttributePath(model, attributes, name);
    }
    
    /**
     * A FieldAnnotation represents an object which calculates some incidental
     * information about a field.
     * 
     * Currently envisaged uses for Annotations are to validate field values,
     * and to check for duplicates on a remote server.
     * 
     *  Because of the need to hit a remote server, the default API is 
     *  asynchronous.
     */
    var FieldAnnotation = Backbone.Model.extend({
        constructor: function FieldAnnotation(){
            superclass(FieldAnnotation).apply(this, arguments);
        },
        
        defaults: {
            calculations: 0, // The number of ongoing async calculations
            message: "", // A truthy value if the annotation
            is_calculating: false, // true if any calculations are ongoing
        },
        
        initialize: function(attrs, modelAttribute) {
            assert(ModelAttribute.isInstance(modelAttribute), modelAttribute);
            _.bindAll(this, "runCalculation");
            this.watchedField = modelAttribute;
            this.watchedField.on("change", this.runCalculation);
        },
        
        runCalculation: function() {
            this.set({"calculations": this.get("calculations") + 1,
                "is_calculating": true});
            this._calculateAsync(this.watchedField);
        },
        
        // This should be overridden to implement async annotation calculation.
        // The default implementation asynchronously calls 
        // onCalculationComplete() immediately _.defer()
        _calculateAsync: function(field) {
            _.defer(_.bind(function() {
                this._calculateSync(field);
            }, this));
        },
        
        // The synchronous version of _calculate
        _calculateSync: function(field) {
            this.onCalculationComplete();
        },
        
        onCalculationComplete: function() {
            var calculationCount = this.get("calculations") - 1;
            this.set("calculations", calculationCount);
            if(calculationCount === 0) {
                this.trigger("calculated", arguments);
                this.set("is_calculating", false);
            }
        },
        
        getMessage: function() {
            return this.get("message") || "";
        },
        
        isCalculating: function() {
            return this.get("is_calculating");
        }
    });
    
    var ValidationFieldAnnotation = FieldAnnotation.extend({
        constructor: function ValidationFieldAnnotation(){
            superclass(ValidationFieldAnnotation).apply(this, arguments);
        },
        
        defaults: function() {
            return _.extend(_.clone(superproto(ValidationFieldAnnotation).defaults), {
                // Whether the Annotation has been made visible. Annotations are 
                // invisible by default.47
                is_visible: false,
                is_valid: true
            });
        },
        
        makeVisible: function() {
            this.set("is_visible", true);
        },
        
        isVisible: function() {
            return this.get("is_visible");
        },
        
        isValid: function() {
            return this.get("is_valid");
        }
    });
    
    var EmptynessFieldAnnotation = ValidationFieldAnnotation.extend({
        constructor: function EmptynessFieldAnnotation(){
            superclass(EmptynessFieldAnnotation).apply(this, arguments);
        },
        
        defaults: function(){
            return _.extend(_.clone(
                    superproto(EmptynessFieldAnnotation).defaults.call(this)), {
                message: "This field cannot be empty."
            });
        },
        
        _calculateSync: function(field) {
            var isValid = true;
            var message = "";
            if(field.get().length === 0)
                this.set({
                    is_valid: false});
            else
                this.set({
                    is_valid: true});
            
            this.onCalculationComplete();
        }
    });
    
    var UniquenessFieldAnnotation = ValidationFieldAnnotation.extend({
        constructor: function UniquenessFieldAnnotation(){
            superclass(UniquenessFieldAnnotation).apply(this, arguments);
        },
        
        defaults: function(){
            return _.extend(_.clone(superproto(UniquenessFieldAnnotation)
                    .defaults.call(this)), {
                message: "This value has already been used."
            });
        },
        
        initialize: function(attributes, url /*, modelAttributes */) {
            var modelAttributes = _.tail(arguments, 2);
            ValidationFieldAnnotation.prototype.initialize.call(this, {},
                    modelAttributes[0]);
            assert(_.isString(url), url);
            assert(_.all(modelAttributes, function(attr) {
                return ModelAttribute.isInstance(attr); }));
            
            this.url = url;
            this.modelAttributes = modelAttributes;
            
            // Listen for changes to any of our watched fields and recalculate
            _.each(this.modelAttributes, function(attr) {
                attr.off("change", this.runCalculation);
                attr.on("change", this.runCalculation);
            }, this);
            
            // wrap the http request initiating function with debounce() so that
            // it can only run at most every 333ms.
            this.triggerUniquenessHttpRequest = 
                _.debounce(this.triggerUniquenessHttpRequest, 333);
        },
        
        pickModelFields: function() {
            return _.chain(this.modelAttributes).map(function(attr){
                return [attr.getName(), attr.get()];
            }).toObject().value();
        },
        
        _calculateAsync: function() {
            this.triggerUniquenessHttpRequest();
        },
        
        triggerUniquenessHttpRequest: function() {
            // Because of our debouncing _calculate, not every calculation 
            // started will result in a call to onCalculationComplete, so we
            // need to make sure that calculations goes to 0 once we're done.
            // Our debouncing means that only one _calculate call can run at
            // any time, so this is safe.
            var calculationCount = this.get("calculations");
            if(calculationCount > 1)
                this.set("calculations", 1);
            
            // Abort any currently running check request so that we will only 
            // ever have one running at once.
            if(this.uniquenessCheckXHR) {
                this.uniquenessCheckXHR.abort();
            }
            
            var xhr = $.ajax(this.url, {
                data: this.pickModelFields(),
                dataType: "json"
            });
            
            xhr.done(_.bind(function(data){
                if(_.isEqual(this.pickModelFields(), data.fields)) {
                    this.set({is_valid: !data.exists});
                }
                
            }, this));
            
            xhr.fail(_.bind(function(xhr, textStatus, errorThrown){
                // on error default to saying the name is unique, unless we've
                // been aborted in which case another request will take over.
                if(textStatus !== "abort") {
                    this.set({is_valid: true});
                } 
            }, this));

            xhr.always(_.bind(function(xhr){
                if(this.xhr === xhr) {
                    this.xhr = null;
                }
                this.onCalculationComplete();
            }, this));
            
            this.uniquenessCheckXHR = xhr;
        }
    });
    
    /**
     * A collection which maintains View Models.
     */
    var ViewModelCollection = Backbone.Collection.extend({
        constructor: function ViewModelCollection(){
            superclass(ViewModelCollection).apply(this, arguments);
        },
        
        initialize: function() {
            _.bindAll(this, "recalculateSynchedStatus");
            this.on("reset add remove change:synched", 
                    this.recalculateSynchedStatus);
            this.recalculateSynchedStatus();
        },
        
        recalculateSynchedStatus: function() {
            var wasSynched = this.synched;
            this.synched = this.all(function(viewModel) {
                return viewModel.isSynched(); });
            
            // Notify listeners if our synched status has changed
            if(wasSynched !== this.synched) {
                this.trigger("change:all_synched", this.synched);
            }
        },
        
        add: function(models) {
            if(!_.isArray(models))
                models = [models];
            
            if(!_.all(models, ViewModelBase.isParentOf)) {
                throw new Error(
                        "Added non view model to a ViewModelCollection.");
            }
            
            return Backbone.Collection.prototype.add.apply(this, arguments);
        },
        
        isSynched: function() {
            return this.synched;
        }
    });
    
    var ViewModelBase = Backbone.Model.extend({
        constructor: function ViewModelBase(){
            superclass(ViewModelBase).apply(this, arguments);
        },
        
        defaults: {
            synched: false
        },
        
        initialize: function() {
            _.bindAll(this, "updateSynchedStatus", "onChanged");
            this.on("change", this.onChanged)
        },
        
        onChanged: function() {
            this.triggerSubtreeModified();
        },
        
        triggerSubtreeModified: function() {
            this.trigger.apply(this, 
                    ["subtree_modified"].concat(_.toArray(arguments)));
        },
        
        /**
         * Decides if this ViewModel is 'synched'. Override to add conditions.
         */
        areSynchedConditionsMet: function() {
            assert(false, "areSynchedConditionsMet() should be overridden in " +
                    this);
        },
        
        updateSynchedStatus: function() {
            this.set("synched", Boolean(this.areSynchedConditionsMet()));
        },
        
        isSynched: function() {
            return this.get("synched");
        }
    },
    {
        isParentOf: function(model) {
            return model instanceof ViewModelBase ||
                    model instanceof ViewModelCollection;
        }
    });
    
    
    /**
     * The parent class for ViewModels which contain nested ViewModels.
     */
    var ViewModel = ViewModelBase.extend({
        constructor: function ViewModel(){
            superclass(ViewModel).apply(this, arguments);
        },
        
        initialize: function() {
            _.bindAll(this, "onChildrenSynched", "onAddRemoveChildren",
                    "synchToModel");
            ViewModelBase.prototype.initialize.call(this);
            
            // Create a collection to hold all of the view models which we
            // maintain in our attributes.
            this.childViewModels = new ViewModelCollection();
            this.childViewModels.on("change:all_synched",
                    this.onChildrenSynched);
            this.childViewModels.on("subtree_modified", this.synchToModel)
            // Initialise it with any existing models.
            this.onAddRemoveChildren();
            this.on("change", this.onAddRemoveChildren);
            //this.on("change", this.synchToModel);
            this.childViewModels.on("change", this.synchToModel);
            this.onChildrenSynched(this.childViewModels.isSynched());
        },
        
        synchToModel: function() {
            // Override to implement synch functionality
        },
        
        /** Called when our collection of child view models has a synch change.*/
        onChildrenSynched: function(areChildrenSynched) {
            this.set("children_synched", areChildrenSynched);
            this.updateSynchedStatus();
        },
        
        /**
         * Decides if this ViewModel is 'synched'. Override to add more 
         * conditions.
         */
        areSynchedConditionsMet: function() {
            return this.get("children_synched");
        },
        
        onAddRemoveChildren: function() {
            // If only synched has changed then ignore the change event.
            var changed = this.changedAttributes();
            if(changed.length === 1 && "synched" in changed)
                return;
            
            // Find all view model instances attached to this model
            var viewModels = _(this.attributes).filter(function(value) {
                return value instanceof ViewModelBase;
            });
            
            this.childViewModels.reset(viewModels);
        }
    });
    
    /**
     * A view model which represents the state of an HTML <input> element.
     * It delegates changes to the <input> element to another (typically data)
     * model. 
     */
    var InputViewModel = ViewModelBase.extend({
        constructor: function InputViewModel(){
            superclass(InputViewModel).apply(this, arguments);
        },
        
        defaults: {
            value: null,
            normalised_value: null,
            delegatedAttribute: null,
            error: null,
        },
        
        initialize: function(attrs, normaliser) {
            superproto(InputViewModel).initialize.call(this);
            _.bindAll(this, "onAttributeChanged", "onAttributeValueChanged",
                    "onAttributeModelError", "onValueChanged", 
                    "onNormalisedValueChanged");
            var error = this.validate();
            assert(!error, error);
            assert(ModelAttribute.isInstance(this.delegatedAttribute()), 
                    this.delegatedAttribute());
            this.on("change:value", this.onValueChanged);
            this.on("change:normalised_value", this.onNormalisedValueChanged);
            this.on("change:delegatedAttribute", this.onAttributeChanged);
            this.onAttributeChanged();
            
            if(normaliser) {
                if(_.isFunction(normaliser))
                    this.normaliser = normaliser;
                else if(_.isArray(normaliser))
                    this.normaliser = _.compose.apply(null, normaliser);
                else
                    throw new Error("normaliser was not an array or function:" +
                            String(normaliser));
            }
        },
        
        /**
         * Gets a ModelAttribute which targets the "value" attribute of this 
         * model. The value attribute stores the denormalised raw user input.
         */
        valueAttribute: function() {
            if(!this.rawValueAttribute)
                this.rawValueAttribute = ModelAttribute.create(this, "value");
            return this.rawValueAttribute;
        },
        
        delegatedAttribute: function() {
            return this.get("delegatedAttribute");
        },
        
        delegatedValue: function() {
            return this.delegatedAttribute().get();
        },
        
        onAttributeChanged: function() {
            var previousAttribute = this.previous("delegatedAttribute");
            if(previousAttribute) {
                previousAttribute.off("change", this.onAttributeValueChanged);
                previousAttribute.off("error", this.onAttributeModelError);
            }
            
            this.delegatedAttribute().on(
                    "change", this.onAttributeValueChanged);
            this.delegatedAttribute().on("error", this.onAttributeModelError);
            this.onAttributeValueChanged(this.delegatedAttribute().get());
        },
        
        onAttributeValueChanged: function(value) {
            this.set("error", null);
            if(this.normalisedValue() !== value) {
                this.set("normalised_value", value);
            }
            this.updateSynchedStatus();
        },
        
        onValueChanged: function() {
            this.set("normalised_value", this.normalise(this.get("value")));
        },
        
        onNormalisedValueChanged: function() {
            // If our normalised value is updated independently of our value, 
            // update the value to reflect the normalised value.
            if(this.normalise(this.value()) !== this.normalisedValue()) {
                this.set("value", this.normalisedValue());
            }
            
            this.delegatedAttribute().set(this.get("normalised_value"));
            this.updateSynchedStatus();
        },
        
        normalise: function(value) {
            if(this.normaliser)
                return this.normaliser(value);
            return value;
        },
        
        value: function() {
            return this.get("value");
        },
        
        normalisedValue: function() {
            return this.get("normalised_value");
        },
        
        onAttributeModelError: function(error) {
            this.set("error", error);
        },
        
        validationError: function() {
            return this.get("error");
        },
        
        /**
         * Determine if this field is in sync with its model.
         * 
         * The default behaviour is to consider a view in sync if the model's
         * corresponding value is the same as our normalised value, and the
         * model is itself valid.
         */
        areSynchedConditionsMet: function() {
            var attr = this.delegatedAttribute();
            var value = this.normalisedValue();
            return (attr.get() === value);
        },
        
        validate: function() {
            if(!ModelAttribute.isInstance(this.delegatedAttribute())) {
                return '"delegatedAttribute" should be a ModelAttribute. was: ' + 
                        String(this.delegatedAttribute());
            }
        }
    });
    
    var SelectViewModel = InputViewModel.extend({
        constructor: function SelectViewModel() {
            superclass(SelectViewModel).apply(this, arguments);
        },
        
        defaults: function() {
            return _.extend(_.clone(superproto(SelectViewModel).defaults), {
                choices: new Backbone.Collection()
            });
        },
        
        initialize: function() {
            superproto(SelectViewModel).initialize.apply(this, arguments);
            if(!this.get("value") && this.get("choices").length > 0) {
                this.set("value", this.get("choices").models[0].cid);
            }
        },
        
        normalise: function(value) {
            return this.get("choices").getByCid(value);
        },
        
        onSelectionChanged: function(val) {
            this.set("value", val);
        },
        
        onChoicesChanged: function(callback) {
            this.get("choices").on("add remove change", callback);
        },
        
        /**
         * Gets a list of pairs representing the available select options' 
         * [val, text] values.
         */
        choices: function() {
            return this.get("choices").map(function(choice) {
                return [choice.cid, String(choice)];
            });
        }
    });
    
    /**
     * A wrapper to provide additional information for an InputViewModel.
     */
    var FormFieldViewModel = ViewModel.extend({
        constructor: function FormFieldViewModel(){
            superclass(FormFieldViewModel).apply(this, arguments);
        },
        
        defaults: {
            name: "",
            description: "",
            inputViewModel: null,
            annotations: null,
            annotations_calculated: true,
            annotations_valid: true
        },

        initialize: function(attrs) {
            _.bindAll(this, "onAnnotationUpdate");
            ViewModel.prototype.initialize.call(this);
            assert(this.get("inputViewModel") instanceof InputViewModel,
                    "inputViewModel must be an instance of InputViewModel.");
            
            var annotations = this.get("annotations");
            if(annotations instanceof Backbone.Collection)
                this.set("annotations", annotations);
            else if(_.isArray(annotations) || 
                    annotations instanceof Backbone.Model) {
                this.set("annotations", new Backbone.Collection(annotations));
            }
            
            annotations = this.get("annotations");
            annotations.on("change:is_calculating change:is_valid add remove",
                    this.onAnnotationUpdate);
        },

        /**
         * We consider ourselves to be synched if all children are synched and
         * our annotations are both calculated and valid (for validity 
         * annotations).
         */
        areSynchedConditionsMet: function() {
            return ViewModel.prototype.areSynchedConditionsMet.call(this) &&
                this.get("annotations_calculated") &&
                this.get("annotations_valid");
        },
        
        areAnyAnnotationsInvalid: function() {
            return this.get("annotations").any(function(a){
                return a instanceof ValidationFieldAnnotation &&
                        a.isVisible() &&
                        !(a.get("is_valid"));
            });
        },
        
        onValidityChange: function(callback) {
            this.get("annotations").on("change:is_valid", callback);
        },

        onAnnotationUpdate: function() {
            console.debug("onAnnotationUpdate");
            var allCalculated = this.get("annotations").all(
            function(annotation) {
                return !annotation.isCalculating();
            }, this);
            
            var allValid = this.get("annotations").all(function(annotation) {
                return (!annotation instanceof 
                        ValidationFieldAnnotation) || 
                        annotation.isValid();
            }, this);
            
            this.set("annotations_calculated", allCalculated);
            this.set("annotations_valid", allValid);
            this.updateSynchedStatus();
        }
    });
    
    var FormViewModel = ViewModel.extend({
        constructor: function FormViewModel(){
            superclass(FormViewModel).apply(this, arguments);
        },
        
        defaults: function() {
            return _.extend(_.clone(ViewModel.prototype.defaults), {
                // Whether a form submission HTTP req is ongoing
                is_submitting: false,
                viewModel: null,
                model: null
            });
        },
        
        initialize: function(fields, url, method, contentType) {
            ViewModel.prototype.initialize.call(this);
            assert(_.isString(url), "url should be a string but was: " +
                    String(url));
            this._submissionUrl = url;
            this._submissionMethod = method || "POST";
            this._submissionContentType = contentType || "application/json";
            this.get("model").on("validity_change", this.updateSynchedStatus);
        },
        
        areSynchedConditionsMet: function() {
            return ViewModel.prototype.areSynchedConditionsMet.call(this) &&
                this.get("model").isValid();
        },
        
        submissionUrl: function() {
            return this._submissionUrl;
        },
        
        submissionData: function() {
            if(!this.isSynched()){
                throw new Error("submissionData() called without this being " +
                        "in sync with model.");
            }
            return JSON.stringify(this.get("model").toJSON());
        },
        
        /** The HTTP method to use when submitting. */
        submissionMethod: function() {
            return this._submissionMethod;
        },
        
        submissionContentType: function() {
            return this._submissionContentType;
        },
        
        /** Submits the form using an HTTP request. */
        submit: function() {
            var xhr = $.ajax(this.submissionUrl(), {
                contentType: this.submissionContentType(),
                data: this.submissionData(),
                type: this.submissionMethod()
            });
            
            assert(!this.xhr, "submit() called with submit ongoing.", this.xhr);
            this.xhr = xhr;
            this.set("is_submitting", true);
            
            xhr.done(_.bind(function(data){
                this.trigger("submit:done");
            }, this));

            xhr.fail(_.bind(function(xhr, textStatus, errorThrown){
                this.trigger("submit:fail");
            }, this));
            
            xhr.always(_.bind(function(xhr){
                this.xhr = undefined;
                this.set("is_submitting", false);
            }, this));
        },
        
        onSave: function() {
            if(this.isSynched()) {
                this.submit();
            }
        },
        
        onCancel: function() {
            window.location = this.cancelRedirectPath || "/";
        }
    });
    
    return {
        ViewModelCollection: ViewModelCollection,
        ViewModelBase: ViewModelBase,
        InputViewModel: InputViewModel,
        FormFieldViewModel: FormFieldViewModel,
        FieldAnnotation: FieldAnnotation,
        EmptynessFieldAnnotation: EmptynessFieldAnnotation,
        FormViewModel: FormViewModel,
        ModelAttribute: ModelAttribute,
        UniquenessFieldAnnotation: UniquenessFieldAnnotation,
        ViewModelCollection: ViewModelCollection,
        ViewModel: ViewModel,
        SelectViewModel: SelectViewModel
    };
});