define(["jquery", "underscore", "backbone", 
        "text!templates/options.html"], 
        function($, _, Backbone, optionsTemplate) {
    "use strict";
    /**
     * The root of the view hierarchy. This view controls the entire page.
     * 
     * Chunks of functionality in the page are delegated to individual, more 
     * specialised views.
     */
    var AppView = Backbone.View.extend({
    
        events: function() {
            return {};
        },
        
        initialize: function() {
            this.events = _.extend({}, Backbone.Events);
        },
        
        render: function() {
        }
    
    });
    
    
    var SuperadminAppView = AppView.extend({
        
        events: function() {
            return _.extend(AppView.prototype.events.call(this), {
                
            });
        },
        
        initialize: function() {
            AppView.prototype.initialize.call(this);
        },
        
        render: function render() {
            AppView.prototype.render.call(this);
        }
    });
    
    /** 
     * Responsible for rendering a collection of Years into a <select>. 
     */
    var YearSelectView = Backbone.View.extend({
        
        tagName: "select",
        id: "year-select",
        template: _.template(optionsTemplate),
        
        events: {
            "change": "changed"
        },
        
        initialize: function() {
            _.bindAll(this, "render");
            this.yearsState = this.options.yearsState;
            this.yearsState.on("change:selectedYear", this.render);
            return this;
        },
        
        render: function() {
            var options = this.yearsState.years().map(function(year) {
                return {name: year.get("name"), value: year.get("start_year")};
            });
            this.$el.html(this.template({options: options}));
            // select the active year
            var year = this.yearsState.selectedYear();
            this.$el.val(year.get("start_year"));
            
            return this;
        },
        
        /** Called when the selected year changes. */
        changed: function() {
            var selectedYearStart = this.$el.val();
            this.yearsState.selectYear(parseInt(selectedYearStart, 10));
        }
    });
    
    /** 
     *  Responsible for rendering a collection of Organisations into a <select>.
     */
    var OrganisationSelectView = Backbone.View.extend({
        
        tagName: "select",
        id: "organisation-select",
        template: _.template(optionsTemplate),
        
        events: {
            "change": "changed"
        },
        
        initialize: function() {
            _.bindAll(this, "render");
            this.yearsState = this.options.yearsState;
            this.yearsState.on("change:selectedOrganisation", this.render);
        },
        
        render: function() {
            var options = this.yearsState.organisations().map(function(org) {
                return {name: org.get("name"), value: org.get("code")};
            });
            this.$el.html(this.template({options: options}));
            
            // select the active organisation, if any
            var selectedOrganisation = this.yearsState.selectedOrganisation();
            this.$el.val(selectedOrganisation.get("code"));
            
            return this;
        },
        
        /** Called when the selected organisation changes. */
        changed: function() {
            var selectedCode = this.$el.val();
            this.yearsState.selectOrganisation(selectedCode);
        }
    });
    
    return {
        AppView: AppView,
        SuperadminAppView: SuperadminAppView,
        YearSelectView: YearSelectView,
        OrganisationSelectView: OrganisationSelectView
    };
});