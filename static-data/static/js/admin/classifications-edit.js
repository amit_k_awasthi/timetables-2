define(["jquery", 
        "admin/views",
        "admin/validation",
        "admin/classification-selectors"
        ],
        function($, views, validation){
    "use strict";
    
    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/classifications/edit/";
    });

    
    // TODO: Refactor
    $(function(){
        $('#level-group-select').change(function(){
            $('.classificationgroup-block').show();
        });
    });

    validation.add(".validation_classification_name",function(element, feedback, ok_func, bad_func) {
           $.get("/administration/validate/classification_name/" +
                 encodeURIComponent($("#classification-name").val()) + "/" +
                 encodeURIComponent($("#classification-name-original").val()), ok_func).error(bad_func);
    });

    validation.bind($("#editor"));
    
    return {};
});
