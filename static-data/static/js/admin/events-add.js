
define(["jquery",
        "admin/views",
        "admin/editor-common"],
        function($, views){
    "use strict";
    
    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/events/add/";
    });
    
    
    return {};
});
