var JSON, console;
define(["jquery"],
        function($){
	"use strict";
	
	
	var checkSpec = function(uncheckedSpec, update) {
		if ( uncheckedSpec.timeslots ) {
			for ( var i = 0; i < uncheckedSpec.timeslots.length; i++ ) {
				var ts = uncheckedSpec.timeslots[i];
				var times = ts.timestamp;
				// make certaint the to date is after the from date.
				var fromh = times.fromh;
				var fromm = times.fromm;
				var toh = times.toh;
				var tom = times.tom;
				if ( !times.fromm ) {
					fromm = "00";
				}
				if ( !times.toh ) {
					toh = "00";
				}
				if ( !times.tom ) {
					tom = "00";
				}
				var f = parseInt(fromh)*60+parseInt(fromm);
				var t = parseInt(toh)*60+parseInt(tom);
				
				if ( t < f ) {
					t = f;
				}
				toh = Math.floor(t/60);
				tom = Math.floor((t%60)/5)*5;
				// pad and segment of 5m intervals
				if ( toh < 10 ) toh = '0'+toh;
				else toh = ''+toh;
				if ( tom < 10 ) tom = '0'+tom;
				else tom = ''+tom;
				// only reset what was specified.
				if ( update || times.fromm ) {
					times.fromm = fromm;
				}
				if ( update || times.toh ) {
					times.toh = toh;
				}
				if ( update || times.tom ) {
					times.tom = tom;
				}
			}
		}
		return uncheckedSpec;

	}
	/**
	 * convert a form into a spec
	 */
	var dateSpecFromForm = function(formSelector) {
		var days = "";
		$(formSelector).find("[name='day']").each(function(){
			if ($(this).is(":checked") ) {
				days = days + $(this).val();
			}
		});
		var dayOrder = [ 'M','Tu','W','Th','F','Sa','Su'];
		var orderedDays = "";
		for ( var i = 0; i < dayOrder.length; i++ ) {
			if ( days.indexOf(dayOrder[i]) >= 0  ) {
				orderedDays = orderedDays + dayOrder[i];
			}
		}
		var fromHour = $(formSelector).find("[name='from-hour']").val();
		var fromMin  = $(formSelector).find("[name='from-minute']").val();
		var toHour = $(formSelector).find("[name='to-hour']").val();
		var toMin = $(formSelector).find("[name='to-minute']").val();
		

		var reptype = $(formSelector).find("input:checked[name='date-range-type']").val();
		var nrepetitions = parseInt($(formSelector).find("[name='repetitions']").val());
		var weeksfrom = parseInt($(formSelector).find("[name='weeks-from']").val());
		var weeksto = parseInt($(formSelector).find("[name='weeks-to']").val());
		var term = $(formSelector).find("[name='term']").val();
		var frequency = $(formSelector).find("input:checked[name='frequency']").val();

		
		var datespec = {};
		if ( reptype === "repetitions" ) {
			if (nrepetitions < 2) {
				datespec.weeksingle = "1";
			} else if ( frequency === "weekly") {
				datespec.weekfrom = "1";
				datespec.weekto = ''+nrepetitions;
			} else {
				var weeksequence = new Array();
				for ( var i = 0; i < nrepetitions; i++ ) {
					weeksequence.push(''+(1+i*2));
				}
				datespec.weeksequence = weeksequence;
			}
		} else {
			if ( weeksfrom === weeksto ) {
				datespec.weeksingle = ''+weeksfrom;
			} else if ( frequency === "weekly") {
				datespec.weekfrom = ''+weeksfrom;
				datespec.weekto = ''+weeksto;
			} else {
				var weeksequence = new Array();
				for ( var i = weeksfrom; i <= weeksto; i+=2 ) {
					weeksequence.push(''+i);
				}
				datespec.weeksequence = weeksequence;
			}
		}
		
		
		
		datespec.timeslots = [
		                      checkSpec({
		                    	  days : orderedDays,
		                    	  timestamp : {
		                    	          fromh : fromHour,
		                    	          fromm : fromMin,
		                    	          toh : toHour,
		                    	          tom : toMin
		                    	  }
		                      }, false)
		                ]
		return datespec;
		

	};
		
    var showTab = function(rb) {
    	$(rb).prop("checked",true);
    	$(rb).parents(".radio-btn-tab-group").find('.radio-btn-tab-content').hide();
        var targetContent = "." + $(rb).attr('tab-content');
        $(rb).parents(".radio-btn-tab-group").find(targetContent).show();    	    	
    };

	/**
	 * Check a checkbox from matches in a string.
	 */
	var checkCheckbox = function(formSelector, fieldSelector, checked) {
		$(formSelector).find(fieldSelector).each(function() {
			if ( checked.indexOf($(this).val()) >= 0 ) {
				$(this).prop('checked', true);
			} else {
				$(this).prop('checked', false);
			}
		});		
	}
	
	
	/**
	 * Loads a date spec into a form.
	 */
	var dateSpecToForm = function(formSelector, datespec) {
		if ( $(formSelector).length !== 1 ) 
			throw "Form selector does not find anything, please speficy a valid jquery selector ie not "+formSelector;
		// reset the form, we mus reset everything. form reset doesnt work here.
		$(formSelector).find("input:checked").each(function() {
    		$(this).attr("checked",false);
    	});
		$(formSelector).find("option:selected").each(function() {
    		$(this).attr("selected",false);
		});
		$(formSelector).find("input[type=text]").each(function() {
    		$(this).val("");
		});


		if (! ( datespec &&
				datespec.timeslots &&
				datespec.timeslots.length === 1 &&
				datespec.timeslots[0].days &&
				datespec.timeslots[0].timestamp &&
				datespec.timeslots[0].timestamp.fromh) ) {
			// We set everything to something meaningful so if the user changes anythign at least they
			// have a valid pattern.
			showTab($(formSelector).find("[name='date-range-type'][value='weeks']"));
			showTab($(formSelector).find("[name='frequency'][value='weekly']"));
			$(formSelector).find("[name='repetitions']").val("0");
			$(formSelector).find("[name='weeks-from']").val("1");
			$(formSelector).find("[name='weeks-to']").val("1");
			checkCheckbox(formSelector,"[name='day']","M");
			$(formSelector).find("[name='from-hour']").val("09");
			$(formSelector).find("[name='from-minute']").val("00");
			$(formSelector).find("[name='to-hour']").val("10");
			$(formSelector).find("[name='to-minute']").val("00");
			// console.log("Spec is not complete, have reset form");
			return;
		}
		if ( !(datespec.weekfrom || datespec.weeksequence || datespec.weeksingle )) 
			throw "Date spec must have a weekfrom, weeksequence or weeksingle";
		datespec = checkSpec(datespec, true);
		if ( datespec.weekfrom || datespec.weeksingle ) {
			showTab($(formSelector).find("[name='date-range-type'][value='weeks']"));
			showTab($(formSelector).find("[name='frequency'][value='weekly']"));

			$(formSelector).find("[name='repetitions']").val("0");
			if ( datespec.weekfrom ) {
				$(formSelector).find("[name='weeks-from']").val(datespec.weekfrom);
				if (datespec.weekto ) {
					$(formSelector).find("[name='weeks-to']").val(datespec.weekto);							
				} else {
					$(formSelector).find("[name='weeks-to']").val(datespec.weekfrom);			
				}				
			} else {
				$(formSelector).find("[name='weeks-from']").val(datespec.weeksingle);
				$(formSelector).find("[name='weeks-to']").val(datespec.weeksingle);				
			}
		} else {
			if ( datespec.weeksequence.length < 2 ) 
				throw "A week sequence of 1 or less makes not sense";
			var sortedWeeks = datespec.weeksequence.sort();
			
			var r = sortedWeeks[1]-sortedWeeks[0];
			
			for ( var i = 1; i < datespec.weeksequence.length; i++ ) {
				if ( (sortedWeeks[i]-sortedWeeks[i-1]) !== r )
					throw "Form is not capable of editing random week sequences";
			}
			$(formSelector).find("[name='repetitions']").val("0");
			if ( r === 0 ) {
				showTab($(formSelector).find("[name='date-range-type'][value='weeks']"));
				showTab($(formSelector).find("[name='frequency'][value='weekly']"));
				$(formSelector).find("[name='weeks-from']").val(sortedWeeks[0]);
				$(formSelector).find("[name='weeks-to']").val(sortedWeeks[0]);
			} else if ( r === 1) {
				showTab($(formSelector).find("[name='date-range-type'][value='weeks']"));
				showTab($(formSelector).find("[name='frequency'][value='weekly']"));
				$(formSelector).find("[name='weeks-from']").val(sortedWeeks[0]);
				$(formSelector).find("[name='weeks-to']").val(sortedWeeks[sortedWeeks.length-1]);
			} else if ( r === 2) {
				showTab($(formSelector).find("[name='date-range-type'][value='weeks']"));
				showTab($(formSelector).find("[name='frequency'][value='bi-weekly']"));
				$(formSelector).find("[name='weeks-from']").val(sortedWeeks[0]);
				$(formSelector).find("[name='weeks-to']").val(sortedWeeks[sortedWeeks.length-1]);			
			} else {
				throw "Form is not capable of editing uniform week sequences greater than bi-weekly";
			}
		}
		var ts = datespec.timeslots[0];
		var times = ts.timestamp;
		checkCheckbox(formSelector,"[name='day']",ts.days);
		
		
		
		
		
		$(formSelector).find("[name='from-hour']").val(times.fromh);
		$(formSelector).find("[name='from-minute']").val(times.fromm);			
		$(formSelector).find("[name='to-hour']").val(times.toh);		
		$(formSelector).find("[name='to-minute']").val(times.tom);
		
		
	}
	
	/**
	 * Accepts a json structure of the form
	 *  {
	 *    "weekfrom":"1",  // range (mututally exclusive with weeksequence, weeksingle)
	 *    "weekto":"5",    // range (mututally exclusive with weeksequence, weeksingle)
	 *    "weeksequence": [ "3", "4", "5" ], // sequence (mututally exclusive with weekfrom, weekfrom, weeksingle)
	 *    "weeksingle" : "4", // sequence (mututally exclusive with weekfrom, weekfrom, weeksequence)
c	 *    "timeslots":[
	 *    		{
	 *    			"days":"M",
	 *    			"timestamp":{
	 *    				"fromh":"10"
	 *    				"fromm":"15", // optional
	 *    				"toh":"12",   // optional
	 *    				"tom":"15"    // optional
	 *    			}
	 *    		},
	 *          { .... // additional timeslots }
	 *    ]
	 *  }
	 *  
	 */
	var formatDateSpec = function(datespec) {
		
		if ( ! datespec ) {
			return "";
		}
		
		var spec = "";
		if (datespec.weekfrom && datespec.weekto) {
			spec = spec+datespec.weekfrom+"-"+datespec.weekto+" ";
		} else if ( datespec.weeksequence ) {
			spec = spec+datespec.weeksequence.join(",")+" ";	
		} else if ( datespec.weeksingle ) {
			spec = spec+datespec.weeksingle+" ";	
		} else {
			throw "Invalid date specifictaiton, need weekfrom/weekto or weeksequence or weeksingle";
		}
		if (! datespec.timeslots || datespec.timeslots.length === 0) {
			throw "Invalid date specification, no timeslots defined";
		}
		for ( var i = 0; i < datespec.timeslots.length; i++ ) {
			var ts = datespec.timeslots[i];
			if (!(ts.days && ts.timestamp )) {
				throw "Invalid date specification, each timeslot must have at least one timestamp, timeslot in error "+i;
			}
			spec = spec+ts.days;
			var times = ts.timestamp;
			if (! times.fromh ) {
				throw "Invalid date specification, each timestamp must have a start hour";
			}
			spec = spec+times.fromh;
			if ( times.fromm ) {
				spec = spec+":"+times.fromm;
			}
			if ( !(times.toh === times.fromh && times.tom === times.fromm) ) {	
				if ( times.toh ) {
					spec = spec+"-"+times.toh;
				}
				if ( times.tom && ! times.toh ) {
					throw "Invalid date specification, if specifying timestamp 'tom' you must specify 'toh' ";
				}
				if ( times.tom ) {
					spec = spec+":"+times.tom;
				}
			}
			spec = spec+" ";
		}		
		return spec;
	};
	
	/**
	 * creates an array of json from a date string
	 */
	var parseDateSpec = function(inputdatepattern ) {
		var allresults = new Array();
		var datepattern = inputdatepattern.split(";");
		// loop over all patterns supplied.
		for ( var i = 0; i < datepattern.length; i++ ) {
			
			// console.log("Checking "+datepattern[i]+" "+i)
			var result = {}
			// patterns are generally space seperated, but days and timeranges can be in the same token.
			var parts = datepattern[i].trim().split(' ');
			// the week specification will always be in the first token.
			// there are 3 forms, "1-2", "1,2,3,4" or "1". In order of preference.
			var weekrange = /^([1-9]?[0-9])-([1-9]?[0-9])/.exec(parts[0]);
			var weeksequence = /^[1-9]?[0-9],([1-9]?[0-9],?)+/.exec(parts[0]);
			var week = /^[1-9]?[0-9]/.exec(parts[0]);
			if ( weekrange ) {
				result.weekfrom = weekrange[1];
				result.weekto = weekrange[2];
				// console.log("Week range "+weekrange[1]+" to "+weekrange[2]);
			} else if ( weeksequence ) {
				result.weeksequence = weeksequence[0].split(",");
				// console.log("Week Sequence "+weeksequence[0].split(","));					
			} else if ( week ) {
				result.weeksingle = week[0];
				// console.log("Single Week "+week[0]);
			} else {
				continue; // no week pattern found
				// console.log("Nothing matched in "+p[0]);
			}
			// lookup for 2 char day specs 
			var day2 = "motuwethfrsasu";
			// lookup for 1-2 car date specs, divide index by 2 to get dat num
			var day1 = "m tuw thf sasu";
			// array of days in normalised format.
			var day = ["M","Tu","W","Th","F","Sa","Su"];
			// becomes true when we have found a day and can start looking to a timeslot
			var isday = false;
			// contains accumuated days.
			var days = ""
			var timeslots = new Array();
			// days/timeslots may be spread over more than one token
			for ( var j = 1; j < parts.length; j++ ) {
				// timeslots contain a day specification and a time specification, they may be seperated by  spaces
				// then again they might not so we loop through the tokens
				// collecting days.
				// days can be of the form MTuW
				// M-Fr
				// M,W
				// there may or may not be a space between the last day and the time.
				var dayrange = /([m,mo,tu,w,we,th,f,fr,sa])-([tu,w,we,th,f,fr,sa,su])/i.exec(parts[j]);
				var daysequence = /([m,mo,tu,w,we,th,f,fr,sa,su],)([m,mo,tu,w,we,th,f,fr,sa,su],?)?/i.exec(parts[j]);
				var dayset = /([m,mo,tu,w,we,th,f,fr,sa,su])+/i.exec(parts[j]);
				// console.log(parts[j]+"Matched "+dayrange+" "+daysequence+" "+dayset);
				if ( dayrange ) {
					// console.log("Day range was matched");
					var startday = dayrange[1].toLowerCase();
					var s = day2.indexOf(startday);
					if ( s < 0 ) s = day1.indexOf(startday);
					s = s/2;					
					var endday = dayrange[2].toLowerCase();
					var e = day2.indexOf(endday);
					if ( e < 0 ) s = day1.indexOf(endday);
					e = e/2;
					for ( var k = s; k <= e; k++) {
						days = days+day[k];
					}
					isday = true;
				} else if ( daysequence ) {
					// console.log("Day sequence was matched");
					// convert the user input which could be 1 or 2day format into a normalised format
					// first by looking up in the 2 day lookup string, then the 1 day string.
					// if a match is found, the index/2 is the index in the normalised days array.
					var dayseq = daysequence[0].toLowerCase().split(",");
					for ( var k = 0; k < dayseq.length; k++) {
						var d = day2.indexOf(dayseq[k]);
						if ( d < 0 ) d = day1.indexOf(dayseq[k]);
						d = d/2;
						days = days + day[d];
					}
					isday = true;
				} else if ( dayset ) {
					// console.log("Day set was matched "+dayset[0]);
					// convert the user input into normalised form as above.
					var daystr = dayset[0].toLowerCase();
					var k = 0;
					while(k < daystr.length) {
						var d = day2.indexOf(daystr.substring(k,k+2));
						if ( d < 0 ) {
							d = day1.indexOf(daystr.substring(k,k+1));
							k++;
						} else {
							k = k + 2;
						}
						if ( d >= 0 ) {
							d = d/2;
							// console.log(" Added  "+day[d]);
							days = days + day[d];
						}
					}
					isday = true;
				}	
				// must have a day before we can consider timeslot
				var timestamp = null;
				if ( isday ) {
					// timeslots take a number of forms
					// 10:12-11:12
					// 10:12-11
					// 10-11:12
					// 10-11
					// 10
					// this could have been achieved with a single RE, but hardly anyone would be able to read it.
					var timestamp1 = /([0-2]?[0-9]):([0-5]?[0-9])-([0-2]?[0-9]):([0-5]?[0-9])/.exec(parts[j]);
					var timestamp2 = /([0-2]?[0-9]):([0-5]?[0-9])-([0-2]?[0-9])/.exec(parts[j]);
					var timestamp3 = /([0-2]?[0-9])-([0-2]?[0-9]):([0-5]?[0-9])/.exec(parts[j]);
					var timestamp4 = /([0-2]?[0-9])-([0-2]?[0-9])/.exec(parts[j]);
					var timestamp5 = /([0-2]?[0-9]):([0-5]?[0-9])/.exec(parts[j]);
					var timestamp6 = /([0-2]?[0-9])/.exec(parts[j]);
					if ( timestamp1 ) {
						timestamp = {
								fromh : timestamp1[1],
								fromm : timestamp1[2],
								toh : timestamp1[3],
								tom : timestamp1[4]
						};
					} else if ( timestamp2 ) {					
						timestamp = {
							fromh : timestamp2[1],
							fromm : timestamp2[2],
							toh : timestamp2[3]
						};
					} else if ( timestamp3 ) {
						timestamp = {
							fromh : timestamp3[1],
							toh : timestamp3[2],
							tom : timestamp3[3]
						};
					} else if ( timestamp4 ) {
						timestamp = {
							fromh : timestamp4[1],
							toh : timestamp4[2]
						};
					} else if ( timestamp5 ) {
						timestamp = {
								fromh : timestamp5[1],
								fromm : timestamp5[2]
						};
					} else if ( timestamp6 ) {
						timestamp = {
								fromh : timestamp6[1]
						};
					}
				}
				if ( timestamp ) {
					// thats the end of a timeslot sequence, we must not dump everyting and reset
					timeslots.push({ 
						days: days,
						timestamp: timestamp
					});
					// reset
					days = "";
					isday = false;
					
					
				}
				
				
			}
			if ( timeslots.length > 0 ) {
				// add the parsed structure to the results.
				result.timeslots = timeslots;
				allresults.push(checkSpec(result, false));
			} else {
				// console.log("No Timeslots found in pattern");
			}
		}
		
		return allresults;
		
		
		
	};

	var dotests = true;
	// test the above.
	if ( dotests ) {
		// test pattern parsing
		var res = parseDateSpec("1,2,5,6 M10");
		// console.log("Parsed as "+JSON.stringify(res));
		if ( res.length !== 1 ) throw "result Length doesnt match: "+JSON.stringify(res);
		if ( ! res[0].weeksequence ) throw "result weeksequence not present: "+JSON.stringify(res);
		if ( res[0].weekfrom ) throw "result weekfrom was present: "+JSON.stringify(res);
		if ( res[0].weekto ) throw "result weekto was present: "+JSON.stringify(res);
		if ( res[0].weeksingle ) throw "result weeksingle was present: "+JSON.stringify(res);
		if ( res[0].weeksequence.length !== 4 ) throw "result week sequence lenght not right: "+JSON.stringify(res);
		if ( res[0].weeksequence[0] !== "1" ) throw "result week sequence not right element 0 "+JSON.stringify(res);
		if ( res[0].weeksequence[1] !== "2" ) throw "result week sequence not right element 1 "+JSON.stringify(res);
		if ( res[0].weeksequence[2] !== "5" ) throw "result week sequence not right element 2 "+JSON.stringify(res);
		if ( res[0].weeksequence[3] !== "6" ) throw "result week sequence not right element 3 "+JSON.stringify(res);
		if ( ! res[0].timeslots ) throw "result timeslots not present: "+JSON.stringify(res);
		if ( res[0].timeslots.length !== 1 ) throw "result timeslots length not right: "+JSON.stringify(res);
		if ( ! res[0].timeslots[0].days ) throw "result days not present: "+JSON.stringify(res);
		if ( res[0].timeslots[0].days !== 'M' ) throw "result days not right: "+JSON.stringify(res);
		if ( ! res[0].timeslots[0].timestamp ) throw "result timestamp not present: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.fromh !== "10" ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.fromm ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.toh ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.tom ) throw "result timestamp not right: "+JSON.stringify(res);
		
		var resStr = formatDateSpec(res[0]);
		// console.log("Date Spec is "+resStr);
		res = parseDateSpec(resStr);
		var resStr2 = formatDateSpec(res[0]);
		if ( resStr !== resStr2) throw "Roundtrip failed "+resStr+" != "+resStr2;
		
		
		var res = parseDateSpec("1,2,5,6 MTuThFSaSu10");
		// console.log("Parsed as "+JSON.stringify(res));
		if ( res.length !== 1 ) throw "result Length doesnt match: "+JSON.stringify(res);
		if ( ! res[0].weeksequence ) throw "result weeksequence not present: "+JSON.stringify(res);
		if ( res[0].weekfrom ) throw "result weekfrom was present: "+JSON.stringify(res);
		if ( res[0].weekto ) throw "result weekto was present: "+JSON.stringify(res);
		if ( res[0].weeksingle ) throw "result weeksingle was present: "+JSON.stringify(res);
		if ( res[0].weeksequence.length !== 4 ) throw "result week sequence lenght not right: "+JSON.stringify(res);
		if ( res[0].weeksequence[0] !== "1" ) throw "result week sequence not right element 0 "+JSON.stringify(res);
		if ( res[0].weeksequence[1] !== "2" ) throw "result week sequence not right element 1 "+JSON.stringify(res);
		if ( res[0].weeksequence[2] !== "5" ) throw "result week sequence not right element 2 "+JSON.stringify(res);
		if ( res[0].weeksequence[3] !== "6" ) throw "result week sequence not right element 3 "+JSON.stringify(res);
		if ( ! res[0].timeslots ) throw "result timeslots not present: "+JSON.stringify(res);
		if ( res[0].timeslots.length !== 1 ) throw "result timeslots length not right: "+JSON.stringify(res);
		if ( ! res[0].timeslots[0].days ) throw "result days not present: "+JSON.stringify(res);
		if ( res[0].timeslots[0].days !== 'MTuThFSaSu' ) throw "result days not right: "+JSON.stringify(res);
		if ( ! res[0].timeslots[0].timestamp ) throw "result timestamp not present: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.fromh !== "10" ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.fromm ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.toh ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.tom ) throw "result timestamp not right: "+JSON.stringify(res);
		
		var resStr = formatDateSpec(res[0]);
		// console.log("Date Spec is "+resStr);
		res = parseDateSpec(resStr);
		var resStr2 = formatDateSpec(res[0]);
		if ( resStr !== resStr2) throw "Roundtrip failed "+resStr+" != "+resStr2;

		
		res = parseDateSpec("3,4,5 Tu12:20-12:44");
		// console.log("Parsed as "+JSON.stringify(res));
		if ( res.length !== 1 ) throw "result Length doesnt match: "+JSON.stringify(res);
		if ( ! res[0].weeksequence ) throw "result weeksequence not present: "+JSON.stringify(res);
		if ( res[0].weekfrom ) throw "result weekfrom was present: "+JSON.stringify(res);
		if ( res[0].weekto ) throw "result weekto was present: "+JSON.stringify(res);
		if ( res[0].weeksingle ) throw "result weeksingle was present: "+JSON.stringify(res);
		if ( res[0].weeksequence.length !== 3 ) throw "result week sequence lenght not right: "+JSON.stringify(res);
		if ( res[0].weeksequence[0] !== "3" ) throw "result week sequence not right element 0 "+JSON.stringify(res);
		if ( res[0].weeksequence[1] !== "4" ) throw "result week sequence not right element 1 "+JSON.stringify(res);
		if ( res[0].weeksequence[2] !== "5" ) throw "result week sequence not right element 2 "+JSON.stringify(res);
		if ( ! res[0].timeslots ) throw "result timeslots not present: "+JSON.stringify(res);
		if ( res[0].timeslots.length !== 1 ) throw "result timeslots length not right: "+JSON.stringify(res);
		if ( ! res[0].timeslots[0].days ) throw "result days not present: "+JSON.stringify(res);
		if ( res[0].timeslots[0].days !== 'Tu' ) throw "result days not right: "+JSON.stringify(res);
		if ( ! res[0].timeslots[0].timestamp ) throw "result timestamp not present: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.fromh !== "12" ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.fromm !== "20") throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.toh !== "12" ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.tom !== "40") throw "result timestamp not right: "+JSON.stringify(res);
		
		resStr = formatDateSpec(res[0]);
		// console.log("Date Spec is "+resStr);
		res = parseDateSpec(resStr);
		resStr2 = formatDateSpec(res[0]);
		if ( resStr !== resStr2) throw "Roundtrip failed "+resStr+" != "+resStr2;

		res = parseDateSpec("1-5 M10;5 Tu10");
		// console.log("Parsed as "+JSON.stringify(res));
		if ( res.length !== 2 ) throw "result Length doesnt match: "+JSON.stringify(res);
		if ( res[0].weeksequence ) throw "result weeksequence present: "+JSON.stringify(res);
		if ( !res[0].weekfrom ) throw "result weekfrom was not present: "+JSON.stringify(res);
		if ( !res[0].weekto ) throw "result weekto was not present: "+JSON.stringify(res);
		if ( res[0].weeksingle ) throw "result weeksingle was present: "+JSON.stringify(res);
		if ( res[0].weekfrom !== "1" ) throw "result week sequence not right element 0 "+JSON.stringify(res);
		if ( res[0].weekto !== "5" ) throw "result week sequence not right element 1 "+JSON.stringify(res);
		if ( ! res[0].timeslots ) throw "result timeslots not present: "+JSON.stringify(res);
		if ( res[0].timeslots.length !== 1 ) throw "result timeslots length not right: "+JSON.stringify(res);
		if ( ! res[0].timeslots[0].days ) throw "result days not present: "+JSON.stringify(res);
		if ( res[0].timeslots[0].days !== 'M' ) throw "result days not right: "+JSON.stringify(res);
		if ( ! res[0].timeslots[0].timestamp ) throw "result timestamp not present: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.fromh !== "10" ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.fromm ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.toh ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[0].timeslots[0].timestamp.tom) throw "result timestamp not right: "+JSON.stringify(res);


		if ( res[1].weeksequence ) throw "result weeksequence present: "+JSON.stringify(res);
		if ( res[1].weekfrom ) throw "result weekfrom was not present: "+JSON.stringify(res);
		if ( res[1].weekto ) throw "result weekto was not present: "+JSON.stringify(res);
		if ( !res[1].weeksingle ) throw "result weeksingle was present: "+JSON.stringify(res);
		if ( res[1].weeksingle !== "5" ) throw "result week single not right  "+JSON.stringify(res);
		if ( ! res[1].timeslots ) throw "result timeslots not present: "+JSON.stringify(res);
		if ( res[1].timeslots.length !== 1 ) throw "result timeslots length not right: "+JSON.stringify(res);
		if ( ! res[1].timeslots[0].days ) throw "result days not present: "+JSON.stringify(res);
		if ( res[1].timeslots[0].days !== 'Tu' ) throw "result days not right: "+JSON.stringify(res);
		if ( ! res[1].timeslots[0].timestamp ) throw "result timestamp not present: "+JSON.stringify(res);
		if ( res[1].timeslots[0].timestamp.fromh !== "10" ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[1].timeslots[0].timestamp.fromm ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[1].timeslots[0].timestamp.toh ) throw "result timestamp not right: "+JSON.stringify(res);
		if ( res[1].timeslots[0].timestamp.tom) throw "result timestamp not right: "+JSON.stringify(res);

		resStr = formatDateSpec(res[0]);
		// console.log("Date Spec is "+resStr);
		res = parseDateSpec(resStr);
		resStr2 = formatDateSpec(res[0]);
		if ( resStr !== resStr2) throw "Roundtrip failed "+resStr+" != "+resStr2;

		
	}
	
	
	return {
		parse_date_spec : parseDateSpec,
		format_date_spec : formatDateSpec,
		create_date_spec : dateSpecFromForm,
		update_form : dateSpecToForm
	}
});
