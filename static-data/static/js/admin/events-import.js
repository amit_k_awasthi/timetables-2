
define(["jquery", 
        "admin/validation",
        "admin/classification-selectors",
        "admin/views",
        "domReady"], 
        function($,validation, views){
	"use strict";
    
    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/events/import/";
    });

    validation.bind($('#import-events-form'));
    return {
    };
});