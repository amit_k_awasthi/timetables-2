
define(["jquery",
        "admin/views"], 
        function($, views){
    "use strict";

    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/administrators/";
    });
    
    // provides no functionality at the moment
    return {};
});