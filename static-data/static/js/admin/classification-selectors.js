define(["jquery",
        "domReady"], 
        function($){
	"use strict";
	/**
	 * This module manages the classification selectors, both the radio box based selectors, the check box selectors.
	 * When a classification is added it may have a parent classification. If so it should not be shown as an option for users, 
	 * unless that parent is checked or selected. The wiedgets are rendered by fragments, with each rendered html element containing
	 * details of the parent
	 */
	
	// recurse down the dependency tree finding all children and adjusting as appropriate.
	var manage_checkable_children = function(checkable, level) {
		if ( level > 20 ) {
			console.log("Probably infinite recursion, aborting ");
			return; // infinite recursion protection
		}
    	var parent = $(checkable).parents(".classification-control-container");
        if (checkable.checked) {
           $(parent).find(".classification-parent-"+$(checkable).val()).each(function() {
        	   $(this).show();
        	   manage_checkable_children(this, level+1); // if this was checked, check for its children.
           });
        } else {
        	$(parent).find(".classification-parent-"+$(checkable).val()).each(function(){
           	   $(this).find("input:checked").removeAttr("checked");
           	   $(this).hide();
           	   manage_checkable_children(this, level+1); // if this was checked, check for its children.
            });
        }	
	};
	
	// recurse down the dependecy finding all children and enabling or disabling the option as appropriate.
	var manage_selectable_children = function(selectable, level) {
		if ( level > 20 ) {
			console.log("Probably infinite recursion, aborting ");
			return; // infinite recursion protection
		}
    	var parent = $(selectable).parents(".classification-control-container");
    	var selectedId = $(selectable).val()
    	if ( selectable.selected ) {
	    	$(parent).find(".classification-selector option").each(function(){
	    		var parentId = $(this).attr("parent");
	    		if ( parentId === selectedId ) {
					$(this).removeAttr("disabled"); // enable the option, but leave selected alone.
					manage_selectable_children(this, level+1);
	    		}
	    	});
    	} else {
	    	$(parent).find(".classification-selector option").each(function(){
	    		var parentId = $(this).attr("parent");
	    		if ( parentId === selectedId ) {
	    			$(this).removeAttr("selected"); // deselect becuase the parent was not selected
					$(this).attr("disabled","disable"); // disable the option.
					manage_selectable_children(this, level+1);
	    		}
	    	});    		
    	}
	};

	
	var update_selectable_container = function(selectable) {
    	var parent = $(selectable).parents(".classification-control-container");
    	$(parent).find("option").each(function() { manage_selectable_children(this,0)});		
	};
	

	// bind to the classification selectors in classifcation-selection and classification-control-radio to show and hide elements that are not clildren of what is selected.
    $(".classification-control").click(function(){
    	manage_checkable_children(this,0);
    });
    
    // bind to classification drop downs in classification-selection-dropdown to make then obey parent child relationships
    $(".classification-selector").change(function(){
    	update_selectable_container(this);
    });
    
    // initialse, since some of the controls may have been loaded checked.
    $(".classification-control").each(function(){
    	manage_checkable_children(this,0);
    });
    
    $(".classification-selector option").each(function(){
    	manage_selectable_children(this,0);
    });
    
});
