define(["jquery",
        "admin/validation",
        "admin/views",
        "admin/classification-selectors"
       ],
        function($, validation, views){    
	"use strict";

    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/administrators/add";
    });
    
    validation.bind('#add-administrators');
    
    return {};
});