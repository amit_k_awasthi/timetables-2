var require, _, Backbone, console;

// Configure RequireJS
require.config({
    // Alias the library imports to shorter names
    paths: {
        domReady: "libs/domReady",
        text: "libs/text",
        jquery: "libs/jquery",
        underscore: "libs/underscore",
        backbone: "libs/backbone",
        modernizr: "libs/modernizr-min",
        json2: "libs/json2",
        uri: "libs/uri",
        fullcalendar: "libs/fullcalendar/fullcalendar",
        bootstrap_affix: "libs/bootstrap-affix",
        "jquery/bgiframe": "libs/jquery-ui/external/jquery.bgiframe",
        "jquery-sort" : "libs/jquery-sort",
        "jquery-ui/core": "libs/jquery-ui/ui/jquery.ui.core",
        "jquery-ui/widget": "libs/jquery-ui/ui/jquery.ui.widget",
        "jquery-ui/position": "libs/jquery-ui/ui/jquery.ui.position",
        "jquery-ui/dialog": "libs/jquery-ui/ui/jquery.ui.dialog",
        "jquery-ui/tabs": "libs/jquery-ui/ui/jquery.ui.tabs",
        "jquery-ui/mouse": "libs/jquery-ui/ui/jquery.ui.mouse",
        "jquery-ui/sortable": "libs/jquery-ui/ui/jquery.ui.sortable",
        "jquery-ui/autocomplete": "libs/jquery-ui/ui/jquery.ui.autocomplete",
        "jquery-ui/datepicker": "libs/jquery-ui/ui/jquery.ui.datepicker",
        "jquery-ui/button": "libs/jquery-ui/ui/jquery.ui.button",
        "jquery-ui/selectbox": "libs/jquery-ui/ui/jquery.ui.selectbox",
        "jquery-ui/effects/core": "libs/jquery-ui/ui/jquery.effects.core",
        "jquery-ui/effects/scale": "libs/jquery-ui/ui/jquery.effects.scale",
        "jquery-ui/effects/drop": "libs/jquery-ui/ui/jquery.effects.drop"
    },
    // Give RequireJS names to global variables loaded from the non AMD 
    // libraries at the (aliased) paths listed as keys. These key names expand
    // to the values given in the paths config section.
    shim: {
        jquery: {
            exports: "$"
        },
        underscore: {
            exports: "_"
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        // Don't think we actually use Modernizr, should be removed if we
        // don't need it.
        modernizr: {
            exports: "Modernizr"
        },
        uri: {
            exports: "URI"
        },
        fullcalendar: {
            deps: ["jquery"]
        },
        bootstrap_affix: {
            deps: ["jquery"]
        },
        // Tell RequireJS about script.js. It should be refactored into an AMD 
        // module, and probably into distinct pieces.
        script: {
            deps: ["jquery"]
        },
        "jquery-ui/core": {
            deps: ["jquery"]
        },
        "jquery-ui/dialog": { 
            deps: ["jquery-ui/core", "jquery-ui/position", "jquery-ui/widget"] 
        },
        "jquery-ui/tabs": { 
            deps: ["jquery-ui/core", "jquery-ui/widget"] 
        },
        "jquery-ui/mouse": { 
            deps: ["jquery-ui/core", "jquery-ui/widget"]
        },
        "jquery-ui/sortable": { 
            deps: ["jquery-ui/core", "jquery-ui/widget", "jquery-ui/mouse"]
        },
        "jquery-ui/autocomplete": { 
            deps: ["jquery-ui/core", "jquery-ui/position", "jquery-ui/widget"] 
        },
        "jquery-ui/position": {
            deps: ["jquery"]
        },
        "jquery-ui/widget": {
            deps: ["jquery"]
        },
        "libs/jquery-django-csrf": {
            deps: ["jquery"]
        },
        "jquery-sort": {
            deps: ["jquery"]
        },
        "jquery-ui/effects/core": {
            deps: ["jquery"]
        },
        "jquery-ui/effects/scale": {
            deps: ["jquery-ui/effects/core"]
        },
        "jquery-ui/effects/drop": {
            deps: ["jquery-ui/effects/core"]
        }
    }
});

require(
        // Load script.js and plugins.js. It looks like we don't actually need
        // plugins.js, it should probably be removed...
        ["jquery", "underscore", "underscore-mixins", "script", "plugins",
         "libs/jquery-django-csrf", "json2", "domReady","popup"],
        // main.js's entry point.
        function($, _, underscoreMixins) {
   "use strict";

    // Configure Underscore template syntax. ERB templates make my eyes bleed :(
    _.templateSettings = {
        // Include var w/o HTML escaping: {{& safe_var }} 
        interpolate: /\{\{&([\s\S]+?)\}\}/g, 
        // Include var with HTML escaping: {{ some_var }}
        escape:      /\{\{([\s\S]+?)\}\}/g,
        // Evaluate javascript: {% _.each(foos, function(foo) { %}
        evaluate:    /\{%([\s\S]+?)%\}/g
    };
    _.mixin(underscoreMixins);

    // Unregister global names of underscore and Backbone. Note that jQuery's 
    // $ should be unregistered with $.noConflict() as well, but script.js still
    // depends on it, so we can't for now.
    //_.noConflict();
    //Backbone.noConflict();

    // The script tag used to load require.js has an attribute containing the
    // module to be loaded after main (this).
    var page_module = $("script[src$='require.js']").attr("data-page-module");
    if(!page_module) {
        console.log("No value for attribute data-page-module provided on " + 
                "require.js's script tag.");
        return;
    }

    console.log("Loading entry point:", page_module);
    // Load the module defined as the page's entry point.
    _.defer(function() {
        require([page_module], function(page_module) {
            // If the module returns a backbone view, run it.
            if(page_module && page_module.ApplicationView) {
                // Kick things off (waiting for dom to be loaded):
                require(["domReady"], function() {
                    new page_module.ApplicationView({
                        // The ApplicationView controls the entire page body
                        el: $("body").get()
                    }).render();
                });
            }
        });
    });
});
