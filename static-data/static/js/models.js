define(["jquery", "underscore", "backbone", "backbone-utils"], 
        function($, _, Backbone, backbone_utils) {
    "use strict";
    
    var superclass = backbone_utils.superclass;
    var superproto = backbone_utils.superproto;
    
    // We should never need to import views.js in models. Models should not know
    // about views.
    
    var Organisation = Backbone.Model.extend({
        constructor: function Organisation() {
            superclass(Organisation).apply(this, arguments);
        },
        
        defaults: {
            name: undefined,
            id: undefined,
            code: undefined
        }
    });
    
    var Organisations = Backbone.Collection.extend({
        constructor: function Organisations() {
            superclass(Organisations).apply(this, arguments);
        },
        
        model: Organisation
    });
    
    var Year = Backbone.Model.extend({
        constructor: function Year() {
            superclass(Year).apply(this, arguments);
        },
        
        initialize: function() {
            var organisationsJSON = this.get("organisations");
            this.set({"organisations": 
                    new Organisations(organisationsJSON)});
        },
        
        organisationWithCode: function(code) {
            return this.get("organisations").find(function(org) {
                return org.code === code;
            }); 
        },
        
        toString: function() {
            return this.get("name");
        }
    });
    
    var Years = Backbone.Collection.extend({
        constructor: function Years() {
            superclass(Years).apply(this, arguments);
        },
        
        model: Year
    });
    
    /**
     * Maintains a Years collection of Year models, plus the index of the
     * active year.
     */
    var YearsState = Backbone.Model.extend({
        constructor: function YearsState() {
            superclass(YearsState).apply(this, arguments);
        },
        
        defaults: {
            selectedYear: 0,
            selectedOrganisation: 0,
            years: null
        },
        
        initialize: function() {
//            var years = this.get("years");
//            if(years && years.length > 0) {
//                this.set({selectedYear: 0});
//                var orgs = years.get("organisation");
//                if()
//            }
        },
        
        selectYear: function(start_year) {
            // Find the index of the new year
            var index = null;
            this.years().each(function(year, i) {
                if(year.get("start_year") === start_year) {
                    index = i;
                    return {}; // break iteration
                }
            });
            
            // When updating the year we need to keep track of the current 
            // organisation and try to re-select an equivalent from the new 
            // year's organisation list.
            var oldOrganisation = this.selectedOrganisation();
                        
            var organisationCode = null;
            if(oldOrganisation) {
                organisationCode = oldOrganisation.get("code");
            }
            
            // Update the selected year and organisation
            this.set({ selectedYear: index || 0 });
            this.selectOrganisation(organisationCode);
            return this;
        },
        
        selectOrganisation: function(organisationCode) {
            var index = null;
            this.organisations().each(function(org, i) {
                if(org.get("code") === organisationCode) {
                    index = i;
                    return {}; // break iteration
                }
            });
            this.set({ selectedOrganisation: index });
            return this;
        },
        
        selectedYear: function() {
            var selectedYear = this.get("selectedYear");
            if(this.years() && this.years().length) {
                return this.years().at(this.get("selectedYear"));
            }
            return null;
        },
        
        selectedOrganisation: function() {
            var orgs = this.organisations();
            if(!orgs) {
                return null;
            }
            return orgs.at(this.get("selectedOrganisation"));
        },
        
        years: function() {
            return this.get("years");
        },
        
        organisations: function() {
            var year = this.selectedYear();
            if(!year) {
                return null;
            }
            return year.get("organisations");
        }
    },
    // Class properties
    {
        fromJSON: function(data) {
            var years = new Years(data);
            return new YearsState({years: years});
        }
    });
    
    return {
        Organisation: Organisation,
        Organisations: Organisations,
        Year: Year,
        Years: Years,
        YearsState: YearsState
    };
});