define([
    "jquery",
    "student/calendar/all",
    "student/calendar/calendar",
    "fullcalendar",
    "domReady"],
    function($, all, calendar) {

    all.bindCommonCalendarInteractions();

    var weekCalendar = new calendar.Calendar(
        // Parent element
        $(".calendar-content .fullcalendar"),
        // Settings
        {defaultView: "agendaWeek"});

    // Bind interactions with various page elements which effect the calendar.
    calendar.bindEvents(weekCalendar);

    // Insert the calendar into the page (into the parent element)
    weekCalendar.initialise();
});