define(["jquery", "student/calendar/all", "domReady"], function($, all){

	all.bindCommonCalendarInteractions();

    // TODO: Refactor
    
    $(".course").hover(
        function () {
            $(this).closest(".term").find("." + $(this).attr("id")).css("background","#2FB2BF");
        },
        function () {
            $(this).closest(".term").find("." + $(this).attr("id")).css("background","#DBDBDB");
        }
    );
});