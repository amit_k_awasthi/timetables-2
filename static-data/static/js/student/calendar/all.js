define(["jquery", "uri"], function($, URI){

	function bindCommonCalendarInteractions() {
		$("#week-nav input[name=sweek]").on("change", function() {
			if($(this).val() === "cambridge") {
				window.location = String(URI().query({weektype: "cambridge"}));
			}
			else {
				window.location = String(URI().removeQuery("weektype"));
			}
		});
	}

    return {
    	bindCommonCalendarInteractions: bindCommonCalendarInteractions
    };
});