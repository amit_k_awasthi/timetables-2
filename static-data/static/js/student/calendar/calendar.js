define(["jquery", "underscore", "backbone", "bootstrap/username", "domReady"], 
	function($, _, Backbone, username) {

	function Calendar($dest, options) {
		if(!(options === undefined || _.isObject(options))) {
			throw new Error("Expected an object or undefined for options, got: "
				+ String(options));
		}

		this._$dest = $dest;
		this._options = options || {};

		_.bindAll(this, "_onViewDisplay");
	}

	Calendar.calendarUrl = function calendarUrl() {
		if(username === null)
			return null;
		return "/feeds/fullcalendar/personaltimetable/"
				+ encodeURIComponent(username) + ".json";
	}

	// Calendar instance methods/properties
	_.extend(Calendar.prototype, Backbone.Events, {
		/**
	 	 * The default options to use when creating fullcalendar calendars.
		 */
		defaultOptions: function() {
			return {
				events: {
					url: Calendar.calendarUrl(),
					cache: true
				},
				allDayDefault: false,
				firstDay: 1,
				viewDisplay: this._onViewDisplay,
				header: {left: "", center: "", right: ""}
			}
		},

		/**
		 * Shortcut for calling this._$dest.fullCalendar(foo, bar). Call
		 * this.fullCalendar(foo, bar) instead.
		 */
		fullCalendar: function() {
			$().fullCalendar.apply(this._$dest, arguments);
		},

		/**
		 * Gets the fullcalendar settings.
		 *
		 * This is a merge of the default settings and any extra settings 
		 * provided in the Calendar constructor, or setOption().
		 */
		options: function() {
			return _.extend({}, this.defaultOptions(), this._options);
		},

		/**
		 * Inserts/reinserts the fullcalendar widget into our destination 
		 * element.
		 */
		initialise: function() {
			// Fix height before emptying and re-populating the container
			// to avoid changing the scroll position in the page.
			this._$dest.css({height: this._$dest.height()});
			
			// re-create the calendar with current options
			this._$dest.empty();
			this.fullCalendar(this.options());

			// Let the height go back to whatever it wants
			this._$dest.css({height: ""});
		},

		setOption: function(name, value) {
			if(_.isObject(name))
				var options = name;
			else {
				var options = {};
				options[name] = value;
			}

			_.extend(this._options, options);
		},

		setWeekType: function(weekType) {
			if(weekType === "standard")
				var firstDay = 1; // Monday
			else if(weekType === "cambridge")
				var firstDay = 4; // Thurs
			else
				throw new Error("Unknown week type: " + weekType);

			this.setOption("firstDay", firstDay);
			this.initialise();
		},

		next: function() {
			this.fullCalendar("next");
		},

		previous: function() {
			this.fullCalendar("prev");
		},

		_onViewDisplay: function(view) {
			this.trigger("viewDisplay", view);
		},
	});

	function bindEvents(calendar) {
		
		// Listen for changes to selected week type and update the calendar.
		var selectedWeekType = $("#week-nav input[name=sweek]:checked").val();
		calendar.setWeekType(selectedWeekType);

		$(".calendar-content #next").on("click", function(event) {
			calendar.next();
		});

		$(".calendar-content #prev").on("click", function(event) {
			calendar.previous();
		});

		// Update the title area when the fullcalendar view updates
		calendar.on("viewDisplay", function(view) {
			$(".middle-date").html(view.title);
		});
	}

	return {
		Calendar: Calendar,
		bindEvents: bindEvents
	};
});