/**
 * Javascript for the student/events pages.
 */
define(["jquery",
        "admin/views",
        "uri",
        "student/student-base",
        "jquery-ui/effects/core"
    ], 
function($, views, URI) {	
	"use strict";
	
	/**
	 * Gets the "facetname:value" string representing a facet.
	 *
	 * $facetCheckbox: The <input type="checkbox"> representing the facet in the
	 *                 page.
	 */
	function facetIdentifier($facetCheckbox) {
		var facetName = $facetCheckbox.closest(".section-facets")
			.attr("data-facet-name");
		var facetValue = $facetCheckbox.attr("data-facet-value");
		return String(facetName) + "-" + String(facetValue);
	}

	function allFacetIdentifiers($facetGroup) {
		var $facetCheckboxes = $facetGroup.find("input[type=checkbox]");
		return _.map($facetCheckboxes, _.compose(facetIdentifier, $));
	}

	function clearAllFacetsFromURI($facetGroup, uri) {
		uri = uri || URI(window.location.href);

		var facetIds = allFacetIdentifiers($facetGroup);
		
		// Strip all facets from window.location.href
		var uriWithoutFacets = _.reduce(facetIds, function(uri, facetId) {
			return uri.removeSearch("narrow", facetId);
		}, uri);
		
		return uriWithoutFacets;
	}

	function addAllFacetsToURI($facetGroup, uri) {
		uri = uri || URI(window.location.href);

		var facetIds = allFacetIdentifiers($facetGroup);
		
		// Add all facets from window.location.href
		var uriWithFacets = _.reduce(facetIds, function(uri, facetId) {
			return uri.addSearch("narrow", facetId);
		}, uri);
		
		return uriWithFacets;
	}

	function updateURIForFacetState($facetCheckbox, uri) {
		uri = uri || URI(window.location.href);
		
		var isChecked = $facetCheckbox.attr("checked");

		// get the name:value string representing the facet
		var facetId = facetIdentifier($facetCheckbox);

		// Update the current location to add or remove the facet
		if(isChecked) {
			uri.addSearch("narrow", facetId);
		}
		else {
			uri.removeSearch("narrow", facetId);
		}

		// Navigate to the new location with the facet added/removed
		return uri;
	}

	/**
	 * Gets the query param value for name in uri, consistently as a list of 
	 * values.
	 */
	function getQueryVal(uri, name) {
		var value = uri.query(true)[name];

		if(_.isArray(value))
			return value;
		if(value === undefined)
			return [];
		return [value];
		
	}

	/**
	 * Updates the narrow= query params to reflect the specified term tab.
	 */
	function updateURIForTermFacet($tab, uri) {
		uri = uri || URI(window.location.href);

		var query = uri.query(true);
		var narrow = getQueryVal(uri, "narrow");

		// Remove existing term narrowing values
		narrow = _.reject(narrow, function(val) {
			return /^term-/.test(val);
		});

		// add the new term
		narrow.push("term-" + $tab.attr("data-term-name"));
		query["narrow"] = narrow;
		return uri.query(query);
	}

	// Watch for clicks on the "More" link on facet groups
	$(".facets").on("click", ".show-more-facets", function() {
		var $more = $(this).closest(".section-facets").find(".more-facets");
		if($more.is(":hidden")) {
			$more.slideDown();
			$(this).text("Less");
		} else {
			$more.slideUp();
			$(this).text("More");
		}
	});

	// Watch for clicks on the "clear" link on facet groups
	$(".facets").on("click", ".ic-deselect", function() {
		window.location.href = clearAllFacetsFromURI(
			$(this).closest(".section-facets"));
	});

	// Watch for clicks on the "Select all" link on facet groups
	$(".facets").on("click", ".ic-select", function() {
		window.location.href = addAllFacetsToURI(
			$(this).closest(".section-facets"));
	});

	// Watch for facet values being selected/deselected
	$(".facets").on("change", "input[type=checkbox]", function(event) {
		window.location.href = updateURIForFacetState($(this));
	});

	// Watch for facet values being selected/deselected
	$(".tabs").on("click", ".tab a", function(event) {
		window.location.href = updateURIForTermFacet($(this).closest(".tab"));
	});

	/**
	 * Starts an ajax request to fetch more results from the current page.
	 *
	 * Returns: A jqXHR object for the ajax request.
	 */
	function fetchMoreResults(resultOffset) {
    	// Note that the currentPageURI conatins current filter settings
		var currentPageURI = URI();
		// append "more" to the path component of the current URI
		currentPageURI.segment(_.flatten([currentPageURI.segment(), ["more"]]));
		// Return the jqxhr so that the caller can bind done, fail, always funcs
		return $.get(String(currentPageURI), {start: resultOffset});
	}

	function hideResultFetchLoader() {
		$(".section-showmore a")
			.animate({"padding-right": "-=36"}, 400, "easeInOutCubic")
			.removeClass("loading");
	}

	function showResultFetchLoader() {
		$(".section-showmore a")
			.animate({"padding-right": "+=36"}, 400, "easeInOutCubic")
			.addClass("loading");
		var $error = $(".section-showmore .error");
		if(!$error.is(":hidden")) {
			$error.slideUp();
		}
	}

	function onMoreResultsReceived(results) {
		var $results = $(results).find(".results-list > li");
		if($results.length > 0) {
			insertResults($results);
		}

		if($results.length < 20) {
			$(".section-showmore a").text("No more results.")
				.addClass("btn-inactive");
		}
	}

	function showMoreResultsErrorMessage() {
		var $error = $(".section-showmore .error");
		$error.slideDown();
	}

	function insertResults($results) {
		var maxresults = 20;

		var $resultsList = $(".results-list");
		var $resultsListParent = $resultsList.parent(".result-list-animator");

		// Fix the height at the current height
		$resultsListParent.height($resultsListParent.height());
		$resultsList.append($results);
		
		// Animate the parent's height to it's full height
		
		// At least 500ms + 0-500 more depending on the result count
		var duration = 500 + (500 * Math.max($results.length / maxresults, 1));
		$resultsListParent.animate({height: $resultsList.height()},
			duration, "easeInOutCubic", function() {
				// Stop overriding height directly on the element
				$resultsListParent.height("");
			});
	}

	// Watch for clicks on show more
	$(".section-showmore a").on("click", function() {
		var $this = $(this);
		if($this.hasClass("loading") || $this.hasClass("btn-inactive"))
			return;
		
		showResultFetchLoader();

		var currentCount = $(".results-list > li").length;

		fetchMoreResults(currentCount)
			.done(onMoreResultsReceived)
			.fail(showMoreResultsErrorMessage)
			.always(hideResultFetchLoader);
	});
    
    
    /**
     * Activate the date dropdown
     */
    views.enableTopNav(function( start_year, org_code ){
    	var uri = new URI();
    	var target = uri.segment(0);
    	var search = uri.search();
    	uri = uri.segment( [ target, encodeURIComponent( start_year ), encodeURIComponent( org_code ) ] );
    	uri = uri.search( search );
    	return uri;
    });
});
