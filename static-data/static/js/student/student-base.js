define(["jquery",
        "bootstrap/personal_timetable",
        "json2"], 
        function($, pt){
	"use strict";
	
	/**
	 * Loads HTML into sections of the page using AJAX
	 * HTML replaces content of dest
	 * Might be useful to move this to a more general utility library 
	 */
	var asyncSetFragment = function(base, name, params, dest, callback){
		// ensure name is an array for iterating over
		if( typeof name === 'string' ) {
		    name = [ name ];
		}
		
		// construct the URL
		var url = "/"+encodeURIComponent(base)+"/f/";
		for( var i = 0; i < name.length; i++ ){
			if( typeof name[i] !== "undefined" ) // don't allow undefined variables - avoids accidental passing of invalid values
			{
				url = url+encodeURIComponent(name[i])+"/";
			}
		}

		// get AJAX response
        var jqxhr = $.get( url, params );
        jqxhr.done(function(htmlFragment){
            $(dest).html( htmlFragment );
            if( callback !== undefined )
            {
            	callback();
            }
        });
        return jqxhr;
    };
	
	
    /************************
     * Personalised timetable
     ************************/
    
    // Update the personal timetable notification settings when the
    // notify checkbox is clicked:
    $('#notify-checkbox').click(function() {
        console.log("email notification changed");
    	$.ajax({
            type: 'POST',
            url: "/timetable/notifications",
            data: { notify:$('#notify-checkbox').is(':checked'),
                    email:$('#personalisedtimetable-notify-address').val() }
    		});
    });

    // When you click on the notify email Edit link then apply a border to the input
    // field and make it editable and give it focus:
    $('#notify-email-edit-link').click(function() {
        console.log("email notification address edit");
        $('#personalisedtimetable-notify-address').css({ 'border': '1px solid black'});
        $('#personalisedtimetable-notify-address').attr('readonly',false);
        $('#personalisedtimetable-notify-address').focus();
    });

    // When you click away from the email field then submit the values to
    // the server in a POST request:
    $('#personalisedtimetable-notify-address').blur(function() {
        console.log("email notification address edit done");
        $('#personalisedtimetable-notify-address').css({ 'border': 'none'});
        $('#personalisedtimetable-notify-address').attr('readonly',true);
    	$.ajax({
            type: 'POST',
            url: "/timetable/notifications",
            data: { notify:$('#notify-checkbox').is(':checked'),
                    email:$('#personalisedtimetable-notify-address').val() }
    		});
    });

    
    // open / close timetable
    $('.btn-personalisedtimetable').click(function(e){
    	// prevent link going anywhere
        e.preventDefault();
        
    	// open the timetable
        $('.section-personalisedtimetable-view').slideToggle('fast', timetable_toggle_callback);
    });
    var timetable_toggle_callback = function(){
    	if( $('.section-personalisedtimetable-view').is(":visible") ) {
    		// load the timetable
        	asyncSetFragment( "events", [ "personal-timetable", $( "input#ref_username" ).val(), $( "select#select-date" ).val() ], {}, "div.section-personalisedtimetable-view-content", timetable_toggle_callback_callback);
    	}
    }
    var timetable_toggle_callback_callback = function(){ // this is getting silly :|
        pt_select_term_tab( "michaelmas" );
    }
    
    
    // store for the last clicked add / remove button
    var action_link; // the last add / remove button clicked
    var do_collision_check = true; // whether the collision test should be performed on submit
    
    // Functionality for adding all visible event series to a personal timetable
    $('#add-all-lectures').click(function() {
    	if( !userLoggedIn ){
    		show_dialog($(this), 72, 8, 'popup_login');
    		return false;
    	}
    	
        action_link = $(this);
        $(this).addClass( "loading" );
    	
        var go = true;
        if( do_collision_check ) { // if !do_collision_check then we are resubmitting so don't bother asking
   			go = confirm("Add all events to timetable? Are you sure you want to add all of the listed events to your timetable?"); // TODO - need to tart this up (also "event series" below)
    	}
    
   		if(go){
   			$.ajax({
   	            type: 'POST',
   	            url: location.pathname + "/series_ids/" + location.search,
   	            data: { "do_collision_check": do_collision_check },
   	            success: function( json ){
   	            	refresh_page( json, "add" );
   	            }
   			});
   		} else {
   			$(this).removeClass( "loading" );
   		}
	    
	    do_collision_check = true;
    });

    
    // add all event series in an event group
	$("ul.results-list").on("click", "a.group-lectures.add", function() {
		add_remove_event_group( $(this), "add" );
    });
	
	// remove all event series in an event group
    $("ul.results-list").on("click", "a.group-lectures.remove", function() {
    	add_remove_event_group( $(this), "remove" );
    });
    
    // add individual event series
    $("ul.results-list").on("click", "a.series-lectures.add", function() {
    	add_remove_event_series( $(this), "add" );
    });
    
    // remove individual event series
    $("ul.results-list").on("click", "a.series-lectures.remove", function() {
    	add_remove_event_series( $(this), "remove" );
    });
    
    // remove individual event series from personal timetable directly
    $("div.section-personalisedtimetable-view").on("click", "a.ic-remove", function() {
   		add_remove_event_series( $(this), "remove" );
    });
    
    
    // add or remove all of the event series in an event group group
    function add_remove_event_group( link, action ) {
    	if( !userLoggedIn ){
    		show_dialog(link, 72, 8, 'popup_login');
    		return false;
    	}
    	
    	action_link = link;
    	link.addClass( "loading" );
    	
    	var go = true;
    	if( action == "remove" ) {
    		go = confirm("Remove events from timetable? Are you sure you want to remove these events from your timetable?"); // TODO - need to tart this up (also "event series" below)
    	}
    
    	if(go){
	    	var id_groupusage = link.find( "input.id_groupusage" ).val();
	    	
	    	// get all of the event series contained by this group
	    	var ids_eventseries = $( "ul#series-list-"+id_groupusage+" input.id_eventseries" );
	    	
	    	// construct the string representation of the eventseries / groupusage ID pairs to pass via ajax 
	    	var tuples = ""
			var tuples_array = [];
			$.each( ids_eventseries, function( index, input ){
				var id_eventseries = $(input).attr("value");
				tuples_array.push("["+id_eventseries+", "+id_groupusage+"]");
			});
			tuples = "["+tuples_array.join(",")+"]"
	
			// execute AJAX call
			if( tuples != "" ) {
	    		$.ajax({
	                type: 'POST',
	                url: location.pathname + "/" + action + "/" + location.search,
	                data: { "series_groups": tuples, "do_collision_check": do_collision_check },
	                success: function( json ){
	                	refresh_page( json, action );
	                }
	    		});
	    	}
    	} else {
   			link.removeClass( "loading" );
   		}
    	
    	do_collision_check = true;
    }

    
    // add or remove an individual event series
    function add_remove_event_series( link, action ) {
    	if( !userLoggedIn ){
    		show_dialog(link, 72, 8, 'popup_login');
    		return false;
    	}
    	
    	action_link = link;
    	link.addClass( "loading" );
    	
    	var go = true;
    	if( action == "remove" ) {
    		go = confirm("Remove event from timetable? Are you sure you want to remove this event from your timetable?");
    	}
    	
    	if(go){
	    	var id_groupusage = link.find( "input.id_groupusage" ).val();
	    	var id_eventseries = link.find( "input.id_eventseries" ).val();
	    	
	    	var action_data = "[["+id_eventseries+", "+id_groupusage+"]]"
	    	
	    	$.ajax({
	            type: 'POST',
	            url: location.pathname + "/" + action + "/" + location.search,
	            data: { "series_groups": action_data, "do_collision_check": do_collision_check },
	            success: function( json ){
	            	if( link.hasClass("ic-remove") ) {
	            		link.parent().remove();
	            	}
	            	refresh_page( json, action );
	            }
			});
    	} else {
   			link.removeClass( "loading" );
   		}
    	
    	do_collision_check = true;
    }
    
    
    // utility function to refresh entire page - call on ajax success after add / remove event series instead of each function individually
    function refresh_page( json, action ) {
    	var personal_timetable = json; // default page load
    	if( typeof json["pt"] !== "undefined" ) { // ajax return
    		personal_timetable = json["pt"];
    	}
    	
    	// TODO - move collisions and actions to seperate handlers, etc
    	if( typeof json["collisions"] !== "undefined" ) { // we have collisions - ask for confirmation
    		// populate collision warning
    		$("#popup_collision ul.popup-problems-list").remove();
    		$("#popup_collision div.popup-content-head").after(json["collisions"]["html"]);
    		
    		// display warning
    		show_dialog(action_link, 72, 8, 'popup_collision');
    	}

    	if( typeof json["actions"] !== "undefined" ) { // we have actions to display :|
    		var actioned = json["actions"][0]; // contents in second element did not get actioned
    		
    		$.post(
    			"/f/personal-timetable/action-notification/",
    			{ "actioned": JSON.stringify(actioned), "action": action },
    			function( html ){
    				$('.section-personalisedtimetable-content-container').append(html);
    				$('.section-personalisedtimetable-dialog.new').removeClass("new").slideDown('fast').delay(5000).slideUp('fast', function(){
    	            	$(this).remove();
    	            });
    			}
    		);
    	}
    	
    	set_add_remove_buttons( personal_timetable );
    	pt_set_event_count( personal_timetable );
    }

    
    // check for whether buttons should say "remove"
    function set_add_remove_buttons( personal_timetable )
    {
    	if( typeof action_link !== "undefined" ) {
    		action_link.removeClass( "loading" );
    	}
    	
    	// tracking
    	var series_count = {};
    	var event_total = 0;
    	
    	// go through all of the event series buttons and check whether their corresponding event series has already been added to the user's personal timetable
    	$("a.series-lectures").each( function(){
    		var button = $(this);
    		var id_groupusage = parseInt( button.find( "input.id_groupusage" ).val() );
        	var id_eventseries = parseInt( button.find( "input.id_eventseries" ).val() );
        	
        	// count how many event series there are in this group
        	if( typeof series_count[id_groupusage] === "undefined" ) {
        		var total = button.parents( "ul.series-list" ).find("li").size();
        		series_count[id_groupusage] = {};
        		series_count[id_groupusage]["total"] = total;
        		series_count[id_groupusage]["added"] = 0;
        	}
        	
        	// set event series button text
        	var set_add_button = true;
        	if( typeof personal_timetable[id_groupusage] !== "undefined" ) { // pt is personal timetable object passed from student-base bootstrap
        		if( $.inArray( id_eventseries, personal_timetable[id_groupusage] ) > -1 ) { // set remove button
        			button.find("span").text("Remove");
        			button.removeClass("add").addClass("remove");
    				series_count[id_groupusage]["added"]++;
    				event_total++;
    				set_add_button = false;
        		}
        	}
        	if( set_add_button ) { // set add button - easist to use flag as there are multiple conditions where buttons should be set to add state
        		button.find("span").text("Add");
    			button.removeClass("remove").addClass("add");
        	}
        });
    	
    	// go through all of the event group buttons and check whether all of their events have been added to the personal timetable
    	$("a.group-lectures").each( function(){
    		var button = $(this);
    		var id_groupusage = button.find( "input.id_groupusage" ).val();
    		
    		var s = button.find("span");
    		var t = s.text();
    		
    		if( series_count[id_groupusage]["total"] == series_count[id_groupusage]["added"] ) { // set remove button
    			t = t.replace( "Add", "Remove" );
    			button.find("span").text( t );
    			button.removeClass("add").addClass("remove");
    		} else { // set add button
    			t = t.replace( "Remove", "Add" );
    			button.find("span").text( t );
    			button.removeClass("remove").addClass("add");
    		}
    	});
    }
    
    // set the event counter on the personal timetable
    function pt_set_event_count( personal_timetable ) {
    	var event_total = 0;
    	
    	$.each( personal_timetable, function( id_groupusage, ids_eventseries ){
    		event_total = event_total + ids_eventseries.length;
    	});
    	
    	// set the number of events in the personal timetable
    	$( "div.btn-personalisedtimetable span.counter" ).text( "("+event_total+")" );
    }
    
    
    /*************************************************
     * dialogs (collision, login, export personal timetable, event groups metadata)
     *************************************************/
    var shut_popup = false;
    
    // show the dialog
    function show_dialog(target, offsetX, offsetY, id_dialog, shut_after) {
    	if( shut_after == "undefined" ) { // only metadata popups set shut_after
    		shut_after = false;
    	}
    	
   		shut_popup = false;
    	
    	offsetX = offsetX || 0;
        offsetY = offsetY || 0;
        
    	var popup = $( '#'+id_dialog );
    	$( '#'+id_dialog+' div.expand' ).hide();
    	
    	popup.show();
    	
    	var popupOffset = target.offset();
        popupOffset.top = popupOffset.top + $(target).height() + offsetY;
        popupOffset.left = popupOffset.left - popup.width() + offsetX;
        popup.offset( popupOffset );

        $( '#'+id_dialog+' div.expand' ).slideDown( 'fast', function(){
        	if( shut_after ) { // popup has been closed before it could be opened(!)
        		hide_dialog( id_dialog );
        	}
        });
    }
    
    // hide the dialog
    function hide_dialog( id_dialog ) {
    	$( '#'+id_dialog+' div.expand' ).slideUp( 'fast', function(){
    		$( '#'+id_dialog ).hide();
    	});
    	shut_popup = true;
    }
    
    // dialog button handler
    $('#popup_collision a').click( function(e) {
    	e.preventDefault();
    	
    	if( $(this).hasClass("add") ) {
    		do_collision_check = false; // we are resubmitting the data so do not check for collisions - this only happens if the user has confirmed acceptance of the clashes
    		action_link.click();
    	}
    	
    	hide_dialog( "popup_collision" );
    });
    
    $('#popup_login a.cancel').click( function(e) {
    	e.preventDefault();
    	hide_dialog( "popup_login" );
    });
    
    
    // open or hold open the personal timetable help dialog
    var timer_popup;
    $( "a.ic-export, div#popup_export_help" ).mouseover( function(e){ // mouse over
    	if( timer_popup !== "undefined" ) {
    		clearTimeout(timer_popup);
    	}
    	if( $(this).hasClass( "ic-export" ) ){
    		show_dialog( $(e.currentTarget), 58, 0, "popup_export_help" );
    	}
    }).mouseout( function(e){ // mouse out
    	timer_popup = setTimeout(
    		function(){ hide_dialog( "popup_export_help" ); }, 1000
    	);
    });
    
    
    // event groups metadata popup
    $( "a.show_metadata" ).live( "click", function(e){
    	e.preventDefault();
    }).live( "mouseover", function(e){
    	show_metadata($(this), 72, 1);
    }).live( "mouseout", function(e){
    	hide_dialog( "popup_metadata" );
    });
    
    // show the dialog
    function show_metadata(target, offsetX, offsetY) {
    	shut_popup = false;
    	
    	offsetX = offsetX || 0;
        offsetY = offsetY || 0;

        $( '#popup_metadata div.popup-content-head' ).remove();
        $( '#popup_metadata div.details' ).remove();
        
        var id_subject = target.find( "input.id_subject" ).val();
        
        $.ajax({
            type: 'POST',
            url: "/f/details/subject/"+id_subject+"/" + location.search,
            data: {},
            success: function( html ){
            	$( '#popup_metadata div.popup-content' ).html(html);
            	
            	show_dialog(target, offsetX, offsetY, "popup_metadata", shut_popup);
            }
		});
    }
    
    
    /***************
     * term selector
     ***************/
    $("div.section-personalisedtimetable-view a.term-selector").click( function(e){
    	e.preventDefault();
    	
    	var target = $(this);
    	
    	$("div.section-personalisedtimetable-view a.term-selector").parent().removeClass("nav-selected");
    	target.parent().addClass("nav-selected");
    	
    	if( target.hasClass("michaelmas") ) { // ARGH!
    		pt_select_term_tab( "michaelmas" );
    	} else if( target.hasClass("lent") ) {
    		pt_select_term_tab( "lent" );
    	} else if( target.hasClass("easter") ) {
    		pt_select_term_tab( "easter" );
    	}
    });
    function pt_select_term_tab( term ) {
    	$("ul.lecture-data li").hide();
    	$("ul.lecture-data li."+term).show();
    }
    
    
    /**
     * initial run of the refresh page code - needed most of the time except on student home
     */
    refresh_page( pt );
});

