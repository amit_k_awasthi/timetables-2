/**
* Select box skinning
*/
$(function() {

    /*
    $(document).ready(function() {
        applySkin('#select-subject', '#select-subject-skin');
        applySkin('#select-date', '#select-date-skin');
    });
    
    applySkin = function(selectElement, selectSkin) {
        updateName(selectElement, selectSkin);
        
        var select = $(selectElement);
        select.change(function(){ 
            updateName(selectElement, selectSkin);
        });
    };
    
    function updateName(selectId, selectSkinId) {
        var name = $(selectId + " > option" + ':selected').val();
        $(selectSkinId).find('span').text(name);
    }
    */
    
    // trigger lectures dropdown
    $('#results div.results-wrapper .section-expandcollapse>a').click(function(e){
        $(this).toggleClass('ic-down');
        $(this).toggleClass('ic-up');
        
        $(this).next().slideToggle('fast');
        
        e.preventDefault();
    });
    
   
    // POPUPS
   
    function removeAllPopups() {
        $('.popup').slideUp('fast', function(){$(this).remove();});
    }
    
   
    function addPopup(popupHTML, target, offsetX, offsetY) {
        popupOffset = $(target).offset();
        offsetX = offsetX || 0;
        offsetY = offsetY || 0;
        
        removeAllPopups();
        $('body').append(popupHTML);
        popupOffset.top = popupOffset.top + $(target).height() + offsetY;
        popupOffset.left = popupOffset.left - $('body').children('.popup').last().width() + offsetX;
        $('.popup').offset(popupOffset);
        $('.popup').css({
            display: 'none'
        }).slideDown('fast');
    }
   
    
    //hide popups and dialogs by default
    $('.section-personalisedtimetable-view, .section-personalisedtimetable-dialog, .popup, #admin-event-editor').css({
        display: 'none'
    });
    
    //open admin edit event dialog
    $('#results div.results-wrapper ul.results-list div.result-head>h3 a').click(function(e){
        
        var currentScrollPosition = $('body').scrollTop();
        $('html').css('overflow', 'hidden').scrollTop(currentScrollPosition);
        
        $('#admin-event-editor').css({
            position: 'absolute',
            left: $(window).width()/2 - $('#admin-event-editor').width() /2,
            top: currentScrollPosition + $(window).height()/2 - $('#admin-event-editor').height() /2,
            zIndex: 10000
        }).slideDown('fast');
        
        $('body').append('<div id="overlay"></div>');
        $('#overlay').css({
            display: 'none',
            backgroundColor: '#000000',
            opacity: 0.3,
            width: '100%',
            height: '100%',
            position: 'absolute',
            top: currentScrollPosition,
            left: 0,
            zIndex: 9999
        }).fadeIn('fast');
        
        e.preventDefault();
    });
    
    //overlay for the other content on the page when "edit" event is clicked
    $('#admin-event-editor ul a, #overlay').click(function(e){
        $('#admin-event-editor').slideUp('fast');
        $('#overlay').fadeOut('fast');
        $('html').css('overflow', 'inherit');
        e.preventDefault();
    });
    
});