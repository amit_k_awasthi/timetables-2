var console;

define(["jquery", 
        "underscore", 
        "backbone", 
        "assert", 
        "backbone-utils",
        "validate"], 
        function($, _, Backbone, assert, backbone_utils, validate) {
    "use strict";
    
    var superclass = backbone_utils.superclass;
    var superproto = backbone_utils.superproto;
    
    var ValidityReportingModel = Backbone.Model.extend({
        constructor: function TimetablesModel() {
            superclass(TimetablesModel).apply(this, arguments);
        },
        
        initialize: function() {
            _.bindAll(this, "recheckValidity");
            this._isValid = this.isValid();
            this.on("change", this.recheckValidity);
        },
        
        recheckValidity: function() {
            var wasValid = this._isValid;
            this._isValid = this.isValid();
            if(wasValid !== this._isValid) {
                this.trigger("validity_change", this._isValid);
            }
        },
        
        // Provide a default implementation of validate.
        validate: function(){}
    });
    
    var Classifier = ValidityReportingModel.extend({
        constructor: function Classifier() {
            superclass(Classifier).apply(this, arguments);
        },
        
        defaults: {
            id: null,
            value: null
        },
        
        validate: function(attrs) {
            return validate.notEmpty("Value", attrs.value);
        }
    });
    
    var Classifiers = Backbone.Collection.extend({
        constructor: function Classifiers() {
            superclass(Classifiers).apply(this, arguments);
        },
        
        model: Classifier
    });
    
    var ClassifierGroup = ValidityReportingModel.extend({
        constructor: function ClassifierGroup() {
            superclass(ClassifierGroup).apply(this, arguments);
        },
        
        defaults: function() {
            return {
                id: null,
                name: "",
                type: null,
                classifiers: new Classifiers()
            };
        },
        
        initialize: function() {
            superproto(ClassifierGroup).initialize.apply(this, arguments);
            this.classifiers().on("validity_change add remove", 
                    this.recheckValidity);
        },
        
        classifiers: function() {
            return this.get("classifiers");
        },
        
        addClassifier: function(classifier) {
            this.get("classifiers").add(classifier);
        },
        
        toJSON: function() {
            return _.extend(Backbone.Model.prototype.toJSON.call(this), {
                classifiers: this.classifiers().toJSON()
            });
        },
        
        validate: function(attrs) {
            var error = validate.notEmpty("Name", attrs.name) ||
                   validate.notEmpty("Type", attrs.type);
            if(error) return error;
            if(attrs.classifiers.length == 0) 
                return "classifiers is empty";
             if(attrs.classifiers.any(function(c){
                       return !(c.isValid()); }))
                 return "All classifiers must be valid.";
        }
    });
    
    var ClassifierGroups = Backbone.Collection.extend({
        constructor: function ClassifierGroups() {
            superclass(ClassifierGroups).apply(this, arguments);
        },
        
        model: ClassifierGroup
    });
    
    var Faculty = ValidityReportingModel.extend({
        constructor: function Faculty() {
            superclass(Faculty).apply(this, arguments);
        },
        
        defaults: function() {
            return {
                id: null,
                name: "",
                code: "",
                year: null,
                classifier_groups: new ClassifierGroups()
            };
        },
        
        initialize: function() {
            superproto(Faculty).initialize.apply(this, arguments);
            this.classifierGroups().on("validity_change add remove",
                    this.recheckValidity);
        },
        
        classifierGroups: function() {
            return this.get("classifier_groups");
        },
        
        addClassifierGroup: function(group) {
            this.classifierGroups().add(group);
        },
        
        toJSON: function() {
            var attrs = this.attributes;
            return {
                id: attrs.id,
                name: attrs.name,
                code: attrs.code,
                year: attrs.year.get("start_year"),
                classifier_groups: this.classifierGroups().toJSON()
            };
        },
        
        validate: function(attrs) {
            var groups = attrs.classifier_groups;
            var error = validate.notEmpty("Name", attrs.name) ||
                        validate.notEmpty("Code", attrs.code);            
            if(error) return error;
            if(groups.length < 2) 
                return "At least two classifier groups are required.";
            if(groups.any(function(g) { return !(g.isValid()); }))
                return "Classifier groups must be valid."
             
                //if(!groups.filter(function(group){ return group.get("type") === "level" }))
        }
    });
    
    /**
     * A FacultyAffiliation represents an association between an Administrator
     * and a Faculty. This can confer upon the admin permission to manage the 
     * Faculty.
     */
    var FacultyAffiliation = Backbone.Model.extend({
        constructor: function FacultyAffiliation() {
            superclass(FacultyAffiliation).apply(this, arguments);
        },
        
        defaults: function() {
            return {
                /** The Faculty this affiliation is with. */
                faculty: null,
                
                /** The Admin this affiliation is with. */
                administrator: null,
                
                /** Whether the admin is a key administrator of this Faculty. */
                is_key_admin: false,
                
                /** 
                 * Whether the admin is permitted to edit administrators of
                 * the faculty associated with this affiliation. 
                 */
                can_edit_admins: false,
                
                /**
                 * The set of permissions held by the associated admin with the
                 * associated faculty. The set may contain any of:
                 * read, write, publish, add_classifications
                 */
                permission_set: [],
                
                /**
                 * The set of Classifiers this admin has permission in.
                 */
                classifier_set: new Classifiers()
            };
        },
        
        initialize: function() {
            
        },
        
        validate: function() {
            return !this.get("faculty") ? "No faculty" :
                   !this.get("administrator") ? "No administrator" :
                   undefined;
        }
    });
    
    /**
     * Represents a member of Administration staff in the Timetables system.
     */
    var Administrator = Backbone.Model.extend({
        constructor: function Administrator() {
            superclass(Administrator).apply(this, arguments);
        },
        
        defaults: function() {
            return {
                name: "",
                email: "",
                is_superadmin: false,
                is_disabled: false,
                affiliations: new Backbone.Collection()
            };
        },
        
        initialize: function() {
            
        },
        
        validate: function() {
            return validate.notEmpty("Name", name) ||
                   validate.notEmpty("Email", email) ||
                   validate.looksLikeEmail("Email", email);
        }
    });
    
    return {
        Faculty: Faculty,
        ClassifierGroup: ClassifierGroup,
        Classifier: Classifier,
        Administrator: Administrator,
        FacultyAffiliation: FacultyAffiliation
    };
});