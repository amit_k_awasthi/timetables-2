var console;

define(["superadmin/administrator-form",
        "domReady"], 
        function(adminForm) {
    "use strict";
    
    adminForm.setupAdminForm(false);
});