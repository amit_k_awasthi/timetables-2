define(["jquery",
        "underscore",
        "admin/validation",
        "jquery-ui/dialog",
        "jquery-sort"
        ],
        function($, _, validation){
	"use strict";
	
	/**
	 * Async load a fragment and send it back to a function on success.
	 */
    var asyncAddFragment = function(name, params, dest, onPostInsert) {
        var jqxhr = $.get("/superadministration/faculties/f/" 
            + encodeURIComponent(name) + "/", params);
        jqxhr.done(function(htmlFragment){
            var fragment = $(htmlFragment).appendTo($(dest));
            if(onPostInsert)
                onPostInsert(fragment);
        });

        return jqxhr;
    }

    /**
     * Check the contents of the group, re triggering any validation.
     */
    var updateGroupEmptyStatus = function(group) {
        if($(group).find("tbody").length === 0)
            $(group).addClass("empty");
        else
            $(group).removeClass("empty");
        // Make the validation revalidate our group.
        $(group).find("input.workaround").trigger("change");
    }


    /**
     * Load the classification dialog, using the classifier as a source.
     * If a new , then classifier is false.
     */
    var loadClassifcationDialog = function(dialog, classifier ) {
        $(dialog).find("select[name=org_level] option").each(function() {
        	if (  !$(this).hasClass("_keep") ) {
        		$(this).remove();
        	}
        });
        // populate the level select list from a template
        var template = new String($(dialog).find("select[name=org_level]").next().html());
        var parent_level = $(classifier).find(".name input[name^=parent]").val();
    	
        $('#classifier-groups .level-classifiers .classifier').each(function() {
        	if ( $(this).is(":visible") ) {
	        	var value = $(this).find(".name input[name^=value]").val();
	        	var option = template.replace(/__level_value__/g,value);
	        	var level_id = $(this).find(".name input[name^=id]").val();
	        	option = option.replace(/__level_id__/g,level_id);
	        	if ( parent_level === level_id ) {
	        		option = option.replace(/__level_selected__/g,'selected="selected"');
	        	} else {
	        		option = option.replace(/__level_selected__/g,' ');
	        	}
	        	$(dialog).find("select[name=org_level]").append(option)
        	}
    	})
    	if (! classifier ) {
        	// clear everything else.
            $(dialog).find('input').val("");
            $(dialog).find('textarea').val("");    		
    	} else {
        	// set everything else.
            $(dialog).find('input[name=classification-name]').val($(classifier).find(".name input[name^=value]").val());
            $(dialog).find('input[name=classification-contact_person]').val($(classifier).find(".name input[name^=contact_person]").val());
            $(dialog).find('input[name=classification-website]').val($(classifier).find(".name input[name^=website]").val());
            $(dialog).find('textarea[name=classification-description]').val($(classifier).find(".name input[name^=description]").val());
    	}

    }
    
    /**
     * save a classification from the dialog.
     * if group is a jQuery object and classifier is false, then create a new classifier and add it to the end of group.
     * if classifier is a jQuery object, then update the classifier from the dialog.
     * 
     */
    var saveClassificationDialog = function(dialog, group, classifier) {
        var name = $(dialog).find('input[name=classification-name]').val();
        var level_id = $(dialog).find('.classification-selector option:selected').val();
        var contact_person = $(dialog).find('input[name=classification-contact_person]').val();
        var website = $(dialog).find('input[name=classification-website]').val();
        var description = $(dialog).find('textarea[name=classification-description]').val();
        
        
        
        if ( ! classifier ) {
            var dest = $(group).find("tbody");
            var params = {
                group_index: _.indexOf($(group).parent().children(), $(group)[0]),
                classifier_index: $(dest).children().length,
            };
            asyncAddFragment("classifier", params, dest, function(newclassifier) {
                updateGroupEmptyStatus(group);
                $(newclassifier).find(".name input[name^=value]").val(name);
                $(newclassifier).find(".name input[name^=parent]").val(level_id);
                $(newclassifier).find(".name input[name^=contact_person]").val(contact_person);
                $(newclassifier).find(".name input[name^=website]").val(website);
                $(newclassifier).find(".name input[name^=description]").val(description);
                $(newclassifier).find(".name").find("span").text(name)        	
            });
        } else {
        	if ( $(classifier).length !== 1 ) {
        		alert("Got more than one classification to update, not performing action ");
        	} 
        	if ($(classifier).find(".name input[name^=value]").length !== 1 ) {
        		alert("Got more than one classification to update, not performing action ");        		
        	}
        	$(classifier).find(".name input[name^=value]").val(name);
        	$(classifier).find(".name input[name^=parent]").val(level_id);
            $(classifier).find(".name input[name^=contact_person]").val(contact_person);
            $(classifier).find(".name input[name^=website]").val(website);
            $(classifier).find(".name input[name^=description]").val(description);
        	$(classifier).find(".name").find("span").text(name);        	
        }
        	

    }
    /**
     * Load the level dialog from a level, if classifier is false, then empty the dialog before opening.
     */
    function loadLevelDialog(dialog, level) {
    	if (! level ) {
        	// clear everything else.
            $(dialog).find('input').val("");
            $(dialog).find('textarea').val("");    		
    	} else {
        	// set everything else.
            $(dialog).find('input[name=name]').val($(level).find(".name input[name^=value]").val());
    	}
    }

    /**
     * Save the level dialog. If group is JQ Object and classifier is false, then create a new level.
     * If classifier is not false, then update that level.
     */
    function saveLevelDialog(dialog, group, level) {
        var name = $(dialog).find('input[name=name]').val();
        if ( ! level ) {
            var dest = $(group).find("tbody");
	        var params = {
	            group_index: _.indexOf($(group).parent().children(), $(group)[0]),
	            classifier_index: $(dest).children().length
	        };
	        asyncAddFragment("classifier", params, dest, function(newclassifier) {
	            updateGroupEmptyStatus($(group));
	        	$(newclassifier).find(".name input[name^=value]").val(name);
	            $(newclassifier).find(".name").find("span").text(name);
	        });
    	
        } else {
        	if ( $(level).length !== 1 ) {
        		alert("Got more than one level to update, not performing action ");
        	} 
        	if ($(level).find(".name input[name^=value]").length !== 1 ) {
        		alert("Got more than one level to update, not performing action ");        		
        	}
        	$(level).find(".name input[name^=value]").val(name);
            $(level).find(".name").find("span").text(name);        	
        }

    }

    /**
     * Delete the group.
     */
    var deleteGroup = function (group) {
        // Delete each classifier under the group
        $(group).find(".classifier").each(function(i, elem){
            deleteClassifier($(elem));
        });

        // Delete the group itself
        var hasId = $(group).children("input[name^=id]").val();
        if(!hasId)
            $(group).remove();
        else
            $(group).addClass("hidden")
                .children("input[name^=deleted]").val(true);
    }

    /**
     * Delete the classiier
     * @param classifier
     */
    var deleteClassifier = function (classifier) {
        // If we're deleting a classifier with an ID then it exists in the db,
        // so we need to tell the server that it's been deleted.
        var hasId = $(classifier).find("input[name^=id]").val();
        if(!hasId)
            $(classifier).remove();
        else
            $(classifier).addClass("hidden")
                .find("input[name^=deleted]").val(true);
    }


    // setup the dialogs using the marker class as the selector.
    $(".dialog-container").each(function() {
    	var width = $(this).attr("dialog-width");
    	if ( width === undefined || width === false ) {
    		width = 350;
    	}
    	$(this).dialog({
    		 autoOpen: false,
    	     width: width,
    	     modal: true
    	});
    	var dialog = this;
    	$(this).find(".cancel").click(function(){
    		$(dialog).dialog("close");
        	// Must unbind any save clicks on closing since we re-use the dialogs.
        	$(dialog).find(".save").unbind("click"); 
    	});
    });

    // Bind event handlers:




    // add new classifier group button
    $("#add-classifier-group-button").click(function() {
    	$("#add-new-classification-group-dialog").dialog("open");
    
	    // the save button in add new classifier group
		$("#add-new-classification-group-dialog .save").click(function() {
			$("#add-new-classification-group-dialog .save").unbind("click"); 
			$("#add-new-classification-group-dialog").dialog("close");
	        var dest = $("#classifier-groups");
	        var nextIndex = dest.children().length;
	        var name = $("#add-new-classification-group-dialog").find('input[name=name]').val();
	        asyncAddFragment("group", {index: nextIndex}, dest, function(group) {
	        	 $(group).find(".classifier-group-controls").find("label").text(name);
	        	 $(group).find("input[name^=name]").val(name);
	        });		
		});
    });
	

	  // classifier group's edit (name) button (with save function)
    $("#classifier-groups").on("click",".classifier-group-controls .edit",function(event) {
        var group = $(event.target).parents(".classifier-group");
        var dialog = $("#edit-classification-group-dialog");
        var edit = $(dialog).find('input[name=name]');
        var name = $(group).find(".classifier-group-controls").find("input[name^=name]").val();
        $(edit).val(name);
        $(dialog).dialog("open");
        $(dialog).find(".save").click(function() {
    		$(dialog).dialog("close");
        	$(dialog).find(".save").unbind("click");
            var name = $(edit).val();
	       	$(group).find(".classifier-group-controls").find("label").text(name);
	    	$(group).find("input[name^=name]").val(name);
        });
    });
    
    var confirmDelete = function(dialog, name, deleteFunc) {
        var edit = $(dialog).find('._name');
        console.log("Setting dialog name to "+name);
        $(edit).text(name);
        $(dialog).dialog("open");
        $(dialog).find(".yes").click(function() {
    		$(dialog).dialog("close");
        	$(dialog).find(".no").unbind("click");
        	$(dialog).find(".yes").unbind("click");
        	deleteFunc();
        });
        $(dialog).find(".no").click(function() {
    		$(dialog).dialog("close");
        	$(dialog).find(".no").unbind("click");
        	$(dialog).find(".yes").unbind("click");
        });
    	
    }
    
    // classifier group's delete button
    $("#classifier-groups").on("click", ".classifier-group-controls .delete", function(event) {
        var group = $(event.target).parents(".classifier-group");
        var dialog = $("#confirm-delete-group-dialog");
        var name = $(group).find(".classifier-group-controls").find("input[name^=name]").val();
        confirmDelete(dialog, name, function() {
            deleteGroup(group);
        });
        
    });


    // add new level button
    $("#add-level-button").click(function() {
    	loadLevelDialog($("#add-level-dialog"), false);
    	$("#add-level-dialog").dialog("open");
    });
    
    // save new level
	$("#add-level-dialog .save").click(function() {
		$("#add-level-dialog").dialog("close");
        var group = $('#add-level-button').closest(".classifier-group");
    	saveLevelDialog($("#add-level-dialog"), group, false);
	});

    // level classifier's edit button
    $("#classifier-groups").on("click", ".level-classifiers .edit", function(event) {
        var classifier = $(event.target).parents(".classifier");

        var dialog = $("#edit-level-dialog");
        // populate the form
        loadLevelDialog(dialog, classifier);
  
        $(dialog).dialog("open");
        $(dialog).find(".save").click(function() {
    		$(dialog).dialog("close");
        	$(dialog).find(".save").unbind("click");
        	// save the form
            saveLevelDialog(dialog, false, classifier);
        });        
    });

    // level's delete button
    $("#classifier-groups").on("click", ".level-classifiers .delete", function(event) {
        var classifier = $(event.target).parents(".classifier");
        var dialog = $("#confirm-delete-level-dialog");
        var name = $(classifier).find("input[name^=value]").val();
        confirmDelete(dialog, name, function() {
            deleteClassifier(classifier);
        });
    });

    
    // add new classifier button
    $("#classifier-groups").on("click", ".add-new", function(event) {
        var group = $(event.target).parents(".classifier-group");
        var dialog = $("#edit-classification-dialog");
        // populate the form
        // clear the level select list
        
        loadClassifcationDialog(dialog, false);
    	
    	
          
        $(dialog).dialog("open");
        $(dialog).find(".save").click(function() {
    		$(dialog).dialog("close");
        	// save the form, but only into the parent form
        	saveClassificationDialog(dialog, group, false);
        	$(dialog).find(".save").unbind("click");
        });
        
    });

    // classifier's delete button
    $("#classifier-groups").on("click", ".other-classifiers .delete", function(event) {
        var classifier = $(event.target).parents(".classifier");
        var dialog = $("#confirm-delete-subject-dialog");
        var name = $(classifier).find("input[name^=value]").val();
        confirmDelete(dialog, name, function() {
        	deleteClassifier(classifier);
        });
    });

    // classifier's edit button
    $("#classifier-groups").on("click", ".other-classifiers .edit", function(event) {
        var classifier = $(event.target).parents(".classifier");
        var dialog = $("#edit-classification-dialog");
        // populate the form
        loadClassifcationDialog(dialog, classifier);
  
        $(dialog).dialog("open");
        $(dialog).find(".save").each(function() {
        	$(this).click(function() {
	        	$(dialog).find(".save").unbind("click");
	    		$(dialog).dialog("close");
	        	// save the form
	        	saveClassificationDialog(dialog, false, classifier);
        	})
        });        
    });


    /**
     * 
     */
    var validateClassifierGroup = function(element, feedback, okFunc, badFunc) {
    	var classifier_names = [];
        var classifierCount = 0;
    	var duplicates = 0;
        $(feedback).parents(".classifier-group")
                .find(".classifier").each(function() {
                	classifierCount++;
                	var name = $(this).find(".name input[name^=value]").val();
                	var l = classifier_names.length;
                	var d = 0;
                	for ( var i = 0; i < l; i++ ) {
                		if ( name === classifier_names[i] ) {
                			d++;
                		} 
                		
                	}
                	if ( d == 0 ) {
                		classifier_names.push(name);
                	}
                	duplicates = duplicates + d;
                });
        if(classifierCount === 0 || duplicates !== 0)
            badFunc();
        else
            okFunc();
    }
    validation.add(".validate_classifier_group", validateClassifierGroup);

    // Setup form validation
    validation.bind($("form"));
    var inverse = false;
    // bind to any sortable table header
    $("table.sortable th").click(function(){

        var header = $(this);
        var index = header.index();
        header
            .closest('table')
            .find('td')
            .filter(function(){
                return $(this).index() === index;
            })
            .sortElements(function(a, b){

                a = $(a).text();
                b = $(b).text();
            	if ( a === b) {
            		return 0;
            	}
                return (
                    isNaN(a) || isNaN(b) ?
                        a > b : +a > +b
                    ) ?
                        inverse ? -1 : 1 :
                        inverse ? 1 : -1;

            }, function(){
                return this.parentNode;
            });

        inverse = !inverse;
        $(this).parents("table.sortable").find("th").each(function() {
        	$(this).removeClass("ic-asc-arrow");
        	$(this).removeClass("ic-desc-arrow");
        });
        if ( inverse ) {
        	$(this).addClass('ic-desc-arrow');
        } else {
        	$(this).addClass('ic-asc-arrow');
        }
    });
});