define(["jquery", "jquery-ui/tabs", "domReady"], function($){
	"use strict";
    // TODO: Refactor

    var active = 0;
    try {
    	active = parseInt($('#tabs .active_panel').text());
    } catch(e) {
    	active = 0;
    }
    

     var tabs = $("#tabs").tabs();
     tabs.tabs("select", active);
     $("tr.faculty td span.float-right a.btn-view").die().live("click", function() {
         $(this).closest("tbody").next("tbody").toggle();
         if ($(this).text() === "View") {
             $(this).text("Hide");
             $(this).closest("tbody").css({
                 'background': '#E4E4E4'
             });
         } else {
             $(this).closest("tbody").css({
                 'background': '#FFFFFF'
             });
             $(this).text("View");
         }
         
         return false;
     });
});