define(["underscore",
        "backbone",
        "superadmin/models",
        "viewmodels",
        "backbone-utils",
        "normalise",
        "assert"],
        function(_, Backbone, superadmin_models, viewmodels, backbone_utils,
                normalise, assert) {
    
    var superclass = backbone_utils.superclass;
    var superproto = backbone_utils.superproto;
    
    /**
     * A ViewModel representing the administrator editor used in the add & edit
     * administrator superadmin pages.
     */
    var AdminEditorViewModel = viewmodels.FormViewModel.extend({
        constructor: function AdminEditorViewModel(attrs, adminModel, 
                endpointUrl) {
            
            var nameAttr = viewmodels.ModelAttribute.create(adminModel, "name");
            var emailAttr = viewmodels.ModelAttribute.create(adminModel, "email");
            var isSuperadminAttr = viewmodels.ModelAttribute.create(adminModel,
                    "is_superadmin");
            var isDisabledAttr = viewmodels.ModelAttribute.create(adminModel, 
                    "is_disabled");
            
            superclass(AdminEditorViewModel).call(this, adminModel,
                    endpointUrl);
        },
        
        initialize: function(attrs, adminModel) {
            superproto(AdminEditorViewModel).initialize.call(this);
        }
    });
    
    // Dialog edits ClassifierGroupViewModel rather than model directly 
    var FieldEditorDialogVM = viewmodels.ViewModel.extend({
        constructor: function FieldEditorDialogVM() {
            superclass(FieldEditorDialogVM).apply(this, arguments);
        },
        
        defaults: {
            dialogTitle: "",
            fieldTitle: "",
            fieldDescription: "",
            modelAttr: null,
            formField: null
        },
        
        initialize: function(attrs, normaliser) {
            assert(viewmodels.ModelAttribute.isInstance(this.get("modelAttr")));
            normaliser = normaliser || [];
            assert(_.isArray(normaliser));
            
            this.normaliser = normaliser;
            
            // Create field
            this.set("formField", new viewmodels.FormFieldViewModel({
                name: this.get("fieldTitle"),
                description: this.get("fieldDescription"),
                inputViewModel: new viewmodels.InputViewModel({
                    delegatedAttribute: this.get("modelAttr"),
                }, this.normaliser),
                annotations: []
            }));
        }
    });
    
    function makeFieldEditorDialog(dialogTitle, fieldTitle, modelAttr) {
        var dialogVM = new FieldEditorDialogVM({
            dialogTitle: dialogTitle,
            fieldTitle: fieldTitle,
            modelAttr: modelAttr
        });
        
        // add validators
        var formField = dialogVM.get("formField");
        formField.get("annotations").add(
                new viewmodels.EmptynessFieldAnnotation({}, 
                        viewmodels.ModelAttribute.create(
                                formField.get("inputViewModel"), 
                        "normalised_value")));
        return dialogVM;
    }
    
    function makeClassifierGroupEditor(isCreating, groupVM) {
        return makeFieldEditorDialog((isCreating ? 
                "Add new classification group" : "Edit classification group"),
                "Group name",
                viewmodels.ModelAttribute.create(groupVM, "name"));
    }
    
    function makeClassifierEditor(isCreating, classifierVM) {
        return makeFieldEditorDialog((isCreating ? 
                "Add new classification" : "Edit classification"),
                "Classification name",
                viewmodels.ModelAttribute.create(classifierVM, "value"));
    }
    
    var ClassifierViewModel = viewmodels.ViewModel.extend({
        constructor: function ClassifierViewModel() {
            superclass(ClassifierViewModel).apply(this, arguments);
        },
        
        initialize: function() {
            superproto(ClassifierViewModel).initialize.call(this);
            this.on("change", this.updateSynchedStatus);
        },
        
        editValueDialog: function() {
            return makeClassifierEditor(false, this);
        },
        
        synchToModel: function() {
            var classifier = this.get("classifier");
            classifier.set({
                value: this.get("value")
            });
            this.updateSynchedStatus();
        },
        
        areSynchedConditionsMet: function() {
            return superproto(ClassifierViewModel).areSynchedConditionsMet.call(
                    this);
        },
        
        remove: function() {
            this.get("classifier").destroy();
            this.destroy();
        }
    },
    {
        fromModel: function(classifier) {
            return new ClassifierViewModel({
                value: classifier.get("value"),
                classifier: classifier
            });
        }
    });
    
    var ClassifierGroupViewModel = viewmodels.ViewModel.extend({
        constructor: function ClassifierGroupViewModel() {
            superclass(ClassifierGroupViewModel).apply(this, arguments);
        },
        
        defaults: {
            classifier_group: null,
            name: "",
            type: null,
            classifiers: new viewmodels.ViewModelCollection()
        },
        
        initialize: function() {
            _.bindAll(this, "onClassifierAdded", "synchToModel");
            superproto(ClassifierGroupViewModel).initialize.call(this);
            
            // Ensure we've got a group to work on
            assert(this.get("classifier_group") instanceof Backbone.Model);
            assert(this.get("classifiers") instanceof 
                    viewmodels.ViewModelCollection);
            this.get("classifiers").on("add", this.onClassifierAdded);
            this.get("classifiers").on("add remove reset", this.synchToModel);
        },
        
        onClassifierAdded: function(classifierVM) {
            this.get("classifier_group").classifiers().add(
                    classifierVM.get("classifier"));
        },
        
        synchToModel: function() {
            var pending_classifier = this.get("pending_classifier");
            if(pending_classifier)
                pending_classifier.synchToModel();
            this.get("classifiers").each(function(c) { c.synchToModel(); });
            var classifier_group = this.get("classifier_group");
            classifier_group.set({
                name: this.get("name")
            });
            this.updateSynchedStatus();
        },
        
        areSynchedConditionsMet: function() {
            return superproto(ClassifierGroupViewModel)
                    .areSynchedConditionsMet.call(this) && 
                    this.get("name") === this.get("classifier_group")
                        .get("name") &&
                    this.get("classifier_group").isValid();
        },
        
        classifiers: function() {
            return this.get("classifiers");
        },
        
        editTitleDialog: function() {
            return makeClassifierGroupEditor(false, this);
        },
        
        newClassifierDialog: function() {
            assert(!this.get("pending_classifier"));
            
            var classifier = new superadmin_models.Classifier({ 
                value: "" });
            var classifierVM = new ClassifierViewModel.fromModel(classifier);
            this.set("pending_classifier", classifierVM);
            
            return makeClassifierEditor(true, classifierVM);
        },
        
        onNewClassifierDialogClosed: function(add_pending) {
            if(add_pending) {
                this.classifiers().add(this.get("pending_classifier"));
            }
            this.set("pending_classifier", null);
        },  
        
        remove: function() {
            this.get("classifier_group").destroy();
            this.destroy();
        }
    },
    {
        fromModel: function(group) {
            return new ClassifierGroupViewModel({
                classifier_group: group,
                name: group.get("name"),
                type: group.get("type"),
                classifiers: new viewmodels.ViewModelCollection(
                        group.classifiers().map(ClassifierViewModel.fromModel))
            });
        }
    });
    
    /**
     * A ViewModel representing the faculty editor used in the add & edit 
     * faculty superadmin pages.
     */
    var FacultyEditorViewModel = viewmodels.FormViewModel.extend({
        constructor: function FacultyEditorViewModel(
                attrs, faculty, years, url) {
            var facultyYearAttr = viewmodels.ModelAttribute.create(faculty, "year");
            var facultyNameAttr = viewmodels.ModelAttribute.create(faculty, "name");
            var facultyCodeAttr = viewmodels.ModelAttribute.create(faculty, "code");
            
            var yearInput = new viewmodels.SelectViewModel({
                delegatedAttribute: facultyYearAttr,
                choices: years
            });
            var yearInputValue = viewmodels.ModelAttribute.create("year",
                    yearInput, "normalised_value", "start_year");
            var yearField = new viewmodels.FormFieldViewModel({
                name: "Year",
                description: "The academic year this Faculty exists under.",
                inputViewModel: yearInput,
                annotations: []
            });
            
            var nameInput = new viewmodels.InputViewModel({
                delegatedAttribute: facultyNameAttr}, 
                [normalise.trim]);
            var nameInputValue = viewmodels.ModelAttribute.create("name",
                    nameInput, "normalised_value");
            var nameEmptynessAnnotation = 
                    new viewmodels.EmptynessFieldAnnotation({}, nameInputValue);
            var nameUniquenessAnnotation = 
                    new viewmodels.UniquenessFieldAnnotation({},
                            "/superadministration/faculties/exists/",
                            nameInputValue, yearInputValue);
            var nameField = new viewmodels.FormFieldViewModel({
                name: "Name",
                description: "The full name of the Faculty which users will see.",
                inputViewModel: nameInput,
                annotations: [nameEmptynessAnnotation, nameUniquenessAnnotation]
            });

            var codeInput = new viewmodels.InputViewModel({
                delegatedAttribute: facultyCodeAttr},
                [normalise.trim]);
            var codeInputValue = viewmodels.ModelAttribute.create("code",
                    codeInput, "normalised_value");
            var codeEmptynessAnnotation = new viewmodels.EmptynessFieldAnnotation({}, 
                    codeInputValue);
            var codeUniquenessAnnotation = new viewmodels.UniquenessFieldAnnotation({},
                    "/superadministration/faculties/exists/",
                    codeInputValue, yearInputValue);
            var codeField = new viewmodels.FormFieldViewModel({
                name: "Code",
                description: "This code is short form of the name. It's used in URLs.",
                inputViewModel: codeInput,
                annotations: [codeEmptynessAnnotation, codeUniquenessAnnotation]
            });
            
            attrs = _.extend({
                name: nameField,
                code: codeField,
                year: yearField,
                classifierGroups: null,
            
                // The backing data model of the form
                model: faculty
            }, attrs);
            
            superclass(FacultyEditorViewModel).call(this, attrs, faculty, url);
        },
        
        initialize: function(attrs, faculty, url) {
            _.bindAll(this, "onGroupAdded");
            superproto(FacultyEditorViewModel).initialize.call(this, 
                    attrs, url, "POST");
            
            var groupViewModels = new viewmodels.ViewModelCollection(
                    this.get("model").classifierGroups().map(
                            ClassifierGroupViewModel.fromModel));
            this.set("classifierGroups", groupViewModels);
            groupViewModels.on("add", this.onGroupAdded);
            groupViewModels.on("add remove reset change", this.synchToModel);
            this.on("submit:done", function() {
                window.location = "/superadministration/faculties/";
            })
        },
        
        onGroupAdded: function(groupVM) {
            this.get("model").classifierGroups().add(
                    groupVM.get("classifier_group"));
        },
        
        /**
         * Creates and stores (as an attribute here) a classifierGroup which
         * has not yet been added to the proper list of groups.
         */
        createNewClassificationDialog: function() {
            assert(!this.get("pending_group"), "Only one pending_group can " +
                    "exist at once.");
            
            var classifierGroup = new superadmin_models.ClassifierGroup({ 
                type: "custom" });
            
            var classifierGroupVM = new ClassifierGroupViewModel({
                    classifier_group: classifierGroup, type: "custom"});
            this.set("pending_group", classifierGroupVM);
            
            return makeClassifierGroupEditor(true, classifierGroupVM);
        },
        
        addPendingClassifierGroup: function() {
            this.get("classifierGroups").add(this.get("pending_group"));
            this.set("pending_group", null);
            console.log("added pending classifier",
                    this.get("classifierGroups"));
        },
        
        cancelPendingClassifierGroup: function() {
            this.set("pending_group", null);
        },
        
        synchToModel: function() {
            var model = this.get("model");
            model.set({
                name: this.get("name").get("inputViewModel").normalisedValue(),
                code: this.get("code").get("inputViewModel").normalisedValue(),
                year: this.get("year").get("inputViewModel").normalisedValue()
            });
            this.updateSynchedStatus();
        },
        
        
    });
    
    return {
        FacultyEditorViewModel: FacultyEditorViewModel,
        FieldEditorDialogVM: FieldEditorDialogVM
    };
});