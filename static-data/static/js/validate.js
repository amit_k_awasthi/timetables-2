define([], function() {
	
	// anyone who is here might like to consider using the binding mechanism in administrator/validation.js (ieb:20120810)
    
    function notEmpty(name, value) {
        if(value.trim() !== value)
            return name + " cannot have leading/trailing whitespace";
        if(value.length === 0)
            return name + " cannot be empty";
    }
    
    function looksLikeEmail(name, address) {
        // Check for at least one char in the local part & at least 4 in the
        // domain part. Obviously this does not try to be strict. 
        if(!/^.+@....+$/.test(address)) {
            return name + " does not look like an email address: " + address;
        }
    }
    
    return {
        notEmpty: notEmpty,
        looksLikeEmail: looksLikeEmail
    };
});