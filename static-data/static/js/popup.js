define(["jquery",
        "domReady",
        "jquery-ui/dialog"
        ], 
        function($) {
    "use strict";
    /* ----------------------------------------------------------------------- */
    /* Help Popup dialog */
    /* ----------------------------------------------------------------------- */
    /*
     * This binds to anything with popup-help-control and on hover over it shows a popup
     * containing the html in the next element.
     */
    
    // make certain the popup is at the top level.
    $('body').append($('.popup-help'));
    $(".popup-help-control").mouseover(function(e){
    	
        var popupHeight = $('.popup-help').position();
        var popupWidth = $('.popup-help').width();
        var popupPosition = $(this).offset();

        $('.popup-help').show();
        popupPosition.top = popupPosition.top+15;
        popupPosition.left =popupPosition.left - popupWidth +58;
        $('.popup-help').offset(popupPosition);

        /// Code to insert the right help content in the popup should go here:
        // contents of the help popup come from the element immediately following the .popup-help-control.
        $('.popup-help .popup-content-description ').html($(this).next().html());
        $('.popup-help .popup-content-head p').text($(this).next().attr('help-title'));

        $(".popup-help-control").mouseout(function(){
           $('.popup-help').hide();
        });
    });
});
