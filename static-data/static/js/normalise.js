/**
 * Normalisation functions for use on InputViewModels.
 */
define([], function() {
    
    function trim(input) { return input.trim(); }
    
    return {
        trim: trim
    };
});