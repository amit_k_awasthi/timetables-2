/**
* Select box skinning
*/
$(function() {

    $(document).ready(function() {
        applySkin('#select-subject', '#select-subject-skin');
        applySkin('#select-date', '#select-date-skin');
    });
    
    applySkin = function(selectElement, selectSkin) {
        updateName(selectElement, selectSkin);
        
        var select = $(selectElement);
        select.change(function(){ 
            updateName(selectElement, selectSkin);
        });
    };
    
    function updateName(selectId, selectSkinId) {
        var name = $(selectId + " > option" + ':selected').val();
        $(selectSkinId).find('span').text(name);
    }
    
});