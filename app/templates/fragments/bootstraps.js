{% load timetables_tags %}
{% for name, value in bootstraps.items %}
        define("bootstrap/{{ name|escapejs }}", function() {
            return {{ value|json|safe }};
        });
{% endfor %}