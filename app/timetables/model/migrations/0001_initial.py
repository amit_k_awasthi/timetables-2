# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration
from django.db import models
import django.utils.timezone


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AcademicYear'
        db.create_table('model_academicyear', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=16)),
            ('is_archived', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('model', ['AcademicYear'])

        # Adding model 'Role'
        db.create_table('model_role', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='r_t')),
        ))
        db.send_create_signal('model', ['Role'])

        # Adding model 'Permission'
        db.create_table('model_permission', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=16)),
        ))
        db.send_create_signal('model', ['Permission'])

        # Adding model 'RolePermission'
        db.create_table('model_rolepermission', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('role', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Role'])),
            ('permission', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Permission'])),
        ))
        db.send_create_signal('model', ['RolePermission'])

        # Adding model 'TermInfo'
        db.create_table('model_terminfo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=16)),
            ('day', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('term_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('model', ['TermInfo'])

        # Adding model 'Term'
        db.create_table('model_term', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('term_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.TermInfo'])),
            ('academic_year', self.gf('django.db.models.fields.related.ForeignKey')(related_name='terms', db_column='a_y', to=orm['model.AcademicYear'])),
            ('start', self.gf('django.db.models.fields.DateField')()),
            ('end', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('model', ['Term'])

        # Adding model 'OrganisationRole'
        db.create_table('model_organisationrole', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('role', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Role'])),
            ('organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Organisation'])),
        ))
        db.send_create_signal('model', ['OrganisationRole'])

        # Adding model 'Location'
        db.create_table('model_location', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=10)),
        ))
        db.send_create_signal('model', ['Location'])

        # Adding model 'EventType'
        db.create_table('model_eventtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
        ))
        db.send_create_signal('model', ['EventType'])

        # Adding model 'Organisation'
        db.create_table('model_organisation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('code', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('year', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.AcademicYear'])),
        ))
        db.send_create_signal('model', ['Organisation'])

        # Adding model 'ClassifierGroup'
        db.create_table('model_classifiergroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Organisation'])),
            ('family', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('model', ['ClassifierGroup'])

        # Adding model 'Classifier'
        db.create_table('model_classifier', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.ClassifierGroup'])),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal('model', ['Classifier'])

        # Adding model 'Classification'
        db.create_table('model_classification', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('website', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('contact_person', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Organisation'])),
        ))
        db.send_create_signal('model', ['Classification'])

        # Adding M2M table for field classifiers on 'Classification'
        db.create_table('model_classification_classifiers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('classification', models.ForeignKey(orm['model.classification'], null=False)),
            ('classifier', models.ForeignKey(orm['model.classifier'], null=False))
        ))
        db.create_unique('model_classification_classifiers', ['classification_id', 'classifier_id'])

        # Adding model 'GroupUsage'
        db.create_table('model_groupusage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Organisation'])),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.EventGroup'])),
        ))
        db.send_create_signal('model', ['GroupUsage'])

        # Adding M2M table for field classifiers on 'GroupUsage'
        db.create_table('model_groupusage_classifiers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('groupusage', models.ForeignKey(orm['model.groupusage'], null=False)),
            ('classifier', models.ForeignKey(orm['model.classifier'], null=False))
        ))
        db.create_unique('model_groupusage_classifiers', ['groupusage_id', 'classifier_id'])

        # Adding model 'GroupUsageVersion'
        db.create_table('model_groupusageversion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Organisation'])),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.EventGroupVersion'])),
            ('current_version', self.gf('django.db.models.fields.related.ForeignKey')(related_name='versions', to=orm['model.GroupUsage'])),
        ))
        db.send_create_signal('model', ['GroupUsageVersion'])

        # Adding M2M table for field classifiers on 'GroupUsageVersion'
        db.create_table('model_groupusageversion_classifiers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('groupusageversion', models.ForeignKey(orm['model.groupusageversion'], null=False)),
            ('classifier', models.ForeignKey(orm['model.classifier'], null=False))
        ))
        db.create_unique('model_groupusageversion_classifiers', ['groupusageversion_id', 'classifier_id'])

        # Adding model 'EventGroup'
        db.create_table('model_eventgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('publication_status', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='p_status')),
            ('owning_organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Organisation'])),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('term', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Term'])),
            ('default_pattern', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('default_location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Location'])),
            ('event_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.EventType'])),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
        ))
        db.send_create_signal('model', ['EventGroup'])

        # Adding model 'EventGroupVersion'
        db.create_table('model_eventgroupversion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('publication_status', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='p_status')),
            ('owning_organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Organisation'])),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('term', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Term'])),
            ('default_pattern', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('default_location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Location'])),
            ('event_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.EventType'])),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('current_version', self.gf('django.db.models.fields.related.ForeignKey')(related_name='versions', db_column='c_v', to=orm['model.EventGroup'])),
            ('revision', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Revision'])),
        ))
        db.send_create_signal('model', ['EventGroupVersion'])

        # Adding model 'EventSeries'
        db.create_table('model_eventseries', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('date_time_pattern', self.gf('django.db.models.fields.CharField')(max_length=256, db_column='t_s')),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Location'])),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('owning_group', self.gf('django.db.models.fields.related.ForeignKey')(related_name='owned_series', to=orm['model.EventGroup'])),
        ))
        db.send_create_signal('model', ['EventSeries'])

        # Adding M2M table for field usages on 'EventSeries'
        db.create_table('model_eventseries_usages', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('eventseries', models.ForeignKey(orm['model.eventseries'], null=False)),
            ('eventgroup', models.ForeignKey(orm['model.eventgroup'], null=False))
        ))
        db.create_unique('model_eventseries_usages', ['eventseries_id', 'eventgroup_id'])

        # Adding M2M table for field associated_staff on 'EventSeries'
        db.create_table('model_eventseries_associated_staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('eventseries', models.ForeignKey(orm['model.eventseries'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('model_eventseries_associated_staff', ['eventseries_id', 'user_id'])

        # Adding model 'EventSeriesVersion'
        db.create_table('model_eventseriesversion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('date_time_pattern', self.gf('django.db.models.fields.CharField')(max_length=256, db_column='t_s')),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Location'])),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('owning_group', self.gf('django.db.models.fields.related.ForeignKey')(related_name='versioned_owned_series', to=orm['model.EventGroupVersion'])),
            ('current_version', self.gf('django.db.models.fields.related.ForeignKey')(related_name='versions', db_column='c_v', to=orm['model.EventSeries'])),
        ))
        db.send_create_signal('model', ['EventSeriesVersion'])

        # Adding M2M table for field usages on 'EventSeriesVersion'
        db.create_table('model_eventseriesversion_usages', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('eventseriesversion', models.ForeignKey(orm['model.eventseriesversion'], null=False)),
            ('eventgroup', models.ForeignKey(orm['model.eventgroup'], null=False))
        ))
        db.create_unique('model_eventseriesversion_usages', ['eventseriesversion_id', 'eventgroup_id'])

        # Adding M2M table for field associated_staff on 'EventSeriesVersion'
        db.create_table('model_eventseriesversion_associated_staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('eventseriesversion', models.ForeignKey(orm['model.eventseriesversion'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('model_eventseriesversion_associated_staff', ['eventseriesversion_id', 'user_id'])

        # Adding model 'TimeInterval'
        db.create_table('model_timeinterval', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('model', ['TimeInterval'])

        # Adding model 'Event'
        db.create_table('model_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('position', self.gf('django.db.models.fields.IntegerField')()),
            ('default_timeslot', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', null=True, to=orm['model.TimeInterval'])),
            ('overridden_timeslot', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', null=True, to=orm['model.TimeInterval'])),
            ('overridden_title', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('is_staff_overridden', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('overridden_location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Location'], null=True)),
            ('combination_set_id', self.gf('django.db.models.fields.IntegerField')()),
            ('owning_series', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.EventSeries'], db_column='e_s')),
        ))
        db.send_create_signal('model', ['Event'])

        # Adding M2M table for field overridden_staff on 'Event'
        db.create_table('model_event_overridden_staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm['model.event'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('model_event_overridden_staff', ['event_id', 'user_id'])

        # Adding model 'EventVersion'
        db.create_table('model_eventversion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('position', self.gf('django.db.models.fields.IntegerField')()),
            ('default_timeslot', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', null=True, to=orm['model.TimeInterval'])),
            ('overridden_timeslot', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', null=True, to=orm['model.TimeInterval'])),
            ('overridden_title', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('is_staff_overridden', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('overridden_location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Location'], null=True)),
            ('combination_set_id', self.gf('django.db.models.fields.IntegerField')()),
            ('owning_series', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.EventSeriesVersion'], db_column='e_s')),
            ('current_version', self.gf('django.db.models.fields.related.ForeignKey')(related_name='versions', to=orm['model.Event'])),
        ))
        db.send_create_signal('model', ['EventVersion'])

        # Adding M2M table for field overridden_staff on 'EventVersion'
        db.create_table('model_eventversion_overridden_staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('eventversion', models.ForeignKey(orm['model.eventversion'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('model_eventversion_overridden_staff', ['eventversion_id', 'user_id'])

        # Adding model 'Revision'
        db.create_table('model_revision', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('version', self.gf('django.db.models.fields.IntegerField')()),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('comment', self.gf('django.db.models.fields.TextField')()),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Organisation'])),
        ))
        db.send_create_signal('model', ['Revision'])

        # Adding model 'GroupRevision'
        db.create_table('model_grouprevision', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('enclosing_revision', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.Revision'])),
            ('usage_version', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.GroupUsageVersion'])),
            ('group_version', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.EventGroupVersion'], null=True)),
        ))
        db.send_create_signal('model', ['GroupRevision'])

        # Adding model 'PersonalTimetable'
        db.create_table('model_personaltimetable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('year', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['model.AcademicYear'])),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal('model', ['PersonalTimetable'])


    def backwards(self, orm):
        # Deleting model 'AcademicYear'
        db.delete_table('model_academicyear')

        # Deleting model 'Role'
        db.delete_table('model_role')

        # Deleting model 'Permission'
        db.delete_table('model_permission')

        # Deleting model 'RolePermission'
        db.delete_table('model_rolepermission')

        # Deleting model 'TermInfo'
        db.delete_table('model_terminfo')

        # Deleting model 'Term'
        db.delete_table('model_term')

        # Deleting model 'OrganisationRole'
        db.delete_table('model_organisationrole')

        # Deleting model 'Location'
        db.delete_table('model_location')

        # Deleting model 'EventType'
        db.delete_table('model_eventtype')

        # Deleting model 'Organisation'
        db.delete_table('model_organisation')

        # Deleting model 'ClassifierGroup'
        db.delete_table('model_classifiergroup')

        # Deleting model 'Classifier'
        db.delete_table('model_classifier')

        # Deleting model 'Classification'
        db.delete_table('model_classification')

        # Removing M2M table for field classifiers on 'Classification'
        db.delete_table('model_classification_classifiers')

        # Deleting model 'GroupUsage'
        db.delete_table('model_groupusage')

        # Removing M2M table for field classifiers on 'GroupUsage'
        db.delete_table('model_groupusage_classifiers')

        # Deleting model 'GroupUsageVersion'
        db.delete_table('model_groupusageversion')

        # Removing M2M table for field classifiers on 'GroupUsageVersion'
        db.delete_table('model_groupusageversion_classifiers')

        # Deleting model 'EventGroup'
        db.delete_table('model_eventgroup')

        # Deleting model 'EventGroupVersion'
        db.delete_table('model_eventgroupversion')

        # Deleting model 'EventSeries'
        db.delete_table('model_eventseries')

        # Removing M2M table for field usages on 'EventSeries'
        db.delete_table('model_eventseries_usages')

        # Removing M2M table for field associated_staff on 'EventSeries'
        db.delete_table('model_eventseries_associated_staff')

        # Deleting model 'EventSeriesVersion'
        db.delete_table('model_eventseriesversion')

        # Removing M2M table for field usages on 'EventSeriesVersion'
        db.delete_table('model_eventseriesversion_usages')

        # Removing M2M table for field associated_staff on 'EventSeriesVersion'
        db.delete_table('model_eventseriesversion_associated_staff')

        # Deleting model 'TimeInterval'
        db.delete_table('model_timeinterval')

        # Deleting model 'Event'
        db.delete_table('model_event')

        # Removing M2M table for field overridden_staff on 'Event'
        db.delete_table('model_event_overridden_staff')

        # Deleting model 'EventVersion'
        db.delete_table('model_eventversion')

        # Removing M2M table for field overridden_staff on 'EventVersion'
        db.delete_table('model_eventversion_overridden_staff')

        # Deleting model 'Revision'
        db.delete_table('model_revision')

        # Deleting model 'GroupRevision'
        db.delete_table('model_grouprevision')

        # Deleting model 'PersonalTimetable'
        db.delete_table('model_personaltimetable')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'django.utils.timezone.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'django.utils.timezone.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'model.academicyear': {
            'Meta': {'object_name': 'AcademicYear'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archived': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'})
        },
        'model.classification': {
            'Meta': {'object_name': 'Classification'},
            'classifiers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.Classifier']", 'symmetrical': 'False'}),
            'contact_person': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '1024'})
        },
        'model.classifier': {
            'Meta': {'object_name': 'Classifier'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.ClassifierGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'model.classifiergroup': {
            'Meta': {'object_name': 'ClassifierGroup'},
            'family': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"})
        },
        'model.event': {
            'Meta': {'object_name': 'Event'},
            'combination_set_id': ('django.db.models.fields.IntegerField', [], {}),
            'default_timeslot': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': "orm['model.TimeInterval']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_staff_overridden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'overridden_location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']", 'null': 'True'}),
            'overridden_staff': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'overridden_timeslot': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': "orm['model.TimeInterval']"}),
            'overridden_title': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'owning_series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventSeries']", 'db_column': "'e_s'"}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        },
        'model.eventgroup': {
            'Meta': {'object_name': 'EventGroup'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'default_location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']"}),
            'default_pattern': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'owning_organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'publication_status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'p_status'"}),
            'term': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Term']"}),
            'usages': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'used_groups'", 'symmetrical': 'False', 'through': "orm['model.GroupUsage']", 'to': "orm['model.Organisation']"})
        },
        'model.eventgroupversion': {
            'Meta': {'object_name': 'EventGroupVersion'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'current_version': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'db_column': "'c_v'", 'to': "orm['model.EventGroup']"}),
            'default_location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']"}),
            'default_pattern': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'owning_organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'publication_status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'p_status'"}),
            'revision': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Revision']"}),
            'term': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Term']"}),
            'usages': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'versioned_used_groups'", 'symmetrical': 'False', 'through': "orm['model.GroupUsageVersion']", 'to': "orm['model.Organisation']"})
        },
        'model.eventseries': {
            'Meta': {'object_name': 'EventSeries'},
            'associated_staff': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'date_time_pattern': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_column': "'t_s'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']"}),
            'owning_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owned_series'", 'to': "orm['model.EventGroup']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'usages': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.EventGroup']", 'db_column': "'e_g'", 'symmetrical': 'False'})
        },
        'model.eventseriesversion': {
            'Meta': {'object_name': 'EventSeriesVersion'},
            'associated_staff': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'current_version': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'db_column': "'c_v'", 'to': "orm['model.EventSeries']"}),
            'date_time_pattern': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_column': "'t_s'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']"}),
            'owning_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versioned_owned_series'", 'to': "orm['model.EventGroupVersion']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'usages': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.EventGroup']", 'db_column': "'e_g'", 'symmetrical': 'False'})
        },
        'model.eventtype': {
            'Meta': {'object_name': 'EventType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        'model.eventversion': {
            'Meta': {'object_name': 'EventVersion'},
            'combination_set_id': ('django.db.models.fields.IntegerField', [], {}),
            'current_version': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'to': "orm['model.Event']"}),
            'default_timeslot': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': "orm['model.TimeInterval']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_staff_overridden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'overridden_location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Location']", 'null': 'True'}),
            'overridden_staff': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'}),
            'overridden_timeslot': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': "orm['model.TimeInterval']"}),
            'overridden_title': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'owning_series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventSeriesVersion']", 'db_column': "'e_s'"}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        },
        'model.grouprevision': {
            'Meta': {'object_name': 'GroupRevision'},
            'enclosing_revision': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Revision']"}),
            'group_version': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventGroupVersion']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'usage_version': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.GroupUsageVersion']"})
        },
        'model.groupusage': {
            'Meta': {'object_name': 'GroupUsage'},
            'classifiers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.Classifier']", 'symmetrical': 'False'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"})
        },
        'model.groupusageversion': {
            'Meta': {'object_name': 'GroupUsageVersion'},
            'classifiers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['model.Classifier']", 'symmetrical': 'False'}),
            'current_version': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'to': "orm['model.GroupUsage']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.EventGroupVersion']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"})
        },
        'model.location': {
            'Meta': {'object_name': 'Location'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        'model.organisation': {
            'Meta': {'object_name': 'Organisation'},
            'code': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'year': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.AcademicYear']"})
        },
        'model.organisationrole': {
            'Meta': {'object_name': 'OrganisationRole'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Role']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'model.permission': {
            'Meta': {'object_name': 'Permission'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'})
        },
        'model.personaltimetable': {
            'Meta': {'object_name': 'PersonalTimetable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'year': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.AcademicYear']"})
        },
        'model.revision': {
            'Meta': {'object_name': 'Revision'},
            'comment': ('django.db.models.fields.TextField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Organisation']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'version': ('django.db.models.fields.IntegerField', [], {})
        },
        'model.role': {
            'Meta': {'object_name': 'Role'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'r_t'"})
        },
        'model.rolepermission': {
            'Meta': {'object_name': 'RolePermission'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Permission']"}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.Role']"})
        },
        'model.term': {
            'Meta': {'object_name': 'Term'},
            'academic_year': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'terms'", 'db_column': "'a_y'", 'to': "orm['model.AcademicYear']"}),
            'end': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start': ('django.db.models.fields.DateField', [], {}),
            'term_info': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['model.TermInfo']"})
        },
        'model.terminfo': {
            'Meta': {'object_name': 'TermInfo'},
            'day': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'}),
            'term_type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'model.timeinterval': {
            'Meta': {'object_name': 'TimeInterval'},
            'end_time': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['model']