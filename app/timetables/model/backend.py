'''
Created on Sep 6, 2012

@author: ieb
'''
from timetables.model.models import Permission, User
import logging
import traceback

log = logging.getLogger(__name__)


class PermissionSubject(object):
    
    def __init__(self, organisation, classifiers=None):
        self.organisation = organisation
        self.classifiers = classifiers
        
    def __unicode__(self):
        if self.classifiers is None:
            return "%s" % self.organisation
        return "%s with classifiers" % self.organisation

class TimetablesAuthorisationBackend(object):
    '''
    This authorization backend checks for permissions on various objects for the supplied user.
    
    It has to resolve
    read,
    write
    publish
    write_classificatoins
    write_administrators
    key_admin
    
    given an Organisation and a set of classifiers the query is
    
    read, write, publish are applied to classifier sets and orgnaisation
    
    write_classifications, write_administrators and key admin are applied to organisations
    
    Organisation level permissions for a user are
    Permission.objects.filter(rolepermission__role__organisationrole__organisation=org,
                    rolepermission__role__organisationrole__user=user),
                    rolepermission__role__organisationrole__classifier__in=classifier_set)
                    
    Classification level permissions for a user are
    Permission.objects.filter(rolepermission__role__organisationrole__organisation=org,
                    rolepermission__role__organisationrole__user=user,
                    rolepermission__role__organisationrole__classifier__in=classifier_set)
    The org join is superfluous since classifiers are bound to orgs via groups.
    
    
    The method backend only responds to queries that have been made with a target object of PermissionSubject
    The object will contain an organization and optionally a set of classifications.
    Assuming the permissions resolution is non trivial then we create a set of permissions for the organisation or
    classification set. Having done that we check for an intersection with the supplied set.
    
    Doing this ensures that provide the permissions checks contain sets of permissions the test can be resolved
    in a single operation which is query set caching is enabled will come from the in memory cache without polluting the cache.
    
    This approach has been used sucessfully in other systems with much more complex permissions systems and many more objects, although
    the datamodel not as complex.
    
    
    '''
    
    supports_object_permissions=True
    supports_anonymous_user=True
    supports_inactive_user=True

    def get_user(self, user_id):
        return None
    
    def authenticate(self,**credentials):
        return None
    
    def _iscached(self, user_obj, subject):
        return hasattr(user_obj,"_permissions") and subject.organisation in user_obj._permissions

    def _getcached(self, user_obj, subject):
        return user_obj._permissions[subject.organisation]

    def _cache(self, user_obj, subject, perms):
        if not hasattr(user_obj,"_permissions"):
            user_obj._permissions = dict()
        user_obj._permissions[subject.organisation] = perms
        return perms
                
    def get_all_permissions(self, user_obj, obj=None):
        # Don't contribute any permissions if no object is specified
        if obj is None:
            return set()

        # If the use allready has a set of permissions, use it.
        if isinstance(obj, PermissionSubject):
            if  obj.classifiers is None and self._iscached(user_obj, obj):
                logging.error("Cached  permissions %s " % user_obj._permissions)
                return self._getcached(user_obj, obj)
            try:
                if user_obj.is_superuser or user_obj.is_staff or User.is_superadmin(user_obj):
                    perms = set()
                    perms.add(Permission.KEY_ADMIN)
                    perms.add(Permission.READ)
                    perms.add(Permission.WRITE)
                    perms.add(Permission.PUBLISH)
                    perms.add(Permission.WRITE_ADMINISTRATORS)
                    perms.add(Permission.WRITE_CLASSIFICATIONS)
                    perms = self._cache(user_obj, obj, perms)
                else:
                    perms = set(p.name for p in Permission.objects.filter(rolepermission__role__organisationrole__organisation=obj.organisation,
                                              rolepermission__role__organisationrole__user=user_obj))
                    # Not giving key admin full permissions makes no sense.
                    # They will be contacted in emergency and hence need to be able to do everything, and not have to 
                    # contact someone else.
                    if Permission.KEY_ADMIN in perms or user_obj.is_superuser or user_obj.is_staff or User.is_superadmin(user_obj):
                        perms.add(Permission.KEY_ADMIN)
                        perms.add(Permission.READ)
                        perms.add(Permission.WRITE)
                        perms.add(Permission.PUBLISH)
                        perms.add(Permission.WRITE_ADMINISTRATORS)
                        perms.add(Permission.WRITE_CLASSIFICATIONS)
                        perms = self._cache(user_obj, obj, perms)
                    elif obj.classifiers is not None:
                        # Classifiers was specified and this user is not a key admin, so we have to query on classifiers
                        perms = set(p.name for p in Permission.objects.filter(rolepermission__role__organisationrole__organisation=obj.organisation,
                                                  rolepermission__role__organisationrole__user=user_obj,
                                                  rolepermission__role__organisationrole__classifier__in=obj.classifiers))
                        # can't cache for classifiers
                    else:
                        perms = self._cache(user_obj, obj, perms)
                log.error("Got permissions %s for %s " % (perms, user_obj))
                return perms
            except:
                log.error(traceback.format_exc())
                return set()
            

    def has_perm(self, user_obj, perm, obj=None):
        return self.has_perms(user_obj, [ perm ], obj)
    
    def has_perms(self, user_obj, perms, obj=None):
        if isinstance(obj,PermissionSubject):
            # Check for authorization being marked as passed
            if user_obj.is_anonymous():
                return False
            if hasattr(user_obj,"_authorized") and  user_obj._authorized:
                log.info("Granted permissions for %s %s %s : pre-authorised" % (user_obj.username, perms, obj))
                return True
            if user_obj.is_superuser:
                log.info("Granted permissions for %s %s %s : superuser" % (user_obj.username, perms, obj))
                return True
            if user_obj.is_staff:
                log.info("Granted permissions for %s %s %s : staff" % (user_obj.username, perms, obj))
                return True
            if User.is_superadmin(user_obj):
                # at the moment there is no simple way of avoiding the above which makes this code
                # re-entrant into the permissions system and expensive as it needs 2x resolutions for
                # every operation. Caching doesn't help as the standard Django session manager invalidates the
                # cache every time someone logs in. Ideally this permission should a property of the user object directly
                # so it can be used directly and not a permission. Because changing this now is major upheaval, we will simply
                # mark the in memory user object as authorized to prevent too much impact.
                user_obj._authorized = True
                log.info("Granted permissions for %s %s %s : timetables superadmin" % (user_obj.username, perms, obj))
                return True
            allperms = self.get_all_permissions(user_obj, obj)
            if set(perms).issubset(allperms):
                log.info("Granted permissions for %s %s %s " % (user_obj.username, perms, obj))
                return True
            log.info("Denied permissions for %s %s %s " % (user_obj.username, perms, obj))
        return False
    
