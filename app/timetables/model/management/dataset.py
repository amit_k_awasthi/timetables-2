'''
get_or_created on May 15, 2012

@author: ieb
'''
from django.contrib.auth.models import User
from timetables.utils.staticdata import create_yaml_loader_relative_to_file
from timetables.utils.datetimes import parse_iso8601_date

import logging
from timetables.model.models import TermInfo, AcademicYear, Term, Role,\
    Permission, RolePermission, OrganisationRole, Organisation, ClassifierGroup,\
    Classifier, EventType, Location, EventGroup, EventSeries
log = logging.getLogger(__name__)
del logging

load_yaml = create_yaml_loader_relative_to_file(__file__)

def _get_or_create_user(username, firstname, lastname, email, password):
    try:
        return User.objects.get(username=username)
    except User.DoesNotExist:
        u1 = User.objects.create_user(username, email, password)
        u1.first_name = firstname
        u1.last_name = lastname
        u1.save()
        return u1

def getorcreate(clazz, *args, **kwargs):
    return clazz.objects.get_or_create(*args, **kwargs)

def make_term(terminfo, year, start, end):
    return Term.objects.get_or_create(term_info=terminfo,
            academic_year=year, start=parse_iso8601_date(start),
            end=parse_iso8601_date(end))

def create_years():
    easter, _ = getorcreate(TermInfo, name="easter", term_type='S')
    easter_day, _ = getorcreate(TermInfo, name="easter_day", day=True, term_type='S')
    lent, _ = getorcreate(TermInfo, name="lent", term_type='S')
    michaelmas, _ = getorcreate(TermInfo, name="michaelmas", term_type='S')
    summer_field_trip, _ = getorcreate(TermInfo, name="summerschool", term_type='O')


    ay11, _ = getorcreate(AcademicYear, starting_year=2011)
    ay12, _ = getorcreate(AcademicYear, starting_year=2012)
    ay13, _ = getorcreate(AcademicYear, starting_year=2013)
    ay14, _ = getorcreate(AcademicYear, starting_year=2014)

    make_term(michaelmas, ay11, "2011-10-04", "2011-12-02")
    make_term(lent, ay11, "2012-01-17", "2012-03-16")
    make_term(easter_day, ay11, "2012-04-08", "2012-04-08")
    make_term(easter, ay11, "2012-04-24", "2012-06-15")
    make_term(summer_field_trip, ay11, "2012-06-24", "2012-07-15")

    make_term(michaelmas, ay12, "2012-10-02", "2012-11-30")
    make_term(lent, ay12, "2013-01-15", "2013-03-15")
    make_term(easter_day, ay12, "2013-03-31", "2013-03-31")
    make_term(easter, ay12, "2013-04-23", "2013-06-14")

    make_term(michaelmas, ay13, "2013-10-08", "2013-12-06")
    make_term(lent, ay13, "2014-01-14", "2014-03-14")
    make_term(easter_day, ay13, "2014-04-20", "2014-04-20")
    make_term(easter, ay13, "2014-04-22", "2014-06-13")

    make_term(michaelmas, ay14, "2014-10-07", "2014-12-05")
    make_term(lent, ay14, "2015-01-13", "2015-03-13")
    make_term(easter_day, ay14, "2015-04-05", "2015-04-05")
    make_term(easter, ay14, "2015-04-21", "2015-06-12")

    return [ay11, ay12, ay13, ay14]

def create_roles_and_permissions(org, year, adminUsers, coordUsers, facultyUsers):
    admin, _ = getorcreate(Role, name="%s administrator" % org.code, type='A')
    coord, _ = getorcreate(Role, name="%s coordinator" % org.code , type='A')
    faculty, _ = getorcreate(Role, name="%s faculty" % org.code, type='O')
    permf, _ = getorcreate(Permission, name="faculty")
    permk, _ = getorcreate(Permission, name="key")
    perms, _ = getorcreate(Permission, name="super")
    getorcreate(RolePermission, role=admin, permission=perms)
    getorcreate(RolePermission, role=admin, permission=permk)
    getorcreate(RolePermission, role=admin, permission=permf)
    getorcreate(RolePermission, role=coord, permission=permk)
    getorcreate(RolePermission, role=faculty, permission=permf)
    for u in adminUsers:
        getorcreate(OrganisationRole, user=u, role=admin, organisation=org)
    for u in coordUsers:
        getorcreate(OrganisationRole, user=u, role=coord, organisation=org)
    for u in facultyUsers:
        getorcreate(OrganisationRole, user=u, role=faculty, organisation=org)

def create_organisation(name, code, year):
    return getorcreate(Organisation, name=name, year=year,
            # Don't include code when deciding if an Organisation exists:
            defaults=dict(code=code))

def create_department(name, code, year, levels, subjects, admin, coord, faculty):
    org, _ = create_organisation(name, code , year)

    # FIXME: need constants for family on model
    lc, _ = ClassifierGroup.objects.get_or_create(name="Levels",
            organisation=org, family="L")
    sc, _ = ClassifierGroup.objects.get_or_create(name="Subjects",
            organisation=org, family="S")
    for l in levels:
        getorcreate(Classifier, group=lc, value=l)
    for s in subjects:
        getorcreate(Classifier, group=sc, value=s)

    create_roles_and_permissions(org, year, admin, coord, faculty)
    return org



def load_test_data():
    years = create_years()
    labEventType, _ = getorcreate(EventType, name="labs")
    classEventType, _ = getorcreate(EventType, name="class")
    lectureEventType, _ = getorcreate(EventType, name="lecture")
    location, _ = getorcreate(Location, name="LocationA")
    departments = load_yaml("dataset/departments.yaml")
    otherorgs = {}
    for y in years:
        otherorgs[y.name()] = []

    for code, dep in departments.iteritems():
        u1 = _get_or_create_user("u100-%s" % code, "firstName_100-%s" % code, "lastName_user", "u100-%s@example.org" % code, "password")
        u2 = _get_or_create_user("u200-%s" % code, "firstName_200-%s" % code, "lastName_user", "u200-%s@example.org" % code, "password")
        u3 = _get_or_create_user("u300-%s" % code, "firstName_300-%s" % code, "lastName_user", "u300-%s@example.org" % code, "password")
        u4 = _get_or_create_user("u400-%s" % code, "firstName_400-%s" % code, "lastName_user", "u400-%s@example.org" % code, "password")
        if 'l' not in dep:
            dep['l'] = []
        if 's' not in dep:
            dep['s'] = []
        log.info("Creating %s %s %s %s" % (dep['n'], code, dep['l'], dep['s']))
        for y in years:
            org = create_department(dep['n'], code, y, dep['l'], dep['s'], [ u1 ], [ u2 ], [u3, u4])
            for t in Term.objects.filter(academic_year=y):
                if t.start == t.end:
                    continue
                for eventType in list([labEventType, classEventType, lectureEventType]):
                    eventGroup, _ = getorcreate(EventGroup,
                                             name="EG-%s-%s" % (code, eventType.name[:2]),
                                             owning_organisation=org,
                                             code="%s%s%s" % (code, t.id, eventType.name[:2]),
                                             term=t,
                                             default_location=location,
                                             event_type=eventType)
                    for n in range(1, 5):
                        eventGroupSerise = getorcreate(EventSeries,
                                                       owning_group=eventGroup,
                                                       title="ES%s%s" % (n, eventGroup.name),
                                                       location=location,
                                                       date_time_pattern="Ea1-8 Sa 9")
            otherorgs[y.name()].append(org)
            if len(otherorgs[y.name()]) > 5:
                otherorgs[y.name()] = otherorgs[y.name()][1:]

    return

