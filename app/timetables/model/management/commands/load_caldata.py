'''
Created on May 23, 2012

@author: ieb
'''
import re
from django.utils import simplejson as json
from django.core.management.base import BaseCommand, CommandError
import os
from timetables.model.models import AcademicYear, Organisation, ClassifierGroup, \
    Classifier, Classification, EventGroup, Location, GroupUsage, TermInfo, Term, \
    EventType, EventSeries

import logging
from django.utils.datetime_safe import datetime
log = logging.getLogger(__name__)



ID_PATTERNS = (
            re.compile(r"T(?P<tripos_id>\d{4})(?P<part_id>\d{5})(?P<year_id>\d{4})(?P<subject_id>\d{3})"),
            re.compile(r"T(?P<tripos_id>\d{4})(?P<part_id>\d{5})(?P<year_id>\d{4})"),
        )

NAME_PATTERNS = (
            re.compile("(?P<name>.*?) Part (?P<level>.*?) (?P<subject>.*)$"),
            re.compile("(?P<name>.*?) Part (?P<level>.*)$"),
            re.compile("(?P<name>.*?)Part (?P<level>.*?) (?P<subject>.*)$"),
            re.compile("(?P<name>.*?)Part (?P<level>.*)$"),
            re.compile("(?P<level>.*?) in (?P<name>.*?) - (?P<subject>.*)$"),
            re.compile("(?P<level>.*?) in (?P<name>.*)$"),
        )

class Command(BaseCommand):
    args = '<path_to_caldata_location> [list]|[<Subject::level> <Subject::level>] '
    help = 'Loads test data to populate the db for the first time, allowing dump'


    def handle(self, *args, **options):
        if not args or len(args) == 0:
            raise CommandError("No paths provided")
        
        logging.basicConfig(level=logging.INFO)

        if len(args) == 1 or args[1] == 'list':
            listOnly = True
        else:
            listOnly = False
        if len(args) == 1:
            self.load_calendar_data(None, args[0], listOnly)
        else:
            nameFilter = []
            for a in args[1:]:
                nameFilter.append(a.lower())
            self.load_calendar_data(nameFilter, args[0], listOnly)

    def load_calendar_data(self, nameFilter, caldir, listOnly):
        # Scan the eventdata subdir
        # For each json file found parse and load
        """
        {
    "name": "Systems Biology",
    "vhash": "0f5a767acaf26ba26dfb8bcc40bea479",
    "organiser": "Example organiser",
    "groups": [
        {
            "term": "Michaelmas",
            "code": "Mi1-8 Th 10",
            "name": "Lecture",
            "elements": [
                {
                    "what": "Systems Biology",
                    "code": " x8",
                    "who": "Example person",
                    "when": " x8",
                    "merge": 0,
                    "eid": "Ee8143b54e72ed0ea44ea140fb3ef7eb4",
                    "where": "Example location"
                }
            ]
        },
        ...........
    ],
    "where": "Example location",
    "id": "T0001000012012002",
    "metadata": {
        "notes": "",
        "course-description": ""
    }
}


{
    "years": [
        {
            "triposes": [
                {
                    "parts": [
                        {
                            "name": "Architecture Tripos Part IA",
                            "id": "T0014001202012"
                        },
                        ....
                    ],
                    "name": "Architecture & History of Art"
                },
        """
        caldir = os.path.abspath(caldir)
        topF = open("%s/top.json" % caldir)
        top = json.loads(topF.read())
        topF.close()

        detail_files = []
        detailMatch = re.compile("details_T\d*.json$")
        files = os.listdir(caldir)
        for fileName in files:
            if detailMatch.match(fileName) is not None:
                detail_files.append(fileName)

        for year in top["years"]:
            log.info("Processing Year %s " % year['name'])
            start_year = int(re.match("(\d{4})", year['name']).group(1))

            for tripos in year['triposes']:
                if not 'parts' in tripos or \
                        len(tripos['parts']) == 0 or \
                        'id' not in tripos['parts'][0]:
                    log.info("Skipping Invalid Tripos %s " % tripos)
                    continue
                if listOnly:
                    log.info("Processing Tripos %s" % (tripos['name']))
                triposId = self._parseId(tripos['parts'][0]['id'])
                if triposId is None:
                    continue

                triposCode = "%s%s" % (triposId['year_id'], triposId['tripos_id'])

                createdCount = {
                           }
                accessedCount = {
                           }
                partsProcessed = 0
                for p in tripos['parts']:
                    n = 0

                    nameParts = self._parsePartName(p["name"])
                    triposId = self._parseId(p['id'])
                    if nameParts is None:
                        nameParts = {"name" : p["name"] }
                        log.error("Failed to parse name  %s " % p["name"])

                    if 'level' in nameParts:
                        partId = "%s::%s" % (nameParts['name'], nameParts['level'])
                    else:
                        partId = "%s::" % (nameParts['name'])
                    if nameFilter is not None and partId.lower() not in nameFilter:
                        if listOnly:
                            log.info("Skipping Part triposId %s  Part %s " % (triposCode, partId))
                        continue

                    log.info("Processing Part triposId %s  Part %s " % (triposCode, partId))
                    if listOnly:
                        continue

                    partsProcessed = partsProcessed + 1

                    ayear, created = AcademicYear.objects.get_or_create(starting_year=start_year)
                    if created:
                        log.info("Added Year %s " % (start_year))
                    self._incCount('AcademicYear', createdCount if created else accessedCount)
                    department, created = Organisation.objects.get_or_create(name=tripos['name'], year=ayear, code=triposCode)
                    if created:
                        log.info("Added Organisation %s %s %s " % (tripos['name'], ayear, triposCode))
                    self._incCount('Organisation', createdCount if created else accessedCount)


                    dre = re.compile("details_%s\d*.json$" % p['id'])
                    log.info("Scanning with pattern %s " % dre.pattern)

                    organiser = "Unknown"

                    for dfn in detail_files:
                        if not dre.match(dfn):
                            continue
                        detailFileName = "%s/%s" % (caldir, dfn)
                        log.info("Found %s " % detailFileName)
                        detailF = open(detailFileName)
                        detail = json.loads(detailF.read())
                        detailF.close()



                        classifiers = []
                        level = None
                        classifier = None
                        if "level" in nameParts:
                            levelCGroup, created = ClassifierGroup.objects.get_or_create(name="Levels",
                                                            organisation=department, family="L")
                            self._incCount('ClassifierGroup', createdCount if created else accessedCount)
                            level, created = Classifier.objects.get_or_create(value=nameParts['level'],
                                                            group=levelCGroup)
                            Classification.objects.get_or_create(classifier=level,
                                                          name=nameParts['level'],
                                                          owner=department)
                            self._incCount('Classifier', createdCount if created else accessedCount)
                            classifiers.append(level)
                            classifier = level
                        groupTitle = "Unknown"
                        if "name" in detail:
                            log.error("Name is in details as %s " % detail['name'])
                            subjectCGroup, created = ClassifierGroup.objects.get_or_create(name="Subjects",
                                                            organisation=department, family="S")
                            self._incCount('ClassifierGroup', createdCount if created else accessedCount)
                            subject, created = Classifier.objects.get_or_create(value=detail['name'],
                                                            group=subjectCGroup, parent=level)
                            Classification.objects.get_or_create(classifier=subject,
                                                          name=detail['name'],
                                                          owner=department)
                            self._incCount('Classifier', createdCount if created else accessedCount)
                            classifiers.append(subject)
                            classifier = subject
                            groupTitle = detail['name']
                        elif "subject" in nameParts:
                            log.error("No Name is in details, using parsed subject as %s " % nameParts['subject'])
                            subjectCGroup, created = ClassifierGroup.objects.get_or_create(name="Subjects",
                                                            organisation=department, family="S")
                            self._incCount('ClassifierGroup', createdCount if created else accessedCount)
                            subject, created = Classifier.objects.get_or_create(value=nameParts['subject'],
                                                            group=subjectCGroup, parent=level)
                            Classification.objects.get_or_create(classifier=subject,
                                                          name=nameParts['subject'],
                                                          owner=department)
                            self._incCount('Classifier', createdCount if created else accessedCount)
                            classifiers.append(subject)
                            classifier = subject
                            groupTitle = nameParts['subject']
                        elif "name" in nameParts:
                            log.warn("No Name for group found in JSON, probably an error using pasrsed name %s  " % nameParts['name'])
                            combinedCGroup, created = ClassifierGroup.objects.get_or_create(name="Combined",
                                                            organisation=department, family="C")
                            self._incCount('ClassifierGroup', createdCount if created else accessedCount)
                            combined, created = Classifier.objects.get_or_create(value=nameParts['name'],
                                                            group=combinedCGroup, parent=level)
                            Classification.objects.get_or_create(classifier=subject,
                                                          name=nameParts['name'],
                                                          owner=department)
                            self._incCount('Classifier', createdCount if created else accessedCount)
                            classifiers.append(combined)
                            classifier = combined
                            groupTitle = nameParts['name']
                            

                        if "groups" in detail:
                            for g in detail['groups']:
                                # TODO:
                                # /groups[]/code
                                # /groups[]/element[]/code
                                # /groups[]/element[]/eid
                                # /groups[]/element[]/who
                                # /groups[]/element[]/merge
                                if 'location' in g:
                                    location, created = Location.objects.get_or_create(name=g['location'])
                                else:
                                    location, created = Location.objects.get_or_create(name="Unknown")
                                self._incCount('Location', createdCount if created else accessedCount)
                                termInfo, created = TermInfo.objects.get_or_create(name=g['term'].lower())
                                self._incCount('TermInfo', createdCount if created else accessedCount)
                                try:
                                    term = Term.objects.get(term_info=termInfo, academic_year=ayear)
                                except Term.DoesNotExist:
                                    term, created = Term.objects.get_or_create(term_info=termInfo,
                                                                               academic_year=ayear,
                                                                               start=datetime.date.today(),
                                                                               end=datetime.date.today())
                                    self._incCount('Term', createdCount if created else accessedCount)
                                # this might be wrong. it could be the name of the Event group rather than the type.
                                eventType, created = EventType.objects.get_or_create(name=g['name'])
                                self._incCount('EventType', createdCount if created else accessedCount)
                                n = n + 1
                                eventGroup, created = EventGroup.objects.get_or_create(
                                                        name=groupTitle,
                                                        publication_status='U',
                                                        owning_organisation=department,
                                                        code="%s%s" % (p['id'][:6], n),
                                                        term=term,
                                                        default_pattern=g['code'],
                                                        default_location=location,
                                                        event_type=eventType)
                                self._incCount('EventGroup', createdCount if created else accessedCount)
                                groupUsage, created = GroupUsage.objects.get_or_create(
                                                        organisation=department,
                                                        group=eventGroup)
                                self._incCount('GroupUsage', createdCount if created else accessedCount)
                                for classifier in classifiers:
                                    groupUsage.classifiers.add(classifier)
                                for e in g['elements']:
                                    '''
                                    {
                        "what": "Systems Biology",
                        "code": " x8",
                        "who": "Example person",
                        "when": " x8",
                        "merge": 0,
                        "eid": "Ee8143b54e72ed0ea44ea140fb3ef7eb4",
                        "where": "Example location"
                    }
                                    '''
                                    if 'where' in e:
                                        esLocation, created = Location.objects.get_or_create(name=e['where'])
                                        self._incCount('Location', createdCount if created else accessedCount)
                                    else:
                                        esLocation = location
                                    if 'what' in e:
                                        what = e['what']
                                    else:
                                        what = e['Unnamed']
                                    if 'when' in e:
                                        ewhen = e['when']
                                    else:
                                        ewhen = ""

                                    es, created = EventSeries.objects.get_or_create(
                                                                title=what,
                                                                date_time_pattern=ewhen,
                                                                location=esLocation,
                                                                owning_group=eventGroup
                                                                              )
                                    
                                    self._incCount('EventSeries', createdCount if created else accessedCount)

                                groupUsage.save()

                        # TODO: From the JSON we dont process
                        # /organiser
                        # /id
                        # /metadata
                        # /where
                        # /vhash
                if partsProcessed > 0:
                    self._dumpCount("Created", createdCount)
                    self._dumpCount("Accessed", accessedCount)

    def _incCount(self, name, counters):
        if name in counters:
            counters[name] = counters[name] + 1
        else:
            counters[name] = 1

    def _dumpCount(self, m, counters):
        log.info("%s %s" % (m, counters))

    def _parseSet(self, id, patterns):
        for r in patterns:
            m = r.match(id)
            if m is not None:
                    return m.groupdict()
        return None
    def _parsePartName(self, id):
        return self._parseSet(id, NAME_PATTERNS)
    def _parseId(self, id):
        return self._parseSet(id, ID_PATTERNS)
