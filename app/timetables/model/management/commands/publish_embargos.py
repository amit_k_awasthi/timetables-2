'''
Created on May 23, 2012

@author: ieb
'''
from django.core.management.base import BaseCommand
from timetables.model.models import EventGroup
import logging
import datetime

log = logging.getLogger(__name__)
del logging

class Command(BaseCommand):
    args = 'None'
    help = 'Publishes any timetables embargoed till today or earlier'


    def handle(self, *args, **options):
        n = EventGroup.objects.filter(embargo_date__lte=datetime.date.today(),publication_status='U').update(publication_status='P')
        log.info("Published %s Event Groups " % n)


