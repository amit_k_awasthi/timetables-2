from haystack.management.commands import clear_index
from django.conf import settings

class Command(clear_index.Command):
    """
    Overrides haystack's clear_index command to work around a bug.
    
    Haystack's clear_index command will fail if SILENTLY_FAIL is disabled in
    settings for an elasticsearch connection. This command overrides haystack's
    clear_index command to enable SILENTLY_FAIL for all elasticsearch indexes.
    
    Delete this file once haystack fixes this, or some other workaround is
    found.
    """

    def handle(self, **options):

        print """
WARNING: Hacking settings to enable SILENTLY_FAIL on elastic search connections.
  (Because otherwise clear_index fails with an exception. See:
  https://groups.google.com/d/topic/django-haystack/DJ3s7_uw5jM/discussion
  -- timetables.models.management.commands.clear_index)
"""

        # Override the settings to enable silent failure for elasticsearch
        self.enable_silent_failure_for_elasticsearch(
                settings.HAYSTACK_CONNECTIONS)

        # Call the real haystack clear_index command.
        clear_index.Command.handle(self, **options)

        # No point in restoring original settings, as Django will terminate 
        # after executing commands has finished. 

    def enable_silent_failure_for_elasticsearch(self, connections):
        """
        Updates a dict of haystack connection settings to enable SILENTLY_FAIL
        on elastic search connections.
        """
        for connection in connections.values():
            if connection.get("ENGINE") == ("haystack.backends."
                    "elasticsearch_backend.ElasticsearchSearchEngine"):
                connection["SILENTLY_FAIL"] = True
