'''
Created on May 15, 2012

@author: ieb
'''
from django.core.management.base import BaseCommand
from timetables.model.management import dataset
class Command(BaseCommand):
    args = ''
    help = 'Loads test data to populate the db for the first time, allowing dump'


    def handle(self, *args, **options):
        dataset.load_test_data()
