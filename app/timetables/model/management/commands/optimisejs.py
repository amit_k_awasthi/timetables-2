from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import subprocess
from os import path

class Command(BaseCommand):

    args = ""
    help = """Runs the requireJS optimiser on each build profile listed in the
REQUIREJS_BUILD_PROFILES settings entry.""".replace("\n", " ")

    def handle(self, *args, **options):
        if args:
            raise CommandError("This command takes no arguments. Received: %s"
                    % " ".join(args))
        build_profiles = getattr(settings, "REQUIREJS_BUILD_PROFILES", [])

        if (not hasattr(build_profiles, "__len__")
                or isinstance(build_profiles, basestring)):
            raise CommandError("REQUIREJS_BUILD_PROFILES should be a "
                    "collection, but got a %s: %s"
                    % (type(build_profiles), build_profiles))

        if not build_profiles:
            raise CommandError("No requirejs build profiles found. List the "
                    "paths of your build profiles in the "
                    "REQUIREJS_BUILD_PROFILES settings key.")

        self.stderr.write("%d optimisation profiles to run:\n   - %s\n\n" % (
                len(build_profiles), "\n   - ".join(build_profiles)))

        for profile in build_profiles:
            self.stderr.write("Running: %s\n" % profile)
            self.run_profile(profile)
            self.stderr.write("\n")

    @staticmethod
    def r_dot_js_path():
        return path.abspath(path.join(path.dirname(__file__), "r.js"))

    def run_profile(self, profile): 
        args = ["node","-v"]
        try:
            subprocess.call(args, stdout=self.stdout, stderr=self.stderr)
            hasNode = True
        except:
            hasNode = False
        if hasNode:
            args = ["node", self.r_dot_js_path(), "-o", profile]
        else:
            args = ["java", "org.mozilla.javascript.tools.shell.Main",
                self.r_dot_js_path(), "-o", profile]
        self.stderr.write("Command being executed is: %s with arguments: %s\n"
                % (args[0], str(args[1:])))
        self.stderr.write("v" * 80 + "\n\n")

        # Run the optimiser
        status = subprocess.call(args, stdout=self.stdout, stderr=self.stderr)

        self.stderr.write("\n" + "^" * 80 + "\n")
        if not status == 0:
            raise CommandError("Optimisation process did not complete "
                    "successfully for profile: %s\n" % profile)
        else:
            self.stderr.write("Profile ran successfully: %s\n" % profile)
