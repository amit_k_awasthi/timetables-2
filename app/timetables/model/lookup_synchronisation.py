from django.core.paginator import Paginator
import django.dispatch

from timetables.utils.lookup import Lookup
from timetables.model.models import User

import logging
import traceback

log = logging.getLogger(__name__)

pre_user_lookup_sync = django.dispatch.Signal(providing_args=["django_user",
        "lookup_user"])
post_user_lookup_sync = django.dispatch.Signal(providing_args=["django_user",
        "lookup_user"])

def synchronise_user(user, save_user=True):
    """
    Performs a one off synchronisation of a single user. If you have more than
    one user to sync at once then use bulk_synchronise_users instead as it
    should be more efficient.
    
    args:
        user: A Django User model (or the timetables User model proxy)
        save_user: If False, the user won't be save()d after updating. 
            Default: True
    returns:
        True if the user was synchronised, False otherwise.
    """
    with Lookup() as lookup:
        lookup_user = lookup.get_user(user.username)
    if not lookup_user or _in_sync(user, lookup_user):
        return False
    _sync_user(user, lookup_user, save_user=save_user)
    return True


def bulk_synchronise_users(users=None, batch_size=256):
    """
    Synchronises the specified collection of User objects with Lookup.cam.ac.uk.
    
    args:
        users: A list or queryset of User instances
        batch_size: The number of users to fetch at once from lookup/the db (if
            users is a queryset)
    """
    if users is None:
        users = User.objects.only("id", "username", "email", "last_name")
    paginator = Paginator(users, batch_size)

    with Lookup() as lookup:
        for page in (paginator.page(n) for n in paginator.page_range):
            django_users = page.object_list
            synchronise_users(django_users, lookup)

def synchronise_users(django_users, lookup):
    """
    Synchronises the provided django User instances with the lookup.cam service,
    using the provided lookup object (an instance of 
    timetables.utils.lookup.Lookup).
    """
    lookup_users = lookup.get_users(
            (user.username for user in django_users))
    for dj_user in django_users:
        lookup_user = lookup_users.get(dj_user.username)

        if lookup_user is None:
            log.warning("User not found in lookup: %s" %
                    dj_user.username)
            continue

        if not _in_sync(dj_user, lookup_user):
            _sync_user(dj_user, lookup_user)

def _ensure_users_match(dj_user, lookup_user):
    assert dj_user.username == lookup_user.crsid, ("mismatching dj and lookup "
            "users. dj username: %s, lookup crsid: %s" %
            (dj_user.username, lookup_user.crsid))

def _in_sync(dj_user, lookup_user):
    """
    returns: 
        True if the django user is up to date with the lookup version of itself.
    """
    _ensure_users_match(dj_user, lookup_user)

    return (dj_user.email == lookup_user.email and
            dj_user.last_name == lookup_user.name)

def _sync_user(dj_user, lookup_user, save_user=True):
    _ensure_users_match(dj_user, lookup_user)

    pre_user_lookup_sync.send(None, django_user=dj_user,
            lookup_user=lookup_user)

    # We store names entirely under last_name as lookup does not distinguish
    # between name components.
    try:
        dj_user.last_name = lookup_user.name
        dj_user.email = lookup_user.email
        # TIMETABLES-353
        if dj_user.email is None:
            dj_user.email = "%s@cam.ac.uk" % dj_user.username
        if save_user:
            dj_user.save()
        post_user_lookup_sync.send(None, django_user=dj_user,
                lookup_user=lookup_user)
    except:
        log.error(traceback.format_exc())


