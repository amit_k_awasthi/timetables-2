from django.contrib import admin
from timetables.model.models import *

#------------------- DJango ADMINISTRATION -------------------------------------

class TermInfoAdmin(admin.ModelAdmin):
    list_display = ("name", "term_type", "day", "is_standard")

class TermInfoInline(admin.StackedInline):
    model = TermInfo

class TermInline(admin.StackedInline):
    list_display = ('name', 'year', 'start', 'end',)
    model = Term
    inlines = [
        TermInfoInline
    ]

    def name(self, obj):
        return obj.terminfo.name

    def year(self, obj):
        return obj.academic_year.name

class AcademicYearAdmin(admin.ModelAdmin):
    list_display = ('name', "is_archived")
    inlines = [
        TermInline
    ]

class TermAdmin(admin.ModelAdmin):
    list_display = ("academic_year", "term_info", "start", "end")

class ClassifierAdmin(admin.ModelAdmin):
    list_display = ("value", "group")

class GroupUsageAdmin(admin.ModelAdmin):
    list_display = ("group", "organisation")

class EventGroupAdmin(admin.ModelAdmin):
    list_display = ("name", "owning_organisation", "publication_status","embargo_date")

class EventSeriesAdmin(admin.ModelAdmin):
    list_display = ("title", "owning_group")

class EventAdmin(admin.ModelAdmin):
    list_display = ("overridden_title", "owning_series")
    
class CollisionEventsAdmin(admin.ModelAdmin):
    list_display = ("event", "timeslot")

admin.site.register(AcademicYear, AcademicYearAdmin)
admin.site.register(CollisionEvents, CollisionEventsAdmin)
admin.site.register(Role)
admin.site.register(Permission)
admin.site.register(RolePermission)
admin.site.register(TermInfo, TermInfoAdmin)
admin.site.register(Term, TermAdmin)
admin.site.register(OrganisationRole)
admin.site.register(Location)
admin.site.register(EventType)
admin.site.register(Organisation)
admin.site.register(ClassifierGroup)
admin.site.register(Classifier, ClassifierAdmin)
admin.site.register(Classification)
admin.site.register(GroupUsage, GroupUsageAdmin)
admin.site.register(GroupUsageVersion)
admin.site.register(EventGroup, EventGroupAdmin)
admin.site.register(EventGroupVersion)
admin.site.register(EventSeries, EventSeriesAdmin)
admin.site.register(EventSeriesVersion)
admin.site.register(TimeInterval)
admin.site.register(Event, EventAdmin)
admin.site.register(EventVersion)
admin.site.register(Revision)
admin.site.register(GroupRevision)
admin.site.register(PersonalTimetable)
admin.site.register(PersonalTimetableUsage)
