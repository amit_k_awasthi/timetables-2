from django.conf.urls.defaults import patterns, url

from timetables.student.views import (LoginView, LogoutView, LoginErrorView,
        ErrorView, pt_action_notification, pt_get_event_series)

from timetables.student.views.events import (BrowseEventsView, SearchEventsView,
        timetable_custom_add, timetable_custom_remove, BrowseSeriesIds,
        SearchSeriesIds, MoreBrowseEventsView, MoreSearchEventsView)

from timetables.student.views import (events, timetable_custom_view,
    timetable_notification_view)

from timetables.student.views.calendar import (TimetableMonthView, 
        TimetableTermView, TimetableWeekView, TimetableListView,
        timetable_redirect)

from timetables.student.views.feeds import icalendar, fullcalendar
from timetables.student.views.home import HomeView
from timetables.views import todo

ORG_CODE = r"(?P<org_code>[a-zA-Z0-9_\-]*)"
YEAR = r"(?P<year_start>\d{4})"
YEAR_OPT = r"(?P<year_start>[\d{4}]*)"
CRSID = r"(?P<crsid>[a-zA-Z0-9]*)"
CRSID_ = r"([a-zA-Z0-9]+)"
# matches both URLs with the YEAR and org code or just the YEAR eg
# /events/2011/
# /events/2011/2343242/
ORG_YEAR = r""+YEAR+"/"+ORG_CODE+"/?"
CRSID_YEAR = r""+CRSID+"/"+YEAR_OPT+"/?"

YEAR_MONTH = r"(?P<year>[0-9]{4})/(?P<month>[0-9]{1,2})"

urlpatterns = patterns("",
    url(r"^$",
            HomeView.as_view(),
            name="student home loggedin"),

    url(r"^f/personal-timetable/action-notification/$",
            pt_action_notification,
            name="student personal timetable notification fragment"),
             
    url(r"^events/" + ORG_YEAR + "$",
            BrowseEventsView.as_view(),
            name="student browse events"),
    
    url(r"^events/" + ORG_YEAR + "/more$",
            MoreBrowseEventsView.as_view()),

    # get lecture list - redundant?
    #url(r"^events/" + ORG_YEAR + "f/lectures/$",
    #        events.events_lectures_fragment,
    #        name="student browse lectures fragment"),


    # personal timetable fragment
    url(r"^events/f/personal-timetable/" + CRSID_YEAR + "$",
            pt_get_event_series,
            name="student personal timetable fragment"),

    url(r"^events-search/" + ORG_YEAR + "$",
            SearchEventsView.as_view(),
            name="student search events"),

    url(r"^events-search/" + ORG_YEAR + "/more$",
            MoreSearchEventsView.as_view()),

    # Add event series to a student timetable:
    url(r"^events-search/" + ORG_YEAR + "/add/$",
            timetable_custom_add,
            name="student search add events"),

    # Remove event series from a student timetable:
    url(r"^events-search/" + ORG_YEAR + "/remove/$",
            timetable_custom_remove,
            name="student search remove events"),

    # Obtain a list of event series ids based on the
    # current page faceting / search:
    url(r"^events-search/" + ORG_YEAR + "/series_ids/$",
            SearchSeriesIds.as_view(),
            name="student search event series ids"),

    url(r"^events/" + ORG_YEAR + "$",
            BrowseEventsView.as_view(),
            name="student browse organisation events"),

    # Add event series to a student timetable:
    url(r"^events/" + ORG_YEAR + "/add/$",
            timetable_custom_add,
            name="student browse add events"),

    # Remove event series from a student timetable:
    url(r"^events/" + ORG_YEAR + "/remove/$",
            timetable_custom_remove,
            name="student browse remove events"),

    # Obtain a list of event series ids based on the
    # current page faceting:
    url(r"^events/" + ORG_YEAR + "/series_ids/$",
            BrowseSeriesIds.as_view(),
            name="student browse event series ids"),

    #url(r'^events/" + ORG_YEAR + "f/lectures/$',
    #        events.events_lectures_fragment,
    #        name="student browse lectures fragment"),

    # Get details of a specified EventGroup
    url(r"^f/details/subject/(?P<id_subject>[0-9]+)/$",
        events.subject_details,
        name="event group details fragment" ),


    url(r"^timetable/admin/"+ ORG_YEAR + "$",
            todo,
            name="student timetable admin preview"),

    # Handles the POST request from "I am an Xth year
    # student studying Faculty, show me my lectures":
    url(r"^timetable/custom/"+ YEAR + "$",
            timetable_custom_view,
            name="student timetable customview"),

    url(r"^timetable/notifications$",
            timetable_notification_view,
            name="student timetable notificationview"),


    # Calendar related views
    
    # Redirect users somewhere sensible if they hit /timetable/ or
    # /timetable/CRSID/
    url(r"^timetable/$", timetable_redirect, name="student timetable"),
    url(r"^timetable/" + CRSID + "/$", timetable_redirect),

    url(r"^timetable/" + CRSID + "/month/$",
            TimetableMonthView.as_view(),
            name="student timetable monthview"),

    url(r"^timetable/" + CRSID + "/terms/$",
            TimetableTermView.as_view(),
            name="student timetable termview"),
    
    url(r"^timetable/" + CRSID + "/terms/" + YEAR + "/$",
            TimetableTermView.as_view(),
            name="student timetable termview atyear"),

    url(r"^timetable/" + CRSID + "/list/$",
            TimetableListView.as_view(),
            name="student timetable listview"),
    # Allow a specific year + month in the path
    url(r"^timetable/" + CRSID + "/list/" + YEAR_MONTH + "/$",
            TimetableListView.as_view(),
            name="student timetable listview yearmonth"),

    url(r"^timetable/" + CRSID + "/week/$",
            TimetableWeekView.as_view(),
            name="student timetable weekview"),


    # Error views
    url(r"^404$",
            ErrorView.as_view(error_code=404),
            name="student 404"),
    url(r"^403$",
            ErrorView.as_view(error_code=403),
            name="student 403"),
    url(r"^500$",
            ErrorView.as_view(error_code=500),
            name="student 500"),


    url(r'^accounts/login',
            LoginView.as_view(),
            name="login url"),

    url(r'^accounts/logout',
            LogoutView.as_view(),
            name="logout url"),

    url(r'^accounts/error',
            LoginErrorView.as_view(),
            name="login error"),


    # Timetable/Event feeds
    url(r"^feeds/icalendar/debug/year/" + YEAR + ".ics",
            icalendar.YearIcalendarFeedView.as_view()),
    url(r"^feeds/icalendar/personaltimetable/" + CRSID + ".ics",
            icalendar.PersonalTimetableIcalendarFeedView.as_view(),
            name="student feed icalendar personal"),
    
    url(r"^feeds/fullcalendar/personaltimetable/" + CRSID_ + ".json",
            fullcalendar.PersonalTimetableEventFeed.as_view())
)
