from timetables.model.models import Event
from timetables.utils.ints import int_safe
from timetables.utils.Json import JSON_CONTENT_TYPE, JSON_INDENT

from django.views.generic import View
from django.http import HttpResponseForbidden, HttpResponse,\
    HttpResponseBadRequest

import json, datetime, logging, pytz

log = logging.getLogger(__name__)

class FullcalendarEventFeedView(View):
    def set_headers(self, response):
        response["Content-Type"] = JSON_CONTENT_TYPE
        return response
    
    def build_event_stream(self, events):
        """
        Converts a sequence of Event objects into a JSON encoded event stream
        suitable for passing to the fullcalendar js library.
        """
        event_stream = [self.to_event_stream_format(event) for event in events]
        
        return json.dumps(event_stream, indent=JSON_INDENT)
    
    def to_event_stream_format(self, event):
        """
        Converts an Event object into a dictionary representing a fullcalendar
        js object.
        
        See: http://arshaw.com/fullcalendar/docs/event_data/Event_Object/ 
        """
        return {
            "id": event.id,
            "title": event.get_title(),
            "start": event.get_timeslot().start_time.isoformat(),
            "end": event.get_timeslot().end_time.isoformat()
        }
    
    def _parse_iso_datetime(self, datestr):
        # Why doesn't Python have a sensible way to parse common date formats
        # in its std library is beyond me.
        try:
            # Only support UTC (Z prefix) dates, because it takes a ridiculous
            # amount of effort to support offsets, and this is only used to
            # debug...
            dt = datetime.datetime.strptime(datestr, "%Y-%m-%dT%H:%M:%SZ")
        except ValueError:
            log.exception("Bad ISO datetime timestamp: %s" % datestr)
            return None
        dt.replace(tzinfo=pytz.utc)
        return dt
    
    def _get_datetime(self, timestamp_string):
        
        # Try to interpret as a unix timestamp
        timestamp = int_safe(timestamp_string)
        if timestamp is not None:
            try:
                dt = datetime.datetime.utcfromtimestamp(timestamp)
                dt.replace(tzinfo=pytz.utc)
                return dt
            except ValueError:
                log.exception("Bad timestamp: %d" % timestamp)
                return None
        
        # Try to interpret as an ISO date
        return self._parse_iso_datetime(timestamp_string)
    
    def get_request_range(self, request_params):
        """
        Interprets the start=xxx&end=xxx params passed by fullcalendar to
        specify the range of events requested.
        
        Args:
            request_params: The request.GET querydict.
        Returns:
            A tuple of (start, end) where start and end are datetime.datetime
            instances. None is returned if one or both of start and end are
            missing/malformed.
        """
        start_val = request_params.get("start")
        end_val = request_params.get("end")
        
        if not start_val and not end_val:
            return None
        
        if bool(start_val) ^ bool(end_val):
            raise ValueError("start & end cannot be used on their own, specify "
                    "both or neither.")
        
        start = self._get_datetime(start_val) 
        end = self._get_datetime(end_val)
        
        if bool(start) ^ bool(end):
            raise ValueError("Invalid start/end date.")
        
        return (start, end)


class PersonalTimetableEventFeed(FullcalendarEventFeedView):
    
    def get_events(self, username, interval):
        """
        Gets a sequence of Events to convert into an event feed.
        
        Args:
            username: The username of the user whose personal timetable the
                events are to be from.
            interval: A tuple of (start, end) datetime.datetime objects defining
                the time interval that events should intersect.
        Returns: A sequence of Event objects.
        """
        events = Event.objects.all().in_users_timetable(username)
        
        if interval is not None:
            start, end = interval
            events = events.in_range(start, end)
        
        return events
    
    def can_access_timetable(self, request, owner_username):
        return request.user.username == owner_username
    
    def get(self, request, username):
        if not self.can_access_timetable(request, username):
            return HttpResponseForbidden(
                    "You don't have permission to view this timetable.")
        
        try:
            interval = self.get_request_range(request.GET)
        except ValueError as e:
            return HttpResponseBadRequest(e.message)
        
        event_stream = self.build_event_stream(
                self.get_events(username, interval))
        
        return self.set_headers(HttpResponse(event_stream))
