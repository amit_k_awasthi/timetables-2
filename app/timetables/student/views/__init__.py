from django.shortcuts import render
from django.template.loader import render_to_string

from timetables.student import testdata
from timetables.superadministrator.views.overview import term_dates_table
from timetables.utils.requirejs import js_main_module
from timetables.views import BaseView
from timetables.model.models import User as TimetablesUser, UserProfile
from timetables.model.models import AcademicYear, CollisionEvents, Event, EventSeries, Organisation, PersonalTimetableUsage

from django.contrib.auth import logout, login, authenticate
from django.http import HttpResponse, HttpResponseRedirect, Http404,\
        HttpResponseBadRequest
from django.core.urlresolvers import reverse, resolve, NoReverseMatch
from django.contrib.auth.models import User
from django.utils.http import urlencode
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.utils.datetime_safe import date
from django.utils import simplejson as json

import datetime
import logging
import traceback

log = logging.getLogger(__name__)

class StudentBaseView(BaseView):
    "The base class of all Student views."
    class Meta(object):
        abstract = True

    def __init__(self):
        super(StudentBaseView, self).__init__(None)

    def dispatch(self, *args, **kwargs):
        return super(StudentBaseView, self).dispatch(*args, **kwargs)

    def get(self, request, year_start=None, org_code=None, **kwargs):
        new_year, new_org = self.locate_organisation(year_start, org_code)
        if new_year is not None:
            # If the URL didnt exist, redirect to the cloests match
            ka = { "year_start" : new_year, "org_code" : new_org }
            try:
                return HttpResponseRedirect(reverse(resolve(request.path_info).url_name, kwargs=ka))
            except NoReverseMatch:
                pass
        return render(request, self.template,
                              self.context(request, **kwargs))

    def navigation(self):
        pass

    def locate_organisation(self, year_start, org_code):
        """Gets a tuple of (AcademicYear, Organisation) for the provided args.

        Raises:
            django.http.Http404: If no organisation exists for the provided year
                and code.
        """

        if hasattr(self, "year") and hasattr(self, "org"):
            # already done by the permissions check, no point in doing this twice.
            # If you want to do it twice del year and org first.
            return
        if year_start is None:
            today = date.today()
            year_start = today.year
            if today.month < 6: # assumes that academic year starts on 1st June
                year_start = year_start - 1

        try:
            org = Organisation.objects.select_related("year").get(code=org_code, year__starting_year=year_start)
        except Organisation.DoesNotExist:
            log.error("Org %s for year %s Does not exist " % ( org_code, year_start))
            try:
                org = Organisation.objects.filter(year__starting_year=year_start).select_related("year")[0]
            except:
                log.error("No Orgs in year %s " % ( year_start))
                org = Organisation.objects.all().select_related("year")[0]
        self.year = org.year
        self.org = org
        if not (str(self.year.url_id()) == year_start):
            return self.year.url_id(), ""
        if org_code is not None and len(org_code) > 0 and not self.org.url_id() == org_code:
            return self.year.url_id(), self.org.url_id()
        return None, None

    def js_main_module(self):
        return "student/student-base"

    def context(self, request, based_on={}, order_by="name"):
        if "_testdata" in request.REQUEST:
            context = (based_on or {}).copy()
        else:
            context = {}
        context.update(super(StudentBaseView, self).context(request))

        try:
            if self.year and self.org:
                context["page_year"] = self.year
                context["page_organisation"] = self.org
                context["years"] = AcademicYear.objects.active_years()
                context["organisations"] = Organisation.objects.filter(year=self.year).order_by(order_by)
            elif self.year:
                context["page_year"] = self.year
                context["years"] = AcademicYear.objects.active_years()
                context["organisations"] = Organisation.objects.filter(year=self.year).order_by(order_by)
            elif self.org:
                context["page_year"] = self.org.year_set[0]
                context["page_organisation"] = self.org
                context["years"] = AcademicYear.objects.active_years()
                context["organisations"] = Organisation.objects.filter(year=self.org.year_set[0]).order_by(order_by)
            else:
                context["page_year"] = AcademicYear.get_current()
                context["years"] = AcademicYear.objects.active_years()
        except:
            log.error(traceback.format_exc())


        return context

def timetable_custom_view(request, **kwargs):
    ''' This is used to process the "I am A blah year student, studying blah". It essentially
    issues a redirect to the appropriate browse events page, given the subject and year of
    study entered:

    '''

    # Use the current year if we do not, for some strange reason, receive the
    # year_start in the keyword arguments:
    year_start = datetime.datetime.now().year
    try:
        year_start = kwargs['year_start']
    except:
        log.warn("Error fetching year_start in timetable_custom_view");

    # It is expected that the request will contain a student_subject parameter,
    # this is the faculty / organisation that the student will be studying under:
    try:
        student_subject = request.GET.__getitem__('student_subject')
    except:
        log.error("No student subject selected in timetable_custom_view");
        return HttpResponseBadRequest()

    # It is also expected that the request will contain a student_year parameter,
    # this is the year the student is a member of:
    try:
        student_year = request.GET.__getitem__('student_year')
    except:
        log.error("No student year selected in timetable_custom_view");
        return HttpResponseBadRequest()
    # Return a redirect to the browse events page with the year and subject provided in the
    # url structure and the student_year passed in as a request parameter.
    return HttpResponseRedirect(
        '/events/' + year_start + '/' + student_subject + '?student_year=' + student_year)

@xact
def timetable_notification_view(request):
    ''' This handles changing a user's timetable change
    notification settings.

    '''
    # Check that the user making the request is correctly logged in:
    if request.user is None:
        return HttpResponseBadRequest()
    if request.user.is_anonymous():
        return HttpResponseBadRequest()

    # See whether the user has requested notification or not:
    try:
        notify = ( request.POST.__getitem__('notify') == 'true' )
    except:
        log.error("No notification option specified");
        return HttpResponseBadRequest()

    # Extract the user notification email address:
    try:
        email = request.POST.__getitem__('email')
    except:
        log.error("No notification email specified");
        return HttpResponseBadRequest()

    # Fetch the user profile or create a new one if it does not already exist:
    userprofile, created = UserProfile.objects.get_or_create(user_id=request.user.id,
                  defaults={'is_notified': notify, 'notification_email': email})
    # If the user profile already existed, then update it:
    if ( not created ):
        userprofile.is_notified = notify
        userprofile.notification_email = email
        userprofile.save()

    return HttpResponse("timetable notification settings updated with notify: %s and email: %s" % ( notify, email ), content_type="text/plain")


class ErrorView(BaseView):

    error_code = 404

    def __init__(self):
        super(ErrorView, self).__init__(None)

    def get(self, request, request_path=None ):
        context = self.context(request)
        context['request_path'] = request_path
        return render(request,
            "%s.html" % self.error_code, context)



class LoginView(BaseView):

    def __init__(self):
        super(LoginView, self).__init__(None)

    def get(self, request):
        if request.user.is_authenticated():
            if "next" in request.REQUEST:
                return HttpResponseRedirect(request.REQUEST['next'])
            return HttpResponseRedirect(reverse('home'))
        if "HTTP_REMOTE_USER" in request.META:
            try:
                user = User.objects.get_by_natural_key(request.META['HTTP_REMOTE_USER'])
            except User.DoesNotExist:
                # TODO, create the user, since we trust the REMOTE_USER info
                pass
            return self._do_login(request, user, reverse('login error'))

        context = self.context(request)
        if "next" in request.GET:
            context["next"] = request.GET["next"]
        if "error" in request.GET:
            context['error'] = request.GET['error']

        return render(request,
            "login.html", context)

    @method_decorator(xact)
    def post(self, request):
        user = None
        if request.method == "POST":
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
        return self._do_login(request, user, reverse('login url'))


    def _do_login(self, request, user, error_target):
        if user is None:
            # login invalid
            if "next" in request.REQUEST:
                url = "%s?%s" % (error_target, urlencode({'next': request.REQUEST["next"], 'error' : 'invalid_login'}))
            else:
                url = "%s?error=invalid_login" % error_target
            return HttpResponseRedirect(url)
        if user.is_active:
            login(request, user)
            if "next" in request.REQUEST:
                return HttpResponseRedirect(request.REQUEST['next'])
            return HttpResponseRedirect(reverse('student home loggedin'))
        else:
            # Account disabled
            if "next" in request.REQUEST:
                url = "%s?%s" % (error_target, urlencode({'next': request.REQUEST["next"], 'error' : 'account_disabled'}))
            else:
                url = "%s?error=account_disabled" % error_target
            return HttpResponseRedirect(url)


class LogoutView(BaseView):

    def __init__(self):
        super(LogoutView, self).__init__(None)

    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('student home loggedin'))


class LoginErrorView(BaseView):

    def __init__(self):
        super(LoginErrorView, self).__init__(None)

    def get(self, request):
        context = self.context(request)
        if "next" in request.GET:
            context["next"] = request.GET["next"]
        if "error" in request.GET:
            context['error'] = request.GET['error']

        return render(request,
            "login_error.html", context)


def pt_action_notification(request):
    """ Generates the HTML to display as notification when the user adds / removes
    items to / from their personal timetable.
    Request should contain tuple of [EventSeries, GroupUsage] ID pairs.
    """
    actioned = request.POST.get("actioned")
    actioned = json.loads(actioned)
    
    action = request.POST.get("action")
    action_text = "added to"
    if action == "remove":
        action_text = "removed from"
    
    ids_event_series = []
    for item in actioned:
        ids_event_series.append( item[0] )
        
    event_series = EventSeries.objects.filter(pk__in=ids_event_series)
    
    # return rendered list of event series
    return render(request, "student/fragments/pt-action-notification.html", { "event_series": event_series, "action_text": action_text })


def pt_get_ids( request, year ):
    """ Get the personal timetable for the current user.
    Returns dictionary with GroupUsage IDs as key and array of EventSeries IDs as content
    """
    user = request.user

    # Anonymous users don't have timetables. Callers of this function should
    # probably avoid calling us unless user is logged in.
    if not user.is_authenticated():
        return {}

    timetables = PersonalTimetableUsage.objects.filter(timetable__owner=user).filter(timetable__year=year).prefetch_related("group__classifiers__group")
    pt = {}
    for timetable in timetables:
        if not pt.has_key(timetable.group.id):
            pt[timetable.group.id] = []
        pt[timetable.group.id].append( timetable.series.id )
        
    #pt_occupied = pt_collision_check( 1, pt )
        
    return pt


def pt_collision_check( request, new_lectures, pt ):
    """ 
    new_lectures - pairs of [Series Event ID, Group Usage ID] corresponding to the lectures the user is attempting to add
    pt - optional, the existing personal timetable of the user as returned by pt_get_ids();
    """

    # get the occupied timeslots for the user's personal timetable
    pt_series = []
    for id_group, ids_series in pt.items():
        for id_series in ids_series:
            pt_series.append( id_series )
    
    pt_o = CollisionEvents.objects.filter( series__id__in = pt_series ).values_list( 'series', 'timeslot' )
    pt_occupied = []
    for series, timeslot in pt_o: # because we can't use append on ValuesListQuerySet object
        pt_occupied.append( [series, timeslot] )
    
    pt_ids_series = []
    for series_timeslot in pt_occupied:
        pt_ids_series.append( series_timeslot[0] )
    
    # get the timeslots for the new lectures
    new_series = []
    for id_series, id_group in new_lectures:
        new_series.append( id_series )

    new_occupied = CollisionEvents.objects.filter( series__id__in = new_series ).values_list( 'series', 'timeslot', 'event' ) # hmm ok so no real point using values_list any more :|
    
    new_ids_series = []
    for series_timeslot in new_occupied:
        new_ids_series.append( series_timeslot[0] )
    
    # Get excluded event series list - these are event series which are in both the new_lectures list and the current personal timetable.
    # Note as well as preventing clash detection where there is no clash, this also allows remove action to work.
    excluded_series = set(pt_ids_series) & set(new_ids_series)
    
    # get all clashes - cardinality ( pt.length * new.length ) so potentially not great
    collisions = []
    for new_id_series, new_timeslot, new_event in new_occupied:
        found = False
        if new_id_series not in excluded_series: #  don't check "new" lectures which are already in the user's personal timetable
            for pt_id_series, pt_timeslot in pt_occupied:
                if pt_timeslot == new_timeslot:
                    found = True
                    if new_event not in collisions:
                        collisions.append( new_event )
        if not found: # if the timeslot is not found in the existing personal timetable then add it - tests for internal clashes within the lectures being added
            pt_occupied.append( [new_id_series, new_timeslot] )
        
    collisions = pt_collision_details( request, collisions )
        
    # return details of collisions to calling function
    return collisions


def pt_collision_details( request, collision_events ):
    """ Returns details of lecture collisions.
    Passed a list of events which collide
    """
    
    events = Event.objects.filter( id__in = collision_events ).prefetch_related( "owning_series" ).prefetch_related( "default_timeslot" )
    
    series = {}
    ids_series = []
    
    for event in events:
        id = event.owning_series.id
        ids_series.append(id)

        start_time = event.default_timeslot.start_time
        start_time = start_time.strftime('%b %d, %H:%M')
       
        if id in series:
            series[id]["dates"].append(start_time)
        else:
            series[id] = {
                "title": event.owning_series.title,
                "dates": [start_time]
            }

    # return rendered list of collision details
    if len(series):
        return { "html": render_to_string("student/fragments/collision-list.html", { "series": series }), "ids_series": ids_series }
    else:
        return {}


def pt_get_event_series(request, crsid, year_start = False ):
    """
    Get personal timetable's event series for the current user
    CRSID is passed to allow caching
    """
    # get logged in user and current year
    user = request.user
    if year_start:
        year_current = AcademicYear.objects.get( starting_year = year_start )
    else:
        year_current = AcademicYear.get_current()

    # load timetables list, including pre-fetching related data
    #timetables = PersonalTimetableUsage.objects.filter( timetable__owner = user ).filter( timetable__year = year_current )
    #timetables = PersonalTimetableUsage.objects.prefetch_related( "series__location" ).prefetch_related( "group__classifiers__group" ).filter( timetable__owner = user ).filter( timetable__year = year_current )
    timetables = PersonalTimetableUsage.objects.filter(timetable__owner=user).filter(timetable__year=year_current).prefetch_related("series__location").prefetch_related("series__owning_group")

    # get user's timetable for this year    
    event_series = []
    for timetable in timetables:
        # get the timetable's classifiers of type "subject"
        classifiers = timetable.group.classifiers.filter(group__family="S")
        #classifiers = timetable.group.classifiers.all()
        #classifiers = timetable.group.classifers
        subjects = []
        subjects_name = ""
        for classifier in classifiers:
            #if classifier.group.family == "S":
                subjects.append({
                    "id": classifier.id,
                    "value": classifier.value
                })
                if subjects_name == "":
                    subjects_name = classifier.group.name # assumes all classifiers are in the same classifier group - should be safe :|

        # set the data to pass to the template
        series = {
            "id_groupusage": timetable.group.id,
            "id_eventseries": timetable.series.id,
            "name": timetable.series.title,
            "description": timetable.group.group.description,
            "day_time": timetable.series.date_time_pattern,
            "location": timetable.series.location.name,
            "subjects": subjects,
            "subjects_name": subjects_name,
            "term_info": timetable.series.owning_group.term.term_info,
            "lecturers": timetable.series.lecturers()
        }
        event_series.append(series)

    # return rendered list of event series
    return render(request, "student/fragments/personal-event-series.html", { "event_series": event_series })