from django.shortcuts import render

from timetables.student.views import StudentBaseView, pt_get_ids, pt_collision_check
from timetables.model.models import AcademicYear, Classification, Event, EventSeries, GroupUsage, PersonalTimetable, PersonalTimetableUsage, User
from timetables.templatetags.templatetags.timetables_tags import humanise
from timetables.utils.ints import int_safe

from django.http import HttpResponse, HttpResponseBadRequest
from django.utils import simplejson as json
from django.db import IntegrityError

from haystack.query import RelatedSearchQuerySet

from operator import add

import logging
from haystack.inputs import AutoQuery
from timetables.utils.http import HttpParameters
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
log = logging.getLogger(__name__)

class FacetValue(object):
    """
    Represents a single option from a facet category.
    """

    def __init__(self, name, count, is_selected=False, display_name=None):
        if display_name is None:
            display_name = name

        self._name = name
        self._count = count
        self._is_selected = bool(is_selected)
        self._display_name = display_name

    def name(self):
        return self._name

    def display_name(self):
        return self._display_name

    def count(self):
        return self._count

    def is_selected(self):
        return self._is_selected

    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        return u"[%s] %s (%d)" % (
                "x" if self.is_selected() else " ",
                self.name(),
                self.count())

class Facet(object):
    """
    Represents a collection of FacetValues of a specific type.
    
    For example days, names, etc. 
    """

    INITIAL_COUNT = 7

    def __init__(self, name, values, display_name=None):
        assert all(isinstance(v, FacetValue) for v in values)
        if display_name is None:
            display_name = name
        self._name = name
        self._display_name = display_name
        self._values = values

    def name(self):
        return self._name

    def display_name(self):
        return self._display_name

    def values(self):
        return self._values

    def initial_values(self):
        return self.values()[:self.INITIAL_COUNT]

    def more_values(self):
        return self.values()[self.INITIAL_COUNT:]

    def __iter__(self):
        """
        Allow iteration directly over ourself (rather than calling values()).
        """
        return self.values().__iter__()

    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        return "%s\n  %s" % (
                self.name(), "\n  ".join(str(v) for v in self.values()))

class EventsPage(object):
    """
    Represents the assembled data shown in a Student events page.
    
    This consists of a series of Facets representing the selected/available 
    filters, a list of EventGroups (or their IDs).
    """

    def __init__(self, groupusages, facets, resulttab, offset, results_source):
        """
        Initialise an EventsPage
        
        Args:
            groups: A list of GroupUsage objects.
            facets: A list of Facet objects representing the active facet 
                filters applied to the page.
            resulttab: A Facet instance representing the available & selected
                terms.
            offset: The offset into the search results the first element of 
                groups is at. i.e. 0 for the first page of results.
            results_source: A string describing where the results are 'from'.
                On the browse page this should be the name of the organisation
                being browsed. For the search page this is the query string.
        """
        if all(isinstance(g, GroupUsage) for g in groupusages):
            self._groups = groupusages
        else:
            raise ValueError("Unexpected value for groupusages, expected a "
                    "sequence of GroupUsage models, got: %s" % groupusages)

        if not all(isinstance(f, Facet) for f in facets):
            raise ValueError("Expected facets arg to be a list of Facets, "
                    "got: %s" % facets)
        self._facets = facets

        if not isinstance(resulttab, Facet):
            raise ValueError("Expected resulttab arg to be a Facet, got: %s" %
                    resulttab)
        self._resulttab = resulttab

        if offset < 0:
            raise ValueError("offset was negative: %d" % offset)
        self._offset = offset

        if not isinstance(results_source, basestring):
            raise ValueError("Expected a string for results_source, got: %s" %
                    results_source)
        self._results_source = results_source

    def results_source(self):
        return self._results_source

    def result_count(self):
        """
        The total number of results across all tabs of results.
        """
        return reduce(add, (tab.count() for tab in self._resulttab.values()))


    def offset(self):
        """
        The offset into the search results our groups() start at.
        """
        return self._offset

    def groups(self):
        """
        Gets a list of the EventGroup models to show on this page.
        """
        return self._groups

    def facets(self):
        """
        Gets a list of the facet categories which could be/are filtering the
        list of groups shown on this page.
        """
        return self._facets

    def result_tabs(self):
        return self._resulttab

class EventGroupSearcher(object):
    """
    Performs faceted searches for events using haystack.
    
    The search() method executes the search and returns an object representing
    a page of results.
    """
    def __init__(self, narrow_choices, start=0, count=20):
        """
        Args:
            narrow_choices:
                A list of (facet-name, value) pairs representing narrowings to
                apply to the search. 
            start: The offset into the search results to 
        """
        assert all(isinstance(n, basestring) and isinstance(v, basestring)
                for (n, v) in narrow_choices), narrow_choices
        self._narrow_choices = narrow_choices
        self._start = int(start)
        self._count = int(count)

    def initial_queryset(self):
        return RelatedSearchQuerySet().all().models(GroupUsage)

    def escape(self, user_supplied_string):
        """
        Escapes special characters in a user supplied string to make it safe
        to include in a query.
        """
        return self.initial_queryset().query.clean(user_supplied_string)

    def facets(self):
        """
        Returns: A list of facet types to facet the search on.
        """
        return ["level", "topic", "event_type", "staff", "day", "term"]

    def selected_term(self):
        term_choices = [val for (key, val) in self._narrow_choices
                if key == "term"]
        term_name = term_choices[-1] if term_choices else "michaelmas"
        return term_name

    def raw_narrowings(self):
        """
        Gets a list of (facet-name, value) pairs to narrow() the search 
        queryset with.
        """

        # Exclude term from normal faceting as including it will mean we lose
        # the result counts for unselected terms.
        return [narrow for narrow in self._narrow_choices
                if narrow[0] != "term"]


    def narrowings(self):
        """
        Gets a list of encoded querystrings to pass to queryset.narrow().
        
        The output is derived from self.raw_narrowings(), so that should be 
        overridden rather than this in most cases.
        
        Returns: A list of "facet-name:value" strings to narrow the search
            queryset with.
        """
        # Pairs of (name,value)
        allowed_facets = set(self.facets())
        # Encode each name,value pair into a narrow() query
        return ["%s:%s" % (facet, self.escape(val)) for (facet, val)
                in self.raw_narrowings() if facet in allowed_facets]

    def _get_filtered_queryset(self):
        # Get the initial queryset to facet/narrow. This should be pre-filtered
        # with non-facet filters as required.
        queryset = self.initial_queryset()
        # Apply the values to facet on
        queryset = reduce(lambda qs, facet: qs.facet(facet), self.facets(),
                queryset)
        # Narrow by any provided facet values
        queryset = reduce(lambda qs, n: qs.narrow(n), self.narrowings(),
                queryset)

        return queryset

    def build_page(self, queryset):
        raise RuntimeError("Implement in subclass")

    @staticmethod
    def default_facet_handler(facet_name, values, narrowing_set):
        """
        Converts raw haystack facet output into a Facet object. 
        """
        values = [FacetValue(name, count, display_name=humanise(name),
                    is_selected=((facet_name, name) in narrowing_set))
                for (name, count) in values]
        return Facet(facet_name, values, display_name=humanise(facet_name))

    def process_result_facets(self, raw_fields):
        """
        Creates a list of Facet objects from the raw haystack facet output.
        
        The appropriate handler for each raw facet is obtained from
        facet_handler().
        """
        narrowing_set = set(self.raw_narrowings())

        facets = []
        for (facet_name, values) in raw_fields.items():
            handler = self.facet_handler(facet_name)
            facets.append(handler(facet_name, values, narrowing_set))
        return facets

    def facet_handler(self, name):
        """
        Gets a function to build a Facet object for the facet with the given 
        name.
        
        By default this returns default_facet_handler, but can be overriden to
        process facets in a specific way if needed. E.g. to fetch info from the
        db or reformat the names etc.
        """
        return self.default_facet_handler

    def evaluate_queryset(self, queryset, paginate=True):
        """
        Converts the haystack queryset into appropriate objects and calls 
        build_page() with the evaluated data. By default the page will be
        paginated, but you can turn off pagination by setting paginate to
        false.
        
        Returns: The value build_page() returns.
        """
        # Reduce the available set of GroupUsage objects to those with the
        # selected term. Only search results in this resultset will be displayed
        available_groupusages = GroupUsage.objects.filter(
                group__term__term_info__name=self.selected_term())

        raw_facets = queryset.facet_counts()["fields"]

        # Ignore the term facet as we show it as tabs instead of as a facet
        side_facets = raw_facets.copy()
        del side_facets["term"]
        facets = self.process_result_facets(side_facets)

        if paginate:
            results = (queryset.load_all().load_all_queryset(
                    GroupUsage, available_groupusages)
                    # Slice the results to the desired range
                    [self._start:self._start + self._count])
        else:
            results = (queryset.load_all().load_all_queryset(
                    GroupUsage, available_groupusages))

        groupusages = [result.object for result in results]

        raw_term_facet = raw_facets["term"]
        term_facet = self.facet_handler("term")("term", raw_term_facet,
                set([("term", self.selected_term())]))

        return self.build_page(groupusages, facets, term_facet)


    def search(self):
        """
        Gets an EventsPage instance containing the search results.
        """
        queryset = self._get_filtered_queryset()
        return self.evaluate_queryset(queryset)

    def series_groups_ids(self):
        """
        Returns the set of all listed (group usage / event series) tuples
        after faceting / searching has been applied. This supports the
        "Add all lectures" functionality on the browse / search events pages:
        """
        queryset = self._get_filtered_queryset()
        event_page = self.evaluate_queryset(queryset, False)

        # Generate a list with format:
        # [(group_usage id, event series id)]
        series_groups_list = []

        for groupusage in event_page.groups():
                for series in groupusage.group.series():
                    series_groups_list.append((series.id, groupusage.id))

        return series_groups_list


class BrowseEventGroupSearcher(EventGroupSearcher):
    """
    Responsible for querying haystack for events to show on the student browse
    events page. search() returns a EventsPage instance containing the search
    results, facets etc.
    """

    def __init__(self, narrow_choices, organisation, **kwargs):
        super(BrowseEventGroupSearcher, self).__init__(narrow_choices, **kwargs)
        self._organisation = organisation

    def initial_queryset(self):
        queryset = EventGroupSearcher.initial_queryset(self)

        # Only show stuff from our organisation
        queryset = queryset.filter(organisation_id=self._organisation.id)
        return queryset

    def custom_facets(self):
        # FIXME: use self._organisation to implement (get type C 
        # ClassifierGroups). Custom facets are not indexed yet.
        return []

    def facets(self):
        facets = super(BrowseEventGroupSearcher, self).facets()

        # Add any custom facets the organisation has
        return facets + self.custom_facets()

    def build_page(self, groupusages, facets, term_facet):
        return EventsPage(groupusages, facets, term_facet, self._start,
                self._organisation.name)

def narrowing_choices(querydict):
    """
    Gets a list of the facet narrowing choices selected  by the user.
    
    Args:
        querydict: The request.GET querydict normally.
    
    Returns: A list of (facet-name, value) pairs.
    """
    def split_choice(choice):
        split_pos = choice.find("-")
        if split_pos == -1:
            return (choice, "")
        return (choice[0:split_pos], choice[split_pos + 1:])

    raw_choices = querydict.getlist("narrow")
    return [split_choice(choice) for choice in raw_choices]

class BrowseEventsView(StudentBaseView):
    """ Browse events (optionally for specified organisation)
    """
    def js_main_module(self):
        return "student/events"

    def __init__(self):
        super(BrowseEventsView, self).__init__()
        self.template = "student/2.0-Browse-Results.html"

    def context(self, request, organisation=None, **kwargs):
        context = super(BrowseEventsView, self).context(request)

        searcher = BrowseEventGroupSearcher(narrowing_choices(request.GET),
                self.org,
                start=int_safe(request.GET.get("start"), default=0))
        context["page"] = searcher.search()

        # get the student's personal timetable for the current user and selected year
        pt = pt_get_ids( request, context["page_year"] )
        context["bootstraps"].update({"personal_timetable": pt})

        return context


class SearchEventsView(StudentBaseView):
    """ Result of events search
    """
    def __init__(self):
        super(SearchEventsView, self).__init__()
        self.template = "student/3.0-Search-Results.html"

    def js_main_module(self):
        return "student/events"

    def context(self, request, **kwargs):
        context = super(SearchEventsView, self).context(request)

        searcher = SearchEventGroupSearcher(
                request.GET.get(HttpParameters.SEARCH_QUERY, ""),
                narrowing_choices(request.GET),
                start=int_safe(request.GET.get("start"), default=0))
        context["page"] = searcher.search()

        # get the student's personal timetable for the current user and selected year
        pt = pt_get_ids( request, context["page_year"] )
        context["bootstraps"].update({"personal_timetable": pt})

        context["user"] = User.as_context(request.user)
        
        return context

"""
def events_lectures_fragment(request):
    #Construct list of lectures to display in browse events view
    faculty = int_safe(request.GET.get("faculty"), default=0)
    year = int_safe(request.GET.get("year"), default=0)
    org_current = Organisation.objects.get(pk=faculty)
    return render(request, "student/fragments/lecture-list.html", { "org_current": org_current })
"""

class SearchEventsPage(EventsPage):
    """
    Represents the data shown on the Student search events page.
    
    Same as the EventsPage except we also have a search term (query string) 
    entered by the user.
    """
    def __init__(self, groupusages, facets, resulttab, offset, results_source,
            querystring):
        super(SearchEventsPage, self).__init__(groupusages, facets, resulttab,
                offset, results_source)

        if not isinstance(querystring, basestring):
            raise ValueError("Expected a string for querystring, got: %s" %
                    querystring)
        self._querystring = querystring

    def querystring(self):
        """
        Gets the search string the user entered.
        """
        return self._querystring

class SearchEventGroupSearcher(EventGroupSearcher):
    """
    Handles querying haystack for the search events page.
    """
    def __init__(self, querystring, narrow_choices, **kwargs):
        super(SearchEventGroupSearcher, self).__init__(narrow_choices, **kwargs)
        self._querystring = querystring

    def initial_queryset(self):
        queryset = super(SearchEventGroupSearcher, self).initial_queryset()

        # Search on the main document with the user's query. AutoQuery handles
        # building a safe & sensible query from raw user input
        return queryset.filter(content=AutoQuery(self._querystring))

    def facets(self):
        base_facets = super(SearchEventGroupSearcher, self).facets()

        # The search page facets on Organisation in addition to the others
        return base_facets + ["organisation_code"]

    def build_page(self, groupusages, facets, term_facet):
        return SearchEventsPage(groupusages, facets, term_facet, self._start,
                "\"%s\"" % self._querystring,
                self._querystring)

def timetable_custom_update(django_user, academic_year, series_groups, action='add'):
    ''' Iterates through a list of tuples comprising:
    (event series id, group usage id)
    and performs the specified action for them (add or remove)
    against the personal timetable. Actioned event series / group usages
    added to an actioned list. If we find when adding that the
    entry already exists or when deleting find the entry is already
    not there, then the process continues and the entry is added to a
    not actioned list. The actioned and not actioned lists are returned
    as a tuple and the UI can ultimately decide how to render the view of
    what actually happened.
    '''

    # Create a model User from the django user:
    user = User.objects.get(id=django_user.id)

    # Fetch a pre-existing personal timetable for the user for the given year or,
    # if one does not exist, fetch an existing one:
    personal_timetable, _ = PersonalTimetable.objects.get_or_create(
                                owner=user, year=academic_year)

    # We create a list of the event series / group usage tuples that we acted on:
    actioned = []
    # We create a list of the event series / group usage tuples that we did not
    # need to act on as they were already in the correct state, e.g. already
    # present or absent:
    already_actioned = []

    # Now we iterate through the event_series / group_usage tuples and add
    # all the entries to the personal timetable:
    for tuple in series_groups:
        series_id = tuple[0]
        group_usage_id = tuple[1]

        if (action == 'add'):
            try:
                # Create the new personal timetable usage entry:
                personal_timetable_usage = PersonalTimetableUsage.objects.create(
                    timetable=personal_timetable,
                    series=EventSeries.objects.get(id=series_id),
                    group=GroupUsage.objects.get(id=group_usage_id))
                personal_timetable.save()
                actioned.append((series_id, group_usage_id))
            except IntegrityError:
                log.debug("series %s for group usage %s already in the personal timetable" % (series_id, group_usage_id))
                already_actioned.append((series_id, group_usage_id))
        if (action == 'remove'):
            try:
		# Delete the personal timetable usage entry specified by the
		# timetable, event series and group usage:
                personal_timetable_usage = PersonalTimetableUsage.objects.get(
                    timetable=personal_timetable,
                    series=EventSeries.objects.get(id=series_id),
                    group=GroupUsage.objects.get(id=group_usage_id))
                personal_timetable_usage.delete()
                actioned.append((series_id, group_usage_id))
            except PersonalTimetableUsage.DoesNotExist:
                log.debug("series %s for group usage %s already deleted from personal timetable" % (series_id, group_usage_id))
                already_actioned.append((series_id, group_usage_id))

    # Return a tuple containing those event_series / group usage we acted on
    # and those that we did not because their state was already correct:
    return (actioned, already_actioned)


def timetable_custom_add(request, **kwargs):
    return timetable_custom_add_remove( request, 'add', **kwargs )

def timetable_custom_remove(request, **kwargs):
    return timetable_custom_add_remove( request, 'remove', **kwargs )

def timetable_custom_add_remove( request, action, **kwargs ):
    ''' Extracts a set of (group usage, event series ids)
    from a POST request and adds / removes them from
    the logged in user's timetable
    '''
    # Check that the user making the request is correctly logged in:
    if request.user is None:
        return HttpResponseBadRequest()
    if request.user.is_anonymous():
        return HttpResponseBadRequest()

    # Fetch the JSON for the series_groups to add from the POST request:
    try:
        series_groups_json = request.POST.get('series_groups')
    except:
        log.error("Problem fetching series_groups from POST");
        return HttpResponseBadRequest()

    # Convert the series_groups json representation to a list object:
    try:
        series_groups_list = json.loads(series_groups_json)
    except:
        log.error("Problem creating series and groups list from JSON");
        return HttpResponseBadRequest()

    if action == 'add':
        log.debug("Adding event series: %s for year %s" % (series_groups_list, kwargs['year_start']))
    elif action == 'remove':
        log.debug("Removing event series: %s for year %s" % (series_groups_list, kwargs['year_start']))

    # try to save the new data
    return timetable_save( request, kwargs["year_start"], series_groups_list, action )


class BrowseSeriesIds(StudentBaseView):
    ''' Given the current state of the browse events page, returns
    a set of tuples of all the actively selected (group usage,
    event series ids).
    '''
    def __init__(self):
        super(BrowseSeriesIds, self).__init__()

    def context(self, request, organisation=None, **kwargs):
        context = super(BrowseSeriesIds, self).context(request)

        searcher = BrowseEventGroupSearcher(narrowing_choices(request.GET),
                self.org)
        context["series_groups"] = searcher.series_groups_ids()

        log.debug("BrowseSeriesIds found %s" % context["series_groups"]);

        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(year_start, org_code)
        context = self.context(request)
        return HttpResponse(json.dumps(context["series_groups"]), content_type="application/json; charset=utf-8")

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        if request.user is None:
            return HttpResponseBadRequest()
        if request.user.is_anonymous():
            return HttpResponseBadRequest()

        self.locate_organisation(year_start, org_code)
        context = self.context(request)

        # attempt to save
        return timetable_save( request, year_start, context["series_groups"], "add" )


class SearchSeriesIds(StudentBaseView):
    ''' Given the current state of the search events page, either 
    returns a set of tuples of all the actively selected
    (group usage, event series ids) for a GET request, or adds all
    actively selected event series to the user's personal timetable
    for a POST request.
    '''

    def __init__(self):
        super(SearchSeriesIds, self).__init__()

    def context(self, request, organisation=None, **kwargs):
        context = super(SearchSeriesIds, self).context(request)

        searcher = SearchEventGroupSearcher(
                request.GET.get(HttpParameters.SEARCH_QUERY, ""),
                narrowing_choices(request.GET))
        context["series_groups"] = searcher.series_groups_ids()

        log.debug("SearchSeriesIds found %s" % context["series_groups"]);

        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(year_start, org_code)
        context = self.context(request)
        return HttpResponse(json.dumps(context["series_groups"]), content_type="application/json; charset=utf-8")

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        if request.user is None:
            return HttpResponseBadRequest()
        if request.user.is_anonymous():
            return HttpResponseBadRequest()
        
        self.locate_organisation(year_start, org_code)
        context = self.context(request)

        # attempt to save
        return timetable_save( request, year_start, context["series_groups"], "add" )


def timetable_save( request, year_start, series_groups_list, action ):
    """
    Utility function - avoids duplication of code at the end of:
        SearchSeriesIds.post
        BrowseSeriesIds.post
        timetable_custom_add_remove
    """
    
    # check whether we need to test for collisions - defaults to True
    do_collision_check = json.loads(request.POST.get('do_collision_check', True)) 
    
    # initalise useful stuff
    json_return = {}; # to return to caller
    academic_year = AcademicYear.objects.get( starting_year = year_start ) # use to get personal timetable and perform action
    
    # check for collisions
    collisions = {}
    if do_collision_check: # check for override flag - allows user to save even if collisions are present
        pt = pt_get_ids( request, academic_year )
        collisions = pt_collision_check( request, series_groups_list, pt )
        if len(collisions):
            json_return["collisions"] = collisions

    # try to save
    if len(collisions): # uh-oh - collision alert. Tell the user and do not try to save
        pass
    else: # no collisions so we can proceed to do the saving stuff 
        
        # actions is a tuple containing a list of event series / group usage tuples that were
        # acted on and a list of event series / group usage tuples that were already in the
        # correct state. We return a JSON representation of actions:
        json_return["actions"] = timetable_custom_update(request.user, academic_year, series_groups_list, action)
        

    # get the updated content of the user's personal timetable
    json_return["pt"] = pt_get_ids( request, academic_year )
        
    # return all response data in JSON format
    return HttpResponse(json.dumps(json_return), content_type="application/json; charset=utf-8")


class MoreBrowseEventsView(BrowseEventsView):

    def __init__(self):
        super(MoreBrowseEventsView, self).__init__()
        self.template = "student/fragments/more_events.html"

class MoreSearchEventsView(SearchEventsView):

    def __init__(self):
        super(MoreSearchEventsView, self).__init__()
        self.template = "student/fragments/more_events.html"


def subject_details(request, id_subject):
    """ Returns data for an event group.
    Shown in pop-ups on student browse and search pages.
    """
    data = {}
    try:
        data["classification"] = Classification.objects.get( classifier=id_subject ) # get details from corresponding Classification here
    except Classification.DoesNotExist:
        data["classification"] = None
    
    return render(request, "student/fragments/subject_details.html", data)