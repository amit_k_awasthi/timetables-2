from __future__ import absolute_import

from timetables.student import testdata
from timetables.model.models import Event, AcademicYear, Term, TermInfo
from timetables.views import BaseView
from timetables.utils.datetimes import server_datetime_now
from timetables.utils.ints import int_safe

from datetime import datetime, date
import calendar, itertools

from django.utils.datastructures import SortedDict
from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden, Http404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from timetables.student.views.feeds.icalendar import PersonalTimetableIcalendarFeedView

@login_required
def timetable_redirect(request, crsid=None):
    """
    Handles redirecting users who hit timetable URLs without specifying their
    CRSID and or timetable view (week/month/term/list).
    """
    if crsid is None:
        crsid = request.user.username
    
    # Redirect to the month view by default.
    return redirect("student timetable monthview", crsid=crsid)


class TimetableView(BaseView):

    def __init__(self):
        super(TimetableView, self).__init__(navtree=None)
    
    # Require users to be logged in to hit any of these timetable views.
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TimetableView, self).dispatch(*args, **kwargs)
    
    def context(self, request, **kwargs):
        context = super(TimetableView, self).context(request, **kwargs)
        
        context["timetable_type"] = self.timetable_type()
        context["cambridge_week"] = request.GET.get("weektype") == "cambridge"
        context["first_weekday"] = 3 if context["cambridge_week"] else 0
        context["personal_ical_feed"] = (PersonalTimetableIcalendarFeedView
                .anonymous_feed_url(request.get_host(), request.user))
        username = (request.user.username if request.user.is_authenticated() 
                else None)
        context["bootstraps"].update({"username": username})
        
        return context
    
    @classmethod
    def validate_view_permission(cls, request, timetable_owner_username):
        """
        Valdiates that the request's user has permission to see the personal
        timetable owned by timetable_owner_username.
        
        Args:
            request: The HttpRequest passed to the calling view
            timetable_owner_username: The username of the owner of the timetable
                to check.
        
        Returns: None if the user is permitted, otherwise an HttpResponse which
            should be returned from the view calling this function.
        """
        # For now we only allow the owner of the timetable to view their 
        # timetable.
        if timetable_owner_username != request.user.username:
            return HttpResponseForbidden("%s cannot view %s's timetable" % (
                    request.user.username, timetable_owner_username))


class TimetableTermView(TimetableView):

    def __init__(self):
        super(TimetableTermView, self).__init__()
        self.template = "student/5.1-Timetable-Termview.html"

    def timetable_type(self):
        return "term"

    def context(self, request, username, year):
        context = super(TimetableTermView, self).context(request)
        
        calendar = AcademicYearCalendar.from_personal_calendar(username, year,
                firstweekday=context["first_weekday"])
        context["calendar"] = calendar
        context["calendar_owner"] = username
        
        return context

    def get(self, request, crsid=None, year_start=None):
        assert crsid, "crsid is required"
        
        not_allowed = self.validate_view_permission(request, crsid)
        if not_allowed:
            return not_allowed
        
        # The year to show, defaulting to the current academic year is not
        # specified.
        year = int_safe(year_start)
        if year is None:
            year = AcademicYear.get_current(as_int=True)
        
        return render(request, "student/5.1-Timetable-Termview.html",
                self.context(request, crsid, year))

    def js_main_module(self):
        return "student/calendar/term"


class AcademicYearCalendar(object):
    """
    Models the data of the "term" personal calendar page. 
    """
    
    def __init__(self, term_calendars):
        """
        Args:
            term_calendars: A sequence of TermCalendar instances representing
                the terms to be shown in the calendar.
        """
        self._term_calendars = list(term_calendars)
        
        if not all(isinstance(tc, TermCalendar) for tc in self._term_calendars):
            raise ValueError("Expected term_calendars to contain TermCalendar "
                    "instances.")

    @staticmethod
    def from_personal_calendar(username, start_year, firstweekday=0):
        """
        Creates an AcademicYearCalendar containing the events of the specified
        user's personal timetable in the academic year starting at start_year.
        """
        
        # Sorting by term order is not implemented in the db, but we should
        # always get 3 results here, so it's of no significance.
        terms = sorted(Term.objects
                .filter(term_info__term_type=TermInfo.STANDARD_TERM)
                .filter(academic_year__starting_year=start_year)
                # Hopefully nobody assigns any lectures to easter day :-)
                .exclude(term_info__name=TermInfo.NAME_EASTER_DAY))
        
        # Respond with 404 if we don't know of any terms for the year.
        if not terms:
            raise Http404
        
        # Note that events_qs is not evaluated yet...
        events_qs = AcademicYearCalendar._load_events(username, start_year)
        
        # Create a TermCalendar instance for each term
        term_calendars = [TermCalendar.from_events_queryset(term, events_qs,
                firstweekday=firstweekday)
                for term in terms]
        
        return AcademicYearCalendar(term_calendars)
    
    @staticmethod
    def _load_events(username, start_year):
        return (Event.objects.all()
                .in_users_timetable(username, year=start_year)
                .prefetch_related("owning_series__associated_staff")
                .prefetch_related("default_timeslot")
                .order_by("default_timeslot__start_time"))

    def term_calendars(self):
        """
        Returns: A list of the TermCalendar instances under this academic year
            calendar.
        """
        return list(self._term_calendars)
    
    @property
    def events(self):
        """
        Yields: Each of the events from each TermCalendar.
        """
        for term_calendar in self._term_calendars:
            for event in term_calendar.events:
                yield event


class TermCalendar(object):
    """
    Models the data of an individual term in an AcademicYearCalendar.
    """
    
    def __init__(self, term, month_calendars, series):
        """
        Args:
            term: The Term instance that this calendar represents.
            month_calendars: A sequence of MonthListCalendar instances
                representing each of the months in this term.
            series: A sequence of EventSeries objects which occur in this term.
        """
        self._term = term
        self._month_calendars = month_calendars
        self._series = series
    
    @staticmethod
    def from_events_queryset(term, events_queryset, firstweekday=0):
        """
        As from_events(), except the events are specified as a queryset 
        (hopefully unevaluated).
        """
        # Limit the queryset to contain only events in the desired range
        events = TermCalendar._slice_for_term(term, events_queryset)
        
        return TermCalendar.from_events(term, events, firstweekday=firstweekday)
    
    @staticmethod
    def from_events(term, events, firstweekday=0):
        """
        Creates a TermCalendar instance for the specified term and sequence of
        events.
        """
        # It doesn't really matter that events contains events from a wider
        # range than just an individual month, as the MonthListCalendar will
        # ignore events outside its range.
        month_calendars = [MonthListCalendar(year, month, events,
                firstweekday=firstweekday)
                for (year, month) in term.contained_months()]
        
        # Collect a sequence of the series used by the events in this term.
        series = sorted(
                # Strip duplicate series
                set(event.owning_series for event in events),
                # Sort by series title
                key=lambda s: s.title)
        
        return TermCalendar(term, month_calendars, series)
    
    @staticmethod
    def _slice_for_term(term, events_queryset):
        return events_queryset.in_range(term.start, term.end)
    
    def month_calendars(self):
        return list(self._month_calendars)
    
    @property
    def events(self):
        """
        Yields: The events in this term.
        """
        for calendar in self._month_calendars:
            for event in calendar.events:
                yield event
    
    @property
    def series(self):
        return self._series
    
    @property
    def term(self):
        return self._term


class TimetableMonthView(TimetableView):

    def __init__(self):
        super(TimetableMonthView, self).__init__()
        self.template = "student/5.X-Timetable-Monthview.html"

    def timetable_type(self):
        return "month"

    def context(self, request, **kwargs):
        context = super(TimetableMonthView, self).context(request)
        return context

    def get(self, request, crsid=None):
        assert crsid, "crsid is required"
        
        not_allowed = self.validate_view_permission(request, crsid)
        if not_allowed:
            return not_allowed
        
        return render(request, "student/5.X-Timetable-Monthview.html", 
                self.context(request))

    def js_main_module(self):
        return  "student/calendar/month"


class TimetableWeekView(TimetableView):

    def __init__(self):
        super(TimetableWeekView, self).__init__()
        self.template = "student/5.1-Timetable-Weekview.html"

    def timetable_type(self):
        return "week"

    def context(self, request, **kwargs):
        context = super(TimetableWeekView, self).context(request)
        return context

    def get(self, request, crsid=None):
        assert crsid, "crsid is required"
        
        not_allowed = self.validate_view_permission(request, crsid)
        if not_allowed:
            return not_allowed
        
        return render(request, "student/5.1-Timetable-Weekview.html",
                self.context(request))

    def js_main_module(self):
        return "student/calendar/week"
    

class TimetableListView(TimetableView):

    def __init__(self):
        super(TimetableListView, self).__init__()
        self.template = "student/5.X-Timetable-Listview.html"

    def timetable_type(self):
        return "list"

    def context(self, request, username, year, month):
        context = super(TimetableListView, self).context(request)
        
        calendar = MonthListCalendar.from_personal_calendar(
                username, year, month, firstweekday=context["first_weekday"])
        context["calendar"] = calendar
        context["calendar_owner"] = username
        return context
    
    def default_date(self, year, month):
        if year and month > 0 and month < 13:
            return year, month
        
        now = server_datetime_now()
        return now.year, now.month
    
    def get(self, request, crsid=None, year=None, month=None):
        year, month = self.default_date(int_safe(year), int_safe(month))
        
        assert crsid, "crsid is required"
        assert not (bool(year) ^ bool(month)), ("Both year and month or "
                "neither can be specified, not just one")
        
        not_allowed = self.validate_view_permission(request, crsid)
        if not_allowed:
            return not_allowed
        
        context = self.context(request, crsid, year, month)
        return render(request, "student/5.X-Timetable-Listview.html", context)

    def js_main_module(self):
        return "student/calendar/list"


class MonthListCalendar(object):
    """
    Models the data shown in the calendar list page. i.e. a list of events under
    a month grouped by day, and a calendar representation.
    """
    def __init__(self, year, month, events, firstweekday=0):
        """
        Args:
            year: The numeric year of this calendar. 
            month: The numeric month of this calendar. range: [1,12]
            events: A sequence of Event objects. These should start inside the
                specified year & month. If not provided
        """
        self._month = month
        self._year = year
        self._cal = calendar.Calendar(firstweekday)
        self._events = events
        
        # It's required that _events be sorted by starting date/time
        self.events_by_day = self._bucket_into_days(self.events)
    
    @classmethod
    def from_personal_calendar(cls, username, year, month, **kwargs):
        """
        Instantiates a MonthListCalendar with the contents of the specified
        username's personal timetable at the month and year.
        """
        return cls(year, month, cls._load_events(username, year, month),
                 **kwargs)
    
    @staticmethod
    def _load_events(username, year, month):
        """
        Loads a month's worth of events from the personal timetable of username.
        """
        start, end = MonthListCalendar._month_range(year, month)
        return (Event.objects.all()
                .in_users_timetable(username)
                .in_range(start, end)
                .include_series_length()
                .prefetch_related("owning_series__associated_staff")
                .prefetch_related("default_timeslot")
                .order_by("default_timeslot__start_time"))
    
    @staticmethod
    def _month_range(year, month):
        """
        Returns: a 2 pair of datetime objects at the start and end of the
            specified month (inclusive).
        """
        _, length = calendar.monthrange(year, month)
        return (datetime(year, month, 1),
                datetime(year, month, length, 23, 59, 59, 1000000 - 1))
    
    @property
    def month(self):
        return self._month
    
    @property
    def year(self):
        return self._year
    
    @property
    def events(self):
        """
        Returns: A sequence of the known Event model instances which are in this
            month.
        """
        return (e for e in self._events if self._event_in_month(e))

    def _event_in_month(self, event):
        datetime = event.default_timeslot.start_time
        return datetime.month == self.month and datetime.year == self.year

    @staticmethod
    def event_day(event):
        "Returns: The numeric day of the month the Event instance starts on."
        return event.default_timeslot.start_time.day

    @staticmethod
    def _bucket_into_days(all_events):
        """
        Args:
            all_events: A sequence of Event objects.
        Returns: A (sorted) dictionary whose keys are days in a month (integers)
            and values are a list of events starting on the key's day.
        """
        by_day = itertools.groupby(all_events, MonthListCalendar.event_day)
        return SortedDict((day, list(events)) for (day, events) in by_day)
    
    def month_day(self, day):
        """
        Returns: A MonthDay instance for the specified day of this month.
        """
        return MonthListCalendar.MonthDay(self, day)
    
    def month_days(self):
        """
        Enumerates MonthDay instances for days of the month with at least one
        event.
        """
        for day in self.events_by_day.keys():
            yield self.month_day(day)
    
    def calendar_week_days(self):
        """
        Yields: The order of days in this calendars week. Values are integers
            where 0 is Monday and 6 is Sunday.
        """
        return self._cal.iterweekdays()
    
    def calendar_week_days_names(self):
        return (["Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
                "Saturday", "Sunday"][day] for day in self.calendar_week_days())
                
    
    def calendar_month_days(self):
        """
        Generates a sequence of the days in a grid month calendar where cells
        representing days in preceding or subsequent months are None and days in
        our month are MonthDay instances. 
        
        The output is effected by the firstweekday init param.
        
        This works in the same way as the calendar module's 
        Calendar.itermonthdays method.
        """
        for day in self._cal.itermonthdays(self.year, self.month):
            if day == 0:
                yield None
            else:
                yield self.month_day(day)
    
    def calendar_month_days_by_row(self):
        """
        As calendar_month_days() except instead of producing one long sequence,
        multiple 7 day sequences are yielded.
        """
        # Break the complete sequence of month_days() into 7 day blocks
        for (_, row) in itertools.groupby(enumerate(self.calendar_month_days()),
                lambda (i,_): i/7):
            yield [monthday for (_, monthday) in row]
            
    
    def start_date(self):
        return date(self.year, self.month, 1)
    
    def next_month(self):
        return (self.month % 12) + 1
    def prev_month(self):
        return ((self.month - 2) % 12) + 1
    def next_year(self):
        return self.year + 1 if self.month == 12 else self.year
    def prev_year(self):
        return self.year - 1 if self.month == 1 else self.year
    
    class MonthDay(object):
        """
        Represents the events associated with a specific day of a month.
        """
        def __init__(self, monthlistcal, day):
            self.monthlistcal = monthlistcal
            self.day = day
        
        def has_events(self):
            """
            Returns: True if this day has any events in it.
            """
            return len(self.events()) > 0
        
        def events(self):
            """
            Returns: A sequence of events that occur in this MonthDay's day.
            """
            return self.monthlistcal.events_by_day.get(self.day, [])
        
        def date(self):
            """
            Returns: A datetime.date instance at the day this MonthDay is at.
            """
            return date(self.monthlistcal.year, self.monthlistcal.month,
                    self.day)