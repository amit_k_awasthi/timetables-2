from django.conf.urls.defaults import patterns, url
from timetables.utils.views import complete_crsid

urlpatterns = patterns('',

    # End point used for autocompletion of CRSids:
    url(r'autocomplete/crsid/(.*)/$', complete_crsid,
            name="utils autocomplete crsid"),
)
