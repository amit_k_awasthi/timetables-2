'''
Created on Aug 21, 2012

@author: msn
'''
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from django.utils import simplejson as json
from timetables.model.models import User
from timetables.utils.lookup import Lookup

'''
Provides an end point for JQuery to perform autocompletion on CRSid
'''

@login_required
def complete_crsid(request, crsid_fragment):

    # Only open the autocomplete view up to admins at present:
    if ( not User.is_admin(request.user) ):
        return HttpResponseBadRequest()

    lookup_matches = None
    with Lookup() as lookup:
        lookup_matches = lookup.get_matches(crsid_fragment)

    # return a json representation of the lookup matches that
    # can be fed straight into a JS autocomplete function,
    # for example. Adding indent = 4 makes the JSON easier to
    # read for human testing:
    return HttpResponse(json.dumps(lookup_matches,indent=4), content_type="application/json; charset=utf-8")
