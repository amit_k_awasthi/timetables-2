import yaml
from functools import partial
from os.path import join, dirname
from django.conf import settings

# A dumb cache, there will be a better way of doing this.
YAML_CACHE = {}

CACHE_YAML = (settings.CACHE_YAML)


def load_yaml(basedir, relative_path):
    """
    Loads YAML data from relative_path, relative to basedir. 
    """
    path = join(basedir, relative_path)
    if CACHE_YAML and path in YAML_CACHE:
        return dict(YAML_CACHE[path])

    with open(path) as f:
        try:
            data = yaml.load(f)
            if CACHE_YAML:
                YAML_CACHE[path] = data
            return data
        finally:
            f.close()

def create_yaml_loader_relative_to_file(file):
    """
    Returns a version of load_yaml() with the directory of the file argument pre-set as
    the basedir.
    
    In effect, a function is returned which takes 1 argument: a path to a yaml file,
    relative to the file argument to this function. 
    """
    return partial(load_yaml, dirname(file))
