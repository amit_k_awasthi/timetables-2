from django.core.urlresolvers import resolve
from django.core.paginator import Paginator, Page, PageNotAnInteger, EmptyPage


class NavigationTree(object):
    """Represents a hierarchy of pages on a Django site as a tree structure.
    
    Subtrees which are not related to a page can be pruned to simplify the tree
    before exposing it in a UI.
    
    Implementation note: Pruning is only really designed for two levels of views
    (well, 2 + the root node, so 3).
    """
    def __init__(self, name, view_name, children=[], is_current=False, requires=None):
        self.name = name
        self.view_name = view_name
        self.children = children
        self.requires = requires
        self.is_current = is_current

    @staticmethod
    def build_navigations(request, tree):
        if tree is None:
            return (None, None)
        url_name = resolve(request.path).url_name
        marked = tree.with_current_page_marked(url_name)
        return (marked.pruned_for_breadcrumbs(), marked.pruned_for_navigation())

    @staticmethod
    def from_json(root):
        """Constructs a NavigationTree hierarchy from a dict/list structure."""
        return NavigationTree(root["name"], root["view_name"], requires=root["requires"] if 'requires' in root else None,
                children=[NavigationTree.from_json(child)
                        for child in root.get("children", [])])

    def __iter__(self):
        return iter(self.children)

    def traverse(self):
        "Generates the tree's nodes in depth first order."
        yield self
        for child in self.children:
            for node in child.traverse():
                yield node

    def contains_current_page(self):
        "returns True if this or any child page has a view_name of name."
        return (self.is_current or
                any(child.contains_current_page() for child in self.children))

    def with_unrelated_children_stripped(self):
        if self.contains_current_page():
            return self
        # Return a copy with children stripped
        return NavigationTree(self.name, self.view_name, requires=self.requires)

    def with_current_page_marked(self, url_name):
        clone = self.clone()
        for page in clone.traverse():
            if page.view_name == url_name:
                page.is_current = True
        return clone

    def pruned_for_breadcrumbs(self):
        """Prunes subtrees which aren't descendants of the current page."""
        if not self.contains_current_page():
            return None
        clone = self.shallow_clone()
        clone.children = [c for c in (child.pruned_for_breadcrumbs()
                for child in clone.children) if c]
        return clone


    def pruned_for_navigation(self):
        """Gets a version of the navtree with branches unrelated to the page 
        targeted by the request pruned."""
        clone = self.clone()
        clone.children = [child.with_unrelated_children_stripped()
                for child in clone.children]
        return clone

    def shallow_clone(self):
        return NavigationTree(self.name, self.view_name, children=self.children,
                is_current=self.is_current, requires=self.requires)

    def clone(self):
        return NavigationTree(self.name, self.view_name,
                [child.clone() for child in self.children],
                is_current=self.is_current, requires=self.requires)

    def __repr__(self, depth=0):
        children = "".join(child.__repr__(depth=depth + 1)
                for child in self.children)

        return "%sNavigationTree((%s), name: %s, view_name: %s, requires: %s)\n%s" % (
                "  " * depth, "*" if self.is_current else " ",
                self.name, self.view_name, self.requires, children )

    def apply_permissions(self, perms):
        '''
        Applies permissions removing children that don't have requirements.
        :param perms:
        '''
        saved = []
        for child in self.children:
            if child.requires is None or child.requires in perms:
                saved.append(child)
                child.apply_permissions(perms)
        self.children = saved


class TimetablesPaginator(Paginator):

    def page_safe(self, number):
        """As page() except bad numbers are handled by selecting an appropriate
        default page."""
        try:
            return self.page(number)
        except PageNotAnInteger:
            return self.page(1)
        except EmptyPage:
            return self.page(self.num_pages)

    def page(self, number):
        # Override page() to return an instance of our own page class
        page = super(TimetablesPaginator, self).page(number)
        return TimetablesPage(page.object_list, page.number, self)

class TimetablesPage(Page):

    def nearby_pages(self, count=10):
        if count < 0:
            raise ValueError("count cannot be negative: %d" % count)

        if count > self.paginator.num_pages:
            start = 0
            end = self.paginator.num_pages
        elif self.number - (count / 2) < 1:
            start = 0
            end = count
        elif self.number + (count - (count / 2)) > self.paginator.num_pages:
            end = self.paginator.num_pages
            start = end - count
        else:
            start = self.number - (count / 2)
            end = self.number + (count - (count / 2))
        return self.paginator.page_range[start:end]
