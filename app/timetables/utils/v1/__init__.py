from timetables.utils.v1 import pparser
from timetables.utils.v1.year import Year
import traceback

import logging, calendar, datetime
from timetables.utils.v1.grouptemplate import GroupTemplate
from django.db import models
log = logging.getLogger(__name__)
del(logging)


def generate(event_series, group_template, term_name, org, title = None, location = None, staff=[]):
    # Lazy imports to avoid cyclic issues.
    from timetables.model.models import TimeInterval, Event, TermInfo, CollisionEvents, Term
    
    terms = [ term.start for term in Term.objects.filter(term_info__day=False,
                                                         academic_year__organisation=org,
                                                         term_info__term_type='S',
                                                         ).order_by("start")]
    year = Year(terms)
    groupTemplate = GroupTemplate(group_template)
    events = []
    for p in event_series.date_time_pattern.split(";"):
        pattern = "%s %s" % ( term_name, p.strip() )
        p = pparser.fullparse(pattern, groupTemplate)
        n = 0
        dtField = models.DateTimeField()
        for start, end in year.atoms_to_isos(p.patterns()):
            try:
                time_interval = TimeInterval(start_time=dtField.to_python(start), end_time=dtField.to_python(end))
                event = Event(
                        owning_series=event_series,
                        position=n,
                        default_timeslot=time_interval,
                        is_staff_overridden=True,
                        last_modified=event_series.last_modified,
                        combination_set_id=-1)
                event.staff = staff
                event.location = location
                event.title = title
                events.append(event)
            except: 
                log.critical(traceback.format_exc())
                return events
    return events




def publish(event_series):
    '''
    Publishes the event series into events, deleting all previously published events associated with the event series
    :param event_series:
    :returns: the number of new intervals and the number of new events created.
    '''
    # Lazy imports to avoid cyclic issues.
    from timetables.model.models import TimeInterval, TermInfo, Event, CollisionEvents
    new_intervals = 0
    new_events = 0
    deleted_events = event_series.event_set.all().delete()
    # remove all CollisionEvents data relating to this series
    CollisionEvents.objects.filter( series = event_series ).delete()
        
    try:
        terms = [ term.start for term in event_series.owning_group.
                            owning_organisation.year.
                            terms.filter(term_info__day=False,term_info__term_type='S').order_by("start")]
        year = Year(terms)
        termPrefix = TermInfo.TERM_SHORT_NAMES[event_series.owning_group.term.term_info.name] or None
        groupTemplate = GroupTemplate(event_series.owning_group.default_pattern)
        for p in event_series.date_time_pattern.split(";"):
            if termPrefix is not None:
                pattern = "%s %s" % ( termPrefix, p.strip() )
            else:
                pattern = p
            log.info("Term is %s pattern is %s" % ( termPrefix, pattern))
            p = pparser.fullparse(pattern, groupTemplate)
            n = 0
            for start, end in year.atoms_to_isos(p.patterns()):
                try:
                    time_interval, created = TimeInterval.objects.get_or_create(start_time=start, end_time=end)
                    if created:
                        new_intervals = new_intervals + 1
                
                    event = Event.objects.create(
                            owning_series=event_series,
                            position=n,
                            default_timeslot=time_interval,
                            overridden_title=None,
                            is_staff_overridden=False,
                            overridden_location=None,
                            last_modified=event_series.last_modified,
                            combination_set_id=-1)
                    log.debug("Start: %s, End: %s " % (start, end))
                    n = n + 1
                    new_events = new_events + 1
                    
                    # add CollisionEvents data for this event
                    start_s = datetime.datetime.strptime( start, "%Y-%m-%dT%H:%M:%SZ" ) # GM - not sure about timezone handling here - check with IEB / HB
                    start_s = start_s.utctimetuple()
                    start_s = calendar.timegm(start_s) # come back PHP, all is forgiven - absurdly convoluted conversion from string to seconds
                    
                    end_s = datetime.datetime.strptime( end, "%Y-%m-%dT%H:%M:%SZ" )
                    end_s = end_s.utctimetuple()
                    end_s = calendar.timegm(end_s) - 1 # reduce finish time by 1 second - avoids, e.g. 10am finish occupying next hour's first timeslot and resulting in false positives in clash detection
                    
                    event_timeslots = CollisionEvents.get_timeslots(start_s, end_s)
                    
                    for ts in event_timeslots:
                        ce = CollisionEvents( timeslot=ts, event=event )
                        ce.save()
                        
                except: 
                    log.critical(traceback.format_exc())
                    return
    except:
        log.error(traceback.format_exc())
        log.error("Failed to process event %s with pattern %s " % (event_series.title,event_series.date_time_pattern ))
    return new_intervals, new_events, deleted_events

