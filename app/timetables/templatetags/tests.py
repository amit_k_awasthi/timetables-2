"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.template import Template, Context
from django.conf.urls.defaults import patterns, url
from django.core.urlresolvers import reverse

urlpatterns = patterns('',
   url('^foo/(?P<id>\d+)/year/(?P<year>\d{4})/$', "someview", name="foobar pattern"),
   url('^simple/url/$', "someotherview", name="simple url"),
)

class TemplateTagsTest(TestCase):
    urls = "timetables.templatetags.tests"

    def test_url_dynamic(self):
        """
        Tests that the url_dynamic tag resolves the same thing that the reverse() function
        does.
        """
        url_template = Template("{% load timetables_tags %}{% url_dynamic params %}")

        view_description = {
            "view": "foobar pattern",
            "params": {"id": 3, "year": 2012}
        }
        context = Context({"params": view_description})

        self.assertEqual(reverse("foobar pattern", kwargs={"id": 3, "year":2012}),
                         url_template.render(context))

    def test_url_dynamic_backwards_compat_1(self):
        url_template = Template("{% load timetables_tags %}{% url_dynamic name %}")
        context = Context({"name": "simple url"})

        self.assertEqual(reverse("simple url"),
                         url_template.render(context))

    def test_url_dynamic_backwards_compat_2(self):
        url_template = Template("{% load timetables_tags %}{% url_dynamic 'simple url' %}")
        context = Context({"name": "simple url"})

        self.assertEqual(reverse("simple url"),
                         url_template.render(context))

    def test_humanise_filter(self):
        template = Template("{% load timetables_tags %}{{value|humanise}}")

        expectations = [
            ("", ""),
            ("foo", "Foo"),
            ("foo_bar", "Foo Bar"),
            ("foO_bar", "FoO Bar"),
            ("foO_bAr", "FoO BAr"),
            ("a_b_c_d", "A B C D"),
            ("a b c d", "A B C D"),
            ("the_title_of_something", "The Title Of Something"),
        ]

        for input, output in expectations:
            context = Context({"value": input})
            self.assertEqual(output, template.render(context))
