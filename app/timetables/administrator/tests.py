"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from sys import stderr
from timetables.administrator import views

class SamplePagesTest(TestCase):

    PAGES = [
        reverse(views.view_overview),
        reverse(views.view_all_events),
        reverse(views.add_events),
        reverse(views.edit_events),
        reverse(views.import_events),
        reverse(views.view_classifications),
        reverse(views.view_classifications_add),
        reverse(views.view_classifications_edit),
        reverse(views.view_change_history),
        reverse(views.view_change_history_revision, args=[0]),
        reverse(views.view_change_history_revision_details, args=[0]),
        reverse(views.view_administrators),
        reverse(views.view_administrators_add),
    ]

    def ensure_request_for_page_responds_with_200_status(self, page):
        c = Client()
        response = c.get(page)
        self.assertEqual(200, response.status_code,
                "Response status code expected to be 200 but was: %d, page: %s"
                % (response.status_code, page))

    def test_page_requests_respond_with_200_status(self):
        for page in self.PAGES:
            try:
                self.ensure_request_for_page_responds_with_200_status(page)
            except BaseException as e:
                # Why doesn't python support raising an exception with a cause
                # exception?
                print >> stderr, "Error fetching page: %s" % page
                raise
