from timetables.utils.staticdata import create_yaml_loader_relative_to_file

load_yaml = create_yaml_loader_relative_to_file(__file__)

def page_overview_data():
    return load_yaml("uidata/overview.yaml")

def all_events_data():
    return load_yaml("uidata/all-events.yaml")

def add_events_data():
    return load_yaml("uidata/add-events.yaml")

def edit_events_data():
    return load_yaml("uidata/edit-events.yaml")

def import_events_data():
    return load_yaml("uidata/import-events.yaml")

def classifications_data():
    return load_yaml("uidata/classifications.yaml")

def classifications_add_data():
    return load_yaml("uidata/classifications-add.yaml")

def classifications_edit_data():
    return load_yaml("uidata/classifications-edit.yaml")

def change_history():
    return load_yaml("uidata/change-history.yaml")

def change_history_revision():
    return load_yaml("uidata/change-history-revision.yaml")

def change_history_revision_details():
    return load_yaml("uidata/change-history-revision-details.yaml")

def administrators():
    return load_yaml("uidata/administrators.yaml")

def administrators_add():
    return load_yaml("uidata/administrators-add.yaml")