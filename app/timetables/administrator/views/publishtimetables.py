'''
Created on Aug 1, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseBadRequest, HttpResponseRedirect,\
    HttpResponse, HttpResponseForbidden
from timetables.model.models import EventGroup, GroupUsage, Classifier,\
    Permission
from django.utils import simplejson as json
from timetables.utils.Json import JSON_CONTENT_TYPE, JSON_INDENT
import logging
import urlparse
import urllib
from django.forms.fields import DateField
from timetables.model.backend import PermissionSubject


log = logging.getLogger(__name__)

class PublishTimetables(AdminBaseView):
    '''
    Perform the publish timetables operation.
    '''
    

    def js_main_module(self):
        return None

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        # Key Faculty Admins may publish everything for an Organisation in a specific year with an embargo date
        # Non Key faculty Admins may only publish Timetables they are responsible for.
        # When Complete, this should return to the referrer page.
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.PUBLISH, PermissionSubject(self.org)):
            return HttpResponseForbidden()

        embargo_date = None
        if "embargo-date" in request.POST and len(request.POST['embargo-date']) > 0:
            embago_date_req = request.POST['embargo-date']
            try:
                embargo_date = DateField().to_python(embago_date_req)
            except:
                return HttpResponseBadRequest("Can't parse date %s  " % (embago_date_req ) )
            if embargo_date is None:
                return HttpResponseBadRequest("Can't parse date %s  " % (embago_date_req ) )

        if "all" in request.POST and request.POST['all'] == "1":
            # FIXME: Check this is a key admin (HOW?)
            # Publish all Event Series for the group in the year
            if embargo_date is not None:
                EventGroup.objects.filter(owning_organisation=self.org,publication_status='U').update(embargo_date=embargo_date)
                published = EventGroup.objects.filter(owning_organisation=self.org,publication_status='U',embargo_date=embargo_date).count()
            else:
                EventGroup.objects.filter(owning_organisation=self.org).update(publication_status='P',embargo_date=None)
                published = EventGroup.objects.filter(owning_organisation=self.org).count()
        else:
            # FIXME: Include the permissions in the selection of Groups or Classifiers (HOW ?)
            levels = request.POST.getlist("level")
            subjects = request.POST.getlist("subject")
            # get a set of usage pointers for all the subjects specified that relate to this organisation.
            subjectClassifierSet = Classifier.objects.filter(group__organisation=self.org,
                                                        group__family='S',
                                                        id__in=subjects)
            # get a set of usage pointers for all the levels specified relate to this organisation.
            levelClassifierSet = Classifier.objects.filter(group__organisation=self.org,
                                                      group__family='L',
                                                      id__in=levels)
            # get the group usage ids
            groupUsageSet = GroupUsage.objects.filter(classifiers__in=subjectClassifierSet).filter(classifiers__in=levelClassifierSet)
            # Publish all event groups that have classiiers in both usage sets
            
            # This comes out as a single SQL statement.
            if embargo_date is not None:
                EventGroup.objects.filter(owning_organisation=self.org,
                                                   groupusage__in=groupUsageSet,
                                                   publication_status='U').update(embargo_date=embargo_date)
                published = EventGroup.objects.filter(owning_organisation=self.org,
                                                   groupusage__in=groupUsageSet,
                                                   publication_status='U',
                                                   embargo_date=embargo_date).count()
            else:
                EventGroup.objects.filter(owning_organisation=self.org,
                                                   groupusage__in=groupUsageSet).update(publication_status='P')
                published = EventGroup.objects.filter(owning_organisation=self.org,
                                                   groupusage__in=groupUsageSet).count()
        if request.is_ajax():
            # Send a json response
            return HttpResponse(json.dumps({ 'response' : 'Ok',
                                            'message' : '%s Event Groups were published' % published
                                            }, indent=JSON_INDENT),content_type=JSON_CONTENT_TYPE )
        else:
            # go back to where we came from
            target = request.META["HTTP_REFERER"]
            urlparts = urlparse.urlparse(target)
            qdict = urlparse.parse_qs(urlparts[4])
            if embargo_date is not None:
                qdict['eevp'] = published
                qdict['ed'] = embargo_date
                if 'evp' in qdict:
                    del(qdict['evp'])
            else:
                qdict['evp'] = published
                if 'eevp' in qdict:
                    del(qdict['eevp'])
                if 'ed' in qdict:
                    del(qdict['ed'])
            urlparts = urlparse.ParseResult(scheme=urlparts.scheme,
                                            netloc=urlparts.netloc,
                                            path=urlparts.path,
                                            params=urlparts.params,
                                            query=urllib.urlencode(qdict, doseq=True),
                                            fragment=urlparts.fragment)
            target = urlparse.urlunparse(urlparts)
            return HttpResponseRedirect(target)
