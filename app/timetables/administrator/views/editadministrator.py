'''
Created on Aug 20, 2012

@author: ieb
'''
from django.shortcuts import render
from timetables.administrator.views import AdminBaseView
from timetables.model.models import Classifier, Permission, User
from timetables.model.backend import PermissionSubject
from django.http import HttpResponseForbidden, HttpResponseNotFound
import logging

log = logging.getLogger(__name__)

class EditAdministrators(AdminBaseView):
    '''
    Renders the EditAdministration page in an empty state.
    '''
    def js_main_module(self):
        return "admin/administrators-edit"


    def context(self, request, user, based_on=None):
        context = super(EditAdministrators, self).context(request, based_on=based_on)

        context["page_title"] = "Edit Administrator"

        context["organisation"] = {
                    "classifications" : {
                        "subjects" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org)).order_by("value"),
                        "levels" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                             group__organisation=self.org)).order_by("value"),

                            }
                    }
        context["administrator"] = user
        context["admininfo"] = {}
        perms = user.get_all_permissions(PermissionSubject(self.org))
        log.error("Permissions set %s " % perms)
        if Permission.KEY_ADMIN in perms:
            context['admininfo']['type'] = "key"
        else:
            context['admininfo']['type'] = "faculty"
        
        if Permission.KEY_ADMIN in perms:
            context['admininfo']['perms'] ="rwpc"
            context['admininfo']['admins'] = "admins"
        elif Permission.WRITE_ADMINISTRATORS in perms:
            context['admininfo']['perms'] ="rwpc"
            context['admininfo']['admins'] = "admins"
        elif Permission.WRITE_CLASSIFICATIONS in perms:
            context['admininfo']['perms'] ="rwpc"
        elif Permission.PUBLISH in perms:
            context['admininfo']['perms'] ="rwp"
        elif Permission.WRITE in perms:
            context['admininfo']['perms'] ="rw"
        elif Permission.READ in perms:
            context['admininfo']['perms'] ="r"
            
        
        log.error("Context : %s %s " % ( context['admininfo']['perms'], context['admininfo']['type']))
        

        return context

    def get(self, request, year_start, org_code, username):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE_ADMINISTRATORS, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        
        user = None
        try:
            user = User.objects.get(username=username,organisationrole__organisation=self.org)
        except:
            return HttpResponseNotFound()
                
        context = self.context(request, user)
        return render(request, "administrator/4.2-Administrators_Edit.html",
                      context)
