'''
Created on Aug 1, 2012

@author: ieb
'''
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseBadRequest, HttpResponse,\
    HttpResponseRedirect, HttpResponseNotAllowed, HttpResponseForbidden
from timetables.model.models import Event, User, Permission
from timetables.utils.Json import JSON_INDENT, JSON_CONTENT_TYPE
from django.utils import simplejson as json
from timetables.administrator.views.allevents import AllEvents
from timetables.model.backend import PermissionSubject

class BulkEditLecture(AllEvents):
    
    def js_main_module(self):
        return None

    def get(self, request, year_start, org_code):
        return HttpResponseNotAllowed("POST")

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        '''
        POST contains selected-events a comma separated list of EventIds
        lecturers containing comma separated names of lecturers
        This should clear the list of lecturers on all the events listed and 
        add a new set.
        
        :param request:
        :param year_start:
        :param org_code:
        '''
        
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        
        # get a list of User objects, creating any missing staff as we go. The Lecuturers are crsids
        # THis code will need to be changed if the lecturers are added by anything other than crsids
        lecturers = set()
        for crsid in request.POST['lecturers'].split(","):
            l, _ = User.objects.get_or_create(username=crsid)
            lecturers.add(l)
        
        # add that list to each event as overridden_staff, make sure the list of events is also filtered by year since 
        # the user performing this should only have been given access to events owned by orgs they are admins for
        updated = 0
        if "all-events" in request.POST and request.POST['all-events'] == "1":
            events = self.list_events(request,with_sort=False)
        else:
            events = Event.objects.filter(id__in=request.POST['selected-events'].split(","),
                                      owning_series__owning_group__owning_organisation=self.org)

        for e in events:
            e.overridden_staff.clear()
            for l in lecturers:
                e.overridden_staff.add(l)
            e.is_staff_overridden = True
            e.save()
            updated = updated + 1
            
        return self.prepare_response(request, updated=updated)
