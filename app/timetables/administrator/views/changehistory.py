'''
Created on Aug 16, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from timetables.model.models import Revision
from django.shortcuts import render


class ChangeHistory(AdminBaseView):
    '''
    List the change history for a faculty.
    '''

    def js_main_module(self):
        return "admin/changehistory"
    
    
    def context(self, request, based_on=None):
        '''
        Configure the context for change history.
        :param request:
        :param based_on:
        '''
        context = super(ChangeHistory, self).context(request, based_on=based_on)

        context["page_title"] = "Change History"


        context['alloftime'] = Revision.objects.filter(organisation=self.org).order_by("-id").select_related("user","organisation")


        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        context = self.context(request)

        return render(request, "administrator/3.0-Change-History.html", context)
