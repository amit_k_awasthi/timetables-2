'''
Created on Oct 11, 2012

@author: ieb
'''
from django.core.urlresolvers import reverse
from django.views.generic.base import View
from django.http import HttpResponseRedirect

class GoToTimetable(View):
    
    def get(self, request, year_start, org_code):
        if "y" in request.GET:
            year_start = request.GET['y']
        if 'c' in request.GET:
            org_code = request.GET['c']
        return HttpResponseRedirect(reverse("admin events", args=(year_start, org_code) ))
