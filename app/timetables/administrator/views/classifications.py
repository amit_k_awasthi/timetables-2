'''
Created on Aug 3, 2012

@author: ieb
'''
from timetables.utils.http import HttpParameters, ContextKeys
from timetables.model.models import Classification
from django.shortcuts import render
from timetables.administrator.views import AdminBaseView
from timetables.utils import TimetablesPaginator
from copy import deepcopy

class Classifications(AdminBaseView):
    '''
    Show all classifications.
    '''
    PAGE_STATE_KEYS = (
                    ( HttpParameters.SEARCH_QUERY, ContextKeys.SEARCH_QUERY),
                    )
    ORDER_FIELDS = {
        'topic' : 'name',
        }

    # The number of Classifications to show per page.
    results_per_page = 20

    def js_main_module(self):
        return "admin/classifications"

    def list_classifications(self, request, with_sort=True):
        '''
        returns a 2 query sets for a list of classifications, filtered and sorted as requested.
        The second query set has no limits and should only be used for aggregation.
        :param request:
        :param with_sort:
        '''
        querySet = Classification.objects.filter(owner=self.org,classifier__group__family='S')
        if "q" in request.GET:
            querySet = querySet.filter(name__icontains=request.GET['q'].lower())
        querySet = querySet.prefetch_related("classifier","classifier__group") 
        full_query_set = deepcopy(querySet)
        # Apply sorting 
        if with_sort:
            if "s" in request.GET and request.GET['s'] in Classifications.ORDER_FIELDS:
                ordering = Classifications.ORDER_FIELDS[request.GET["s"]]
                if "o" in request.GET and request.GET['o'] == 'd':
                    ordering = "-%s" % ordering
                querySet = querySet.order_by(ordering)
            querySet.order_by("id")
        return querySet, full_query_set

    def context(self, request, based_on=None):
        '''
        Build the context for a list of classifications.
        :param request:
        :param based_on:
        '''
        context = super(Classifications, self).context(request, based_on=based_on)
        querySet, _ = self.list_classifications(request)
        pages = TimetablesPaginator(querySet,
                Classifications.results_per_page)
        page = pages.page_safe(request.GET.get(HttpParameters.PAGE, 1))

        context["page"] = page
        context["classification_matches"] = page.object_list
        
        # pass sort quey and order to the template
        if HttpParameters.SEARCH_QUERY in request.GET:
            context[ContextKeys.SEARCH_QUERY] = request.GET[HttpParameters.SEARCH_QUERY]
        if HttpParameters.SORT in request.GET:
            context[ContextKeys.SORT] = request.GET[HttpParameters.SORT]
        if HttpParameters.SORT_ORDER in request.GET:
            context[ContextKeys.SORT_ORDER] = request.GET[HttpParameters.SORT_ORDER]

        context["url_params"] = {
                "view": "admin classifications",
                "params": [
                    self.year.url_id(),
                    self.org.url_id()
                ]
            }

        context["page_title"] = "Classifications"
        # All query parameters that influence paging, these will be saved by
        context["page_state_keys"] = Classifications.PAGE_STATE_KEYS
        
        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        context = self.context(request)

        return render(request, "administrator/2.0-All-Classifications.html",
                context)
