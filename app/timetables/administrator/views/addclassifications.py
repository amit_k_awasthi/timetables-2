'''
Created on Aug 16, 2012

@author: msn
'''
from timetables.administrator.views import AdminBaseView
from django.shortcuts import render
from timetables.model.models import ClassifierGroup, Permission

import logging
from timetables.model.backend import PermissionSubject
from django.http import HttpResponseForbidden
log = logging.getLogger(__name__)
del(logging)


class AddClassifications(AdminBaseView):
    '''
    Renders the AddClassifications page in an empty state
    '''

    def js_main_module(self):
        return "admin/classifications-add"

    def context(self, request, based_on=None):
        context = super(AddClassifications, self).context(request, based_on=based_on)

        context["page_title"] = "Add Classifications"

        context["organisation"] = {
                    "classifications" : {
                        "levels" : list((ClassifierGroup.objects.for_organisation(self.org)[0]).classifier_set.order_by('value')),
                        "subjects" : list((ClassifierGroup.objects.for_organisation(self.org)[1]).classifier_set.order_by('value')),
                            }
                    }
        context["subject_name"] = ClassifierGroup.objects.get(organisation_id=self.org,family='S').name

        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE_CLASSIFICATIONS, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        context = self.context(request)
        return render(request, "administrator/2.1-Add-New-Classification.html",
                      context)
