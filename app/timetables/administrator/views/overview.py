'''
Created on Aug 20, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.shortcuts import render
from timetables.model.models import Revision, AcademicYear

class Overview(AdminBaseView):
    '''
    Show the overview page.
    '''

    def js_main_module(self):
        return "admin/overview"
    
    def context(self, request, based_on=None):
        context = super(Overview, self).context(request, based_on=based_on)

        context['recent_changes'] = Revision.objects.filter(organisation=self.org).order_by("-id").select_related("user","organisation")[:5]
        
        if 'evp' in request.GET:
            context['published_timetables'] = request.GET['evp']
        if 'eevp' in request.GET:
            context['embargo_published_timetables'] = request.GET['eevp']
            context['embargo_date'] = request.GET['ed']

        context['available_years'] = AcademicYear.objects.filter(starting_year__gt=self.year.starting_year).exclude(organisation=self.org).order_by("starting_year")

        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        pagedata = self.context(request)

        return render(request, "administrator/0.0-Administrator-Overview.html",
                                  pagedata)

