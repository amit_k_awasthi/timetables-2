'''
Created on Aug 12, 2012

@author: msn
'''
from timetables.administrator.views import AdminBaseView
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseBadRequest, HttpResponseForbidden
from timetables.model.models import Classification, Classifier, Permission,\
    ClassifierGroup

import logging
from django.core.urlresolvers import reverse
from timetables.model.backend import PermissionSubject
log = logging.getLogger(__name__)
del logging

class SaveClassification(AdminBaseView):
    '''
    Save classifications.
    '''

    def js_main_module(self):
        return None

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        '''
         Post contents:
         "classification-contact": "optional",
         "classification-description": "optional",
         "classification-name": "required, must be unique",
         "classification-id": "only present on edit",
         "classification-name-original": "only present on edit",
         "classification-website": "optional",
         "csrfmiddlewaretoken": "django cross site scripting protection",
         "org_level": "organizational level",
        
        :param request:
        :param year_start:
        :param org_code:
        '''

        updated = 1
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE_CLASSIFICATIONS, PermissionSubject(self.org)):
            return HttpResponseForbidden()

        # Parse submitted POST parameters:
        try:
            name = request.POST["classification-name"]
            website = request.POST["classification-website"] or ''
            description = request.POST["classification-description"] or ''
            contact_person = request.POST["classification-contact"] or ''
            level = Classifier.objects.get(id=request.POST["org_level"],group__family='L',group__organisation=self.org)
            
        except Classifier.DoesNotExist:
            return HttpResponseBadRequest("Level %s does not exist " % request.POST["org_level"])
        except KeyError:
            return HttpResponseBadRequest()
        
        subject_group = ClassifierGroup.objects.get(family='S', organisation=self.org)

        # If we have a classification-name-orignal field then we are
        # editing, otherwise we are adding:
        if "classification-id" in request.POST:
            if "delete-classification" in request.POST and request.POST['delete-classification'] == "1":
                classification = Classification.objects.get(id=request.POST["classification-id"])
                Classifier.objects.filter(classification=classification).delete()
                classification.delete()
            else:

                # Update the classification object with the name originally set
                # when entering the edit page:
                classification = Classification.objects.get(id=request.POST["classification-id"])
                if classification.classifier is not None:
                    classification.classifier.value = name
                    classification.classifier.parent = level
                    classification.classifier.save()
                else:
                    classification.classifier = Classifier.objects.create(
                                                parent=level,
                                                value=name,
                                                group=subject_group)

                # update the various classification fields:
                classification.name = name
                classification.website = website
                classification.description = description
                classification.contact_person = contact_person
                classification.owner=self.org
                classification.save()
        else:
            # Create the new classification object:
            classifier = Classifier.objects.create(
                                            parent=level,
                                            value=name,
                                            group=subject_group)
            classification = Classification.objects.create(
                    name = name,
                    website = website,
                    description = description,
                    contact_person = contact_person,
                    owner=self.org,
                    classifier=classifier)


        # Return to the classifications page to view the new classification:
        target_url = reverse("admin classifications", args=(self.year.url_id(), str(self.org.url_id())))

        return self.prepare_response(request, target_url=target_url, updated=updated)

