'''
Created on Aug 1, 2012

@author: ieb
'''
from django.shortcuts import render
from timetables.model.models import User, Permission

import logging
from timetables.administrator.views import AdminBaseView
from timetables.utils.http import ContextKeys, HttpParameters
from timetables.utils import TimetablesPaginator
from django.db import models
log = logging.getLogger(__name__)
del logging


class Administrators(AdminBaseView):
    '''
    View to list administrators.
    '''

    PAGE_STATE_KEYS = (
                    ( "q", "search_term"),
                    )


    results_per_page = 20

    ORDER_FIELDS = {
        'name' : 'username',
        'email' : 'email'
        }
    ORDER_SERIES_FIELDS = {
        'name' : 'username',
        'email' : 'email'
        }

    def js_main_module(self):
        return "admin/administrators"




    def list_administrators(self, request, org, year, perms=None, with_sort=True):
        '''
        Create a list of administrators (query)
        :param request: the request
        :param org: the organisation
        :param year: the year
        :param perms: permissions
        :param with_sort: sorted or not
        '''
        querySet =  User.objects.admins(year=year, organisation=org, permissions=perms)
        if "q" in request.GET:
            q = request.GET['q'].lower()
            querySet = querySet.filter(models.Q(last_name__icontains=q)|
                                       models.Q(first_name__icontains=q)|
                                       models.Q(email__icontains=q)|
                                       models.Q(username__icontains=q))
        
        # Apply sorting 
        if with_sort:
            if "s" in request.GET and request.GET['s'] in Administrators.ORDER_FIELDS:
                ordering = Administrators.ORDER_FIELDS[request.GET["s"]]
                if "o" in request.GET and request.GET['o'] == 'd':
                    ordering = "-%s" % ordering
                querySet = querySet.order_by(ordering)
            querySet.order_by("id")
        return User.objects.filter(id__in=querySet)


    def context(self, request, based_on=None):
        '''
        Build context for the list of administrators.
        :param request:
        :param based_on:
        '''
        context = super(Administrators, self).context(request, based_on=based_on)

        # get list of administrators who are key faculty administrators - used to set ticks in UI
        key_admins = set(self.list_administrators(request, org=self.org.id,
                year=self.year.id,
                perms=[Permission.objects.admin_key_admin()]))
        # get the admin users on the current page
        pages = TimetablesPaginator(self.list_administrators(request, org=self.org.id, year=self.year.id),
                Administrators.results_per_page)
        page = pages.page_safe(request.GET.get(HttpParameters.PAGE, 1))

        context["page"] = page
        context["administrators"] = page.object_list
        context["key_administrators"] = key_admins


        # pass sort quey and order to the template
        if HttpParameters.SEARCH_QUERY in request.GET:
            context[ContextKeys.SEARCH_QUERY] = request.GET[HttpParameters.SEARCH_QUERY]
        if HttpParameters.SORT in request.GET:
            context[ContextKeys.SORT] = request.GET[HttpParameters.SORT]
        if HttpParameters.SORT_ORDER in request.GET:
            context[ContextKeys.SORT_ORDER] = request.GET[HttpParameters.SORT_ORDER]

        context["url_params"] = {
                "view": "admin classifications",
                "params": [
                    self.year.url_id(),
                    self.org.url_id()
                ]
            }


        context["page_title"] = "Administrators"
        # All query parameters that influence paging, these will be saved by
        context["page_state_keys"] = Administrators.PAGE_STATE_KEYS


        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        context = self.context(request)
        return render(request, "administrator/4.0-Administrators.html", context)



