'''
Created on Aug 17, 2012

@author: msn
'''
from timetables.administrator.views import AdminBaseView
from django.shortcuts import render
from timetables.model.models import Classification, ClassifierGroup,\
    Permission
from django.http import HttpResponseBadRequest, HttpResponseForbidden

import logging
from timetables.model.backend import PermissionSubject
log = logging.getLogger(__name__)
del(logging)

class EditClassifications(AdminBaseView):
    '''
    Renders the EditClassifications page
    '''

    def js_main_module(self):
        return "admin/classifications-edit"

    def context(self, request, classification, based_on=None):
        context = super(EditClassifications, self).context(request, based_on=based_on)

        # Populate the edit page with the values entered last time round:
        context["page_title"] = "Edit Classification"
        context["classification_id"] = classification.id
        context["classification_name"] = classification.name
        context["classification_contact"] = classification.contact_person
        context["classification_description"] = classification.description
        context["classification_website"] = classification.website
        context["organisation"] = {
                    "classifications" : {
                        "levels" : list((ClassifierGroup.objects.for_organisation(self.org)[0]).classifier_set.order_by('value')),
                        "subjects" : list((ClassifierGroup.objects.for_organisation(self.org)[1]).classifier_set.order_by('value')),
                            }
                    }
        context["subject_name"] = ClassifierGroup.objects.get(organisation_id=self.org,family='S').name

        # Pull out all the selected subjects / levels:
        if classification.classifier is not None:
            if classification.classifier.parent is not None:
                context['subject_selected'] = classification.classifier
                context['level_selected'] = classification.classifier.parent
            else:
                context['subject_selected'] = None
                context['level_selected'] = classification.classifier
        else:
            context['subject_selected'] = None
            context['level_selected'] = None
            

        return context

    def get(self, request, year_start, org_code, classification_id):
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE_CLASSIFICATIONS, PermissionSubject(self.org)):
            return HttpResponseForbidden()

        # Grab the classification object for the given classification_id
        # Exit with an error if the classification is not found!
        classification = None
        try:
            classification = Classification.objects.get(id=classification_id, owner_id=self.org)
        except:
            return HttpResponseBadRequest("Classification does not exist!")

        # Pass the classification object through to populate the context with:
        context = self.context(request, classification)

        return render(request, "administrator/2.2-Edit-Classification.html",
                context)
