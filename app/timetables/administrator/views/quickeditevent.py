'''
Created on Aug 1, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseNotFound, HttpResponseForbidden
from timetables.model.models import Event, User, Location, Permission

import logging
from timetables.model.backend import PermissionSubject
log = logging.getLogger(__name__)
del logging

class QuickEditEvent(AdminBaseView):
    '''
    Perform a quickedit on one or more events.
    '''



    def js_main_module(self):
        return None

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        # POST contains,
        # lecturers (comma separated list), title, location and event-id
        # Should update the event.
        modified = False
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()
        try:
            event = Event.objects.get(id=request.POST['event-id'],
                                      owning_series__owning_group__owning_organisation=self.org)
        except Event.DoesNotExist:
            return HttpResponseNotFound()
        location = request.POST['location'].strip()
        if event.owning_series.location.name == location:
            if event.overridden_location is not None:
                event.overridden_location = None
                modified = True
                log.error("Location Reset to series")
            else:
                log.error("Series location matches")
        else:
            if event.overridden_location != location:
                location, _ = Location.objects.get_or_create(name=request.POST['location'])
                event.overridden_location = location
                modified = True
                log.error("Event location set to %s " % event.overridden_location)
            else:
                log.error("Event location matches")


        title = request.POST['title']
        if event.owning_series.title == title:
            if event.overridden_title is not None:
                event.overridden_title = None
                modified = True
                log.error("Serise Title Reset")
            else:
                log.error("Series title matches")

        else:
            if event.overridden_title != title:
                event.overridden_title = title
                modified = True
                log.error("Event Title set to %s " % event.overridden_title)
            else:
                log.error("Event title matches")




        overridden_staff = ""
        if event.is_staff_overridden:
            staff_s = set()
            for s in event.overridden_staff.all():
                staff_s.add(s.username)
            overridden_staff=",".join(sorted(staff_s))

        staff_s = set()
        for s in event.owning_series.associated_staff.all():
            staff_s.add(s.username)
        staff = ",".join(sorted(staff_s))

        staff_update_s = set([ s.strip() for s in request.POST['lecturers'].split(',') ])
        staff_update = ",".join(sorted(staff_update_s))

        if staff == staff_update:
            if len(overridden_staff) > 0:
                event.is_staff_overridden = False
                event.overridden_staff.clear()
                modified = True
                log.error("Staff Reset to series")
            else:
                log.error("Series staff matches")
        else:
            if overridden_staff != staff_update:
                event.is_staff_overridden = True
                event.overridden_staff.clear()
                for s in staff_update_s:
                    user, _ = User.objects.get_or_create(username=s)
                    event.overridden_staff.add(user)
                modified = True
                log.error("Event staff set to %s " % staff_update_s)
            else:
                log.error("Event staff matches")
        updated = 0
        if modified:
            event.save()
            log.error("Saved event %s " % event.id)
            updated = 1


        return self.prepare_response(request, updated=updated)

