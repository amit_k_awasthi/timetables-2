'''
Created on Aug 20, 2012

@author: msn
'''
from timetables.administrator.views import AdminBaseView
from timetables.model.models import Classifier, EventGroupVersion, EventType,\
    GroupUsageVersion, Revision, Term, ClassifierGroup
from django.shortcuts import render
from django.http import HttpResponseBadRequest

class ChangeHistoryRevisionDetails(AdminBaseView):
    '''
    Show the details of a single revision.
    '''

    def js_main_module(self):
        return "admin/changehistory-revision-details"

    def context(self, request, revision, based_on=None):
        '''
        Configure the context to show the details of a single revision in the change history.
        :param request:
        :param revision:
        :param based_on:
        '''
        context = super(ChangeHistoryRevisionDetails, self).context(request, based_on=based_on)

        # Set page title:
        context["page_title"] = "Revision " + str(revision.id) + " Details"

        # Push the revision object into the context:
        context["revision"] = revision

        # Fetch the version of the event group for the given revision id and push to context:
        event_group_version = EventGroupVersion.objects.get(revision_id=revision.id)
        context['event_group_version'] = event_group_version

        # Fetch the version of the group usage object correlating to the version of the event group:
        group_usage_version = GroupUsageVersion.objects.get(organisation_id=self.org,group_id=event_group_version.id)

        # Pull out all the selected subjects / levels for the versioned group usage object:
        context["organisation"] = {
                "classifications" : {
                        "subjects" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org)).order_by("value"),
                        "levels" : Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                             group__organisation=self.org)).order_by("value"),
                },
                "types" : EventType.objects.all().order_by("name"),
                "terms" : Term.objects.filter(term_info__day=False,
                                              academic_year=self.year,
                                              term_info__term_type='S').order_by("term_info__name").select_related("terminfo"),
        }
        context['subjects_selected'] = group_usage_version.classifiers.all().filter(group__family='S')
        context['levels_selected'] = group_usage_version.classifiers.all().filter(group__family='L')
        context["subject_name"] = ClassifierGroup.objects.get(organisation_id=self.org,family='S').name

        # Push to the context the information needed for displaying the group / series details:
        context["group"] = event_group_version
        context["group_id"] = event_group_version.id
        context["new_group"] = False
        # the ID of the embedded versioned series
        context["series_add_id"] = "n_0"
        context["new_series"] = True
        context['recent_changes'] = Revision.objects.filter(grouprevision__group_version__current_version=event_group_version).order_by("-id").select_related("user","organisation")

        # Set defaults for the group settings:
        context["default_group_title"] = ''
        context["default_group_desc"] = ''
        context["default_group_default_location"] = ''
        context["default_group_default_date_time_pattern"] = ''

        return context

    def get(self, request, year_start, org_code, revision_number):
        self.locate_organisation(request, year_start, org_code)

        # Grab the revision object for the given revision_number
        # Exit with an error if the revision is not found!
        revision = None
        try:
            revision = Revision.objects.get(id=revision_number, organisation_id=self.org)
        except:
            return HttpResponseBadRequest("Revision does not exist!")

        # Push the revision object through to populate the context:
        context = self.context(request, revision)

        return render(request, "administrator/3.2-Revision-Details.html",
                context)
