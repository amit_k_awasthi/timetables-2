'''
Created on Aug 20, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseBadRequest, HttpResponseForbidden
from django.utils import simplejson as json

import logging
from timetables.model.models import Permission
from timetables.model.backend import PermissionSubject
log = logging.getLogger(__name__)
del logging

class SaveExternalUrlView(AdminBaseView):
    '''
    Save an external event with URL.
    '''

    def js_main_module(self):
        return None

    @method_decorator(xact)
    def post(self, request, year_start, org_code):

        target_url = None
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE, PermissionSubject(self.org)):
            return HttpResponseForbidden()

        # Parse submitted POST parameters:
        try:
            pass
        except KeyError:
            return HttpResponseBadRequest()
        
        log.error("No idea how to save this at the moment")
        log.error(json.dumps(request.POST, indent=4))


        return self.prepare_response(request, target_url=target_url)

