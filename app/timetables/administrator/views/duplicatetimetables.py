'''
Created on Sep 12, 2012

@author: ieb
'''
from timetables.administrator.views import AdminBaseView
from django.utils.decorators import method_decorator
from timetables.utils.xact import xact
from django.http import HttpResponseBadRequest, \
    HttpResponse, HttpResponseForbidden, HttpResponseNotFound
from timetables.model.models import EventGroup, GroupUsage, Classifier,\
    Permission, AcademicYear, Organisation, OrganisationRole, ClassifierGroup,\
    Classification, EventSeries, Role, Term
import logging
from timetables.model.backend import PermissionSubject
from timetables.utils.v1 import publish
from django.core.urlresolvers import reverse


log = logging.getLogger(__name__)

class DuplicateTimetables(AdminBaseView):
    '''
    Perform the duplicate timetables operaiton.
    '''
    

    def js_main_module(self):
        return None
    

    @method_decorator(xact)
    def post(self, request, year_start, org_code):
        '''
        Duplicate a faculty.
        This is a bit of an expesive operation. A typical duplication will results in about 12K SQL statements
        taking anything between 20s and a few minutes to complete.
        :param request:
        :param year_start:
        :param org_code:
        '''
        self.locate_organisation(request, year_start, org_code)
        if not request.user.has_perm(Permission.WRITE_ADMINISTRATORS, PermissionSubject(self.org)):
            return HttpResponseForbidden()

        if "year" not in request.POST:
            return HttpResponseBadRequest()
        try:
            year = AcademicYear.objects.get(id=request.POST['year'])
        except AcademicYear.DoesNotExist:
            return HttpResponseNotFound("Academic year does not exist")
        # starting from the organization, find everything connected to it and copy it to a new year.
        
        # check this organization is not attached to the year
        try:
            Organisation.objects.get(name=self.org.name,year=year)
            return HttpResponse("Organisation already exists in year", status=409)
        except Organisation.DoesNotExist:
            pass
        
        
        neworg = Organisation.objects.create(
                        name=self.org.name,
                        year=year,
                        code="%s%s" % (year.starting_year,self.org.id))
        
        # Duplicate roles.
        for orgrole in OrganisationRole.objects.filter(organisation=self.org):
            newrole = Role.objects.get_role_or_create(orgrole.role.permissions.all())
            neworgrole, _ = OrganisationRole.objects.get_or_create(
                                user=orgrole.user,
                                role=newrole,
                                organisation=neworg)
        # Duplicate classifer
        for cg in ClassifierGroup.objects.filter(organisation=self.org):
            newcg, _ = ClassifierGroup.objects.get_or_create(
                                            name=cg.name,
                                            organisation=neworg,
                                            family=cg.family)
            for c in Classifier.objects.filter(group=self.org):
                classification = None
                try:
                    if c.classification is not None:
                        classification = Classification.objects.create(
                                                        name=c.classification.name,
                                                        website=c.classification.website,
                                                        description=c.classification.description,
                                                        contact_person=c.classification.contact_person,
                                                        owner=c.classifcation.owner)
                    newc, _ = Classifier.objects.get_or_create(value=c.value, group=newcg, classification=classification)
                except Classification.DoesNotExist:
                    pass

        # Duplicate Event Groups, based on owning organisation not usage.
        for eg in EventGroup.objects.filter(owning_organisation=self.org):
            try:
                newterm = Term.objects.get(
                                        term_info=eg.term.term_info,
                                        academic_year=year)
            except Term.DoesNotExist:
                # This is a weird term, generate it, approximating the date.
                newterm, _ = Term.objects.get_or_create(
                                        term_info=eg.term.term_info,
                                        academic_year=year,
                                        start=eg.term.start.replace(year=year.starting_year),
                                        end=eg.term.end.replace(year=year.starting_year))
                
            neweg = EventGroup.objects.create(
                                    name=eg.name,
                                    publication_status='U',
                                    owning_organisation=neworg,
                                    code="%s%s" % (neworg.code, eg.id),
                                    term=newterm,
                                    description=eg.description,
                                    default_location=eg.default_location,
                                    event_type=eg.event_type)
            for u in GroupUsage.objects.filter(group=eg,organisation=self.org):
                newu, _ = GroupUsage.objects.get_or_create(
                                        organisation=neworg,
                                        group=neweg)
                for cl in u.classifiers.all():
                    newclg, _ = ClassifierGroup.objects.get_or_create(
                                            name=cl.group.name,
                                            organisation=neworg,
                                            family=cl.group.family)
                    newcl, _ = Classifier.objects.get_or_create(
                                            group=newclg,
                                            value=cl.value)
                    newu.classifiers.add(newcl)
                newu.save()
            for es in eg.owned_series.all():
                newes = EventSeries.objects.create(
                                        title=es.title,
                                        date_time_pattern=es.date_time_pattern,
                                        location=es.location,
                                        external_url=es.external_url,
                                        owning_group=neweg)
                for staff in es.associated_staff.all():
                    newes.associated_staff.add(staff)
                newes.usages.add(neweg)
                newes.save()
                publish(newes)

        target_url = reverse("admin overview", args=(neworg.year.url_id(), str(neworg.url_id()) ))

        return self.prepare_response(request, target_url=target_url)
