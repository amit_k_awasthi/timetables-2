from django.conf.urls.defaults import patterns, include, url
import timetables.views
from timetables.superadministrator.views.forms import FormView

# Enable Django's admin interface
from django.contrib import admin
from timetables.utils.repo import RepoView
from django.views.decorators.csrf import csrf_exempt
admin.autodiscover()

urlpatterns = patterns('',
    # Placeholder for links to places which don't yet exist
    url(r'TODO', timetables.views.todo, name="TODO"),

    # Django admin interface (NOT timetables administrators)
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Timetables administrator interface
    url(r'^administration/', include('timetables.administrator.urls')),
    url(r'^superadministration/', include('timetables.superadministrator.urls')),

    # Timetables utility urls:
    url(r'^utils/', include('timetables.utils.urls')),

    url(r'^forms/([\w\-]+)/$',
            FormView.as_view(),
            name="forms"),

    # This has to be csrf exempt. Look at the view to see what it does.
    url(r'repo/(?P<key>.*)', csrf_exempt(RepoView.as_view()), name="REPO"),
    url(r'^', include('timetables.student.urls')),
)
