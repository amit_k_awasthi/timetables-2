from django.http import HttpResponseBadRequest
from django.shortcuts import render
from django.views.generic import View
from django.utils.html import escape
import re

class FormView(View):
    prefix_pattern = re.compile("^[\w-]+$")
    _registered_forms = {}

    @classmethod
    def register_form(cls, name, formfactory,
            template="superadministrator/form.html"):
        assert isinstance(name, basestring), (
                "name should be a string: %s" % name)
        cls._registered_forms[name] = (formfactory, template)

    def get(self, request, formname):
        if not formname in FormView._registered_forms:
            return HttpResponseBadRequest(
                    """<div>No such form: %s</div>""" %
                    escape(formname))

        prefix = request.GET.get("prefix", None)
        if prefix and not FormView.prefix_pattern.match(prefix):
            return HttpResponseBadRequest("Prefix contained illegal chars: %s" %
                    prefix)

        formfactory, template = FormView._registered_forms[formname]
        forminstance = formfactory(prefix=prefix)
        return render(request, template, {"form": forminstance})
