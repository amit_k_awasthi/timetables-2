"""This module contains the views for the administrator related screens.

- Administrator list
- Add Administrators
- Edit Administrators
"""

import types
import logging
from django.utils.timezone import now
import traceback
from django.utils.decorators import method_decorator
log = logging.getLogger(__name__)
del(logging)

from itertools import chain, groupby

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django import forms
from django.forms import formsets
from django.core.urlresolvers import reverse
from django.db.models import Q

from timetables.utils.xact import xact
from timetables.superadministrator.views import SuperadminBaseView
from timetables.utils import TimetablesPaginator
from timetables.model.models import (User, AcademicYear, Organisation,
    ClassifierGroup, Permission, OrganisationRole)
from timetables.utils.forms import ProgressiveForm, extractor, TimetablesFormset
from timetables.utils.http import HttpParameters, ContextKeys
from timetables.superadministrator.views.forms import FormView
from django.core.exceptions import PermissionDenied

class CustomLabelFromInstance(object):
    """A ModelChoiceField which allows a custom attribute on the choice models
    to be used as the label displayed to users.

    Provide a value for the model_label_field keyword param when instantiating.
    """
    def __init__(self, *args, **kwargs):
        self.model_label_field = kwargs.pop("model_label_field", "__unicode__")
        super(CustomLabelFromInstance, self).__init__(*args, **kwargs)

    def label_from_instance(self, model_instance):
        val = getattr(model_instance, self.model_label_field)
        if isinstance(val, types.FunctionType):
            return val()
        return val

class CustomLabelModelMultipleChoiceField(CustomLabelFromInstance,
        forms.ModelMultipleChoiceField):
    pass

class CustomLabelModelChoiceField(CustomLabelFromInstance,
        forms.ModelChoiceField):
    pass

class OrganisationAffiliationForm(ProgressiveForm):
    """This form is the repeatable block in Add/Edit admin which associates an
    admin with an organisation.
    """
    error_css_class = "error"
    required_css_class = "required"

    ADMIN_TYPES = (
        ("normal", "Faculty Admin"),
        ("key", "Key Faculty Admin"),
    )
    PERMISSION_LEVELS = (
        ("read", "Read Only"),
        ("read-write", "Read / Write"),
        ("read-write-publish", "Read / Write / Publish"),
        ("read-write-publish-classify",
                "Read / Write / Publish / Add New Classifications"),
    )
    model_id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    year = forms.ModelChoiceField(queryset=AcademicYear.objects.active_years(),
            required=True)
    organisation = CustomLabelModelChoiceField(
            label="Faculty",
            model_label_field="name",
            queryset=Organisation.objects.none(), required=True)
    type_of_admin = forms.ChoiceField(
            choices=ADMIN_TYPES, required=True,
            initial="normal",
            widget=forms.RadioSelect(
                    attrs={"class": "list-horizontal"}),
            label="Type of Admin")
    permission = forms.ChoiceField(choices=PERMISSION_LEVELS, required=True,
            widget=forms.RadioSelect,
            initial="read",
            # "Timetables" is what the wireframe says
            label="Timetables")

    can_add_admins = forms.BooleanField(label="Can add & edit administrators",
            required=False)

    @classmethod
    def state_extractors(cls):
        def extract_year(form):
            if not "year" in form.cleaned_data:
                return None
            return {"year": form.cleaned_data["year"]}

        def extract_organisation(form):
            if not "organisation" in form.cleaned_data:
                return None
            return {"organisation": form.cleaned_data["organisation"]}

        return [extractor(f) for f in [extract_year, extract_organisation]]

    def __init__(self, *args, **kwargs):
        initial = kwargs.get("initial", {})
        year = kwargs.pop("year", initial.get("year"))
        organisation = kwargs.pop("organisation", initial.get("organisation"))
        super(OrganisationAffiliationForm, self).__init__(*args, **kwargs)

        organisation_qs = Organisation.objects.filter(year=year) if year else \
                Organisation.objects.none()
        self.fields["organisation"].queryset = organisation_qs
        self.fields["organisation"].instance = initial.get("organisation")
        self.fields["year"].instance = initial.get("year")

        self._classifiers = {}
        if organisation is not None:
            items = []
            for group in (ClassifierGroup.objects
                        .prefetch_related("classifier_set")
                        .filter(organisation=organisation)):

                initial_classifiers = None
                if organisation == initial.get("organisation"):
                    initial_classifiers = initial.get("classifiers", {}).get(group)

                field = self._mk_classifier_field(group, initial=initial_classifiers)
                # Use the group.family field to create the dictionary index
                items.append(("classifiers-%s" % group.family, field))

            self._classifiers = dict(items)
            self.fields.update(self._classifiers)

        self.selected_year = None
        self.selected_organisation = None

    def clean_model_id(self):
        # Default to the initial model_id if there is one
        return self.initial.get("model_id") or self.cleaned_data["model_id"]

    @property
    def classifiers(self):
        classifiers = []
        # We sort the keys in the dictionary self._classifiers so
        # that levels(L) are retrieved before subjects(S). This translates
        # to levels being rendered to the left of subjects, which
        # heirarchically makes sense. If changes are made to classifier
        # group families, sorting may no longer be sufficient to order
        # the classifier retrieval and something more specific than 
        # alphabetical sorting may be required here:
        for classifier in sorted(self._classifiers):
            log.debug('Found classifier index: ' + classifier )
            # Fetch item from dictionary and append to classifiers list:
            classifiers.append(self[classifier])
        return classifiers

    def clean(self):
        # affiliation wide validation goes here
        return super(OrganisationAffiliationForm, self).clean()

    def _mk_classifier_field(self, classifiergroup, initial=None):
        return CustomLabelModelMultipleChoiceField(
                model_label_field="value",
                label=classifiergroup.name,
                queryset=classifiergroup.classifier_set.all(),
                widget=forms.CheckboxSelectMultiple,
                initial=initial,
                required=False)

    @classmethod
    def _permissions_for_level(cls, level):
        perms = set()
        if "read" in level:
            perms.add(Permission.objects.admin_read())
        if "write" in level:
            perms.add(Permission.objects.admin_write())
        if "publish" in level:
            perms.add(Permission.objects.admin_publish())
        if "classify" in level:
            perms.add(Permission.objects.admin_write_classifications())
        return perms

    def save(self, admin):
        assert isinstance(admin, User)
        data = self.cleaned_data
        org_role_id = data.get("model_id", None)

        if data.get("DELETE", False):
            if org_role_id:
                role = OrganisationRole.objects.get(id=org_role_id)
                if role.user != admin:
                    raise PermissionDenied
                role.delete()
        else:
            permissions = self._permissions_for_level(data["permission"])
            if data["type_of_admin"] == "key":
                permissions.add(Permission.objects.admin_key_admin())
            if data["can_add_admins"]:
                permissions.add(Permission.objects.admin_write_administrators())

            # Associate the admin with the organisation, granting all permissions
            # from the permissions set. 
            org_role = User.make_admin(admin, data["organisation"], permissions)
            assert org_role.id

            if org_role_id and org_role_id != org_role.id:
                OrganisationRole.objects.get(id=org_role_id).delete()

            # Build a flat list of all classifiers specified across each 
            # multi-select in the form
            classifier_lists = (list(v) for k, v in data.items()
                    if k.startswith("classifiers-"))
            classifiers = list(chain.from_iterable(classifier_lists))
            # Add each classifier (we shouldn't use bulk_create methods as they 
            # won't fire signals used by the validation code).
            org_role.classifiers.clear()
            org_role.classifiers.add(*classifiers)

    @classmethod
    def initial_from_orgrole(cls, orgrole):
        permissions = set(orgrole.role.permissions.all())
        can_add_admins = (Permission.objects.admin_write_administrators()
                in permissions)
        is_key_admin = (Permission.objects.admin_key_admin() in permissions)


        level = 0
        perms = [Permission.objects.admin_read(),
                Permission.objects.admin_write(),
                Permission.objects.admin_publish(),
                Permission.objects.admin_write_classifications()]
        for perm in perms:
            if not perm in permissions:
                break
            level += 1

        classifiers = dict(
                # eagerly evaluate the groupby group into a list (otherwise it
                # is a on use iterator)
                ((key, list(group)) for key, group in
                        # Group classifiers in to a list under their group 
                        groupby(orgrole.classifiers.order_by("group"),
                                lambda cg: cg.group)))

        return {
            "model_id": orgrole.id,
            "year": orgrole.organisation.year,
            "organisation": orgrole.organisation,
            "type_of_admin": "key" if is_key_admin else "normal",
            "can_add_admins": can_add_admins,
            "permission": {
                0: None,
                1: "read",
                2: "read-write",
                3: "read-write-publish",
                4: "read-write-publish-classify"
            }[level],
            "classifiers": classifiers
        }

    @classmethod
    def initial_list_from_user(cls, user):
        return [cls.initial_from_orgrole(orgrole) for orgrole in
                OrganisationRole.objects.filter(user=user)]

OrganisationAffiliationFormSetAdd = formsets.formset_factory(
        OrganisationAffiliationForm.progressively_load, extra=1,
        formset=TimetablesFormset)
OrganisationAffiliationFormSetEdit = formsets.formset_factory(
        OrganisationAffiliationForm.progressively_load, extra=0,
        can_delete=True, formset=TimetablesFormset)

def org_affiliation_factory_factory(formsetcls):
    def org_affiliation_factory(prefix=None):
        form = formsetcls().empty_form
        form.prefix = prefix
        return form
    return org_affiliation_factory
FormView.register_form("organisation-affiliation-add",
        org_affiliation_factory_factory(OrganisationAffiliationFormSetAdd),
        "superadministrator/fragments/faculty_affiliation_form.html")
FormView.register_form("organisation-affiliation-edit",
        org_affiliation_factory_factory(OrganisationAffiliationFormSetEdit),
        "superadministrator/fragments/faculty_affiliation_form.html")

class BaseAdminForm(forms.Form):
    error_css_class = "error"
    required_css_class = "required"

    crsid = forms.SlugField(label="CRSid", max_length=100)
    is_superadmin = forms.BooleanField(label="Super Admin", required=False)
    meta_scroll_offset = forms.IntegerField(
            required=False, widget=forms.HiddenInput)
    meta_was_auto_submitted = forms.BooleanField(
            required=False, widget=forms.HiddenInput)

    def __init__(self, **kwargs):
        super(BaseAdminForm, self).__init__(**kwargs)
        data = kwargs.get("data")

        initial = kwargs.get("initial") or {}
        self._affiliations = self.formset_class()(data=data,
                initial=initial.get("affiliations", None))

    @property
    def affiliations(self):
        return self._affiliations

    def is_valid(self):
        return (super(BaseAdminForm, self).is_valid() and
                self.affiliations.is_valid() and
                not self.cleaned_data["meta_was_auto_submitted"])

    def clean(self):
        return self.cleaned_data

    @classmethod
    def _user_from_crsid(cls, crsid):
        assert False, "Override in subclass"

    @method_decorator(xact)
    def save(self):
        admin_form = self.cleaned_data

        # Create a user object. Note that this fails if the same username 
        # already exists.
        admin = self._user_from_crsid(self.cleaned_data["crsid"])

        # Grant superadmin status if requested
        if User.is_superadmin(admin):
            if not admin_form["is_superadmin"]:
                User.strip_superadmin(admin)
        else:
            if admin_form["is_superadmin"]:
                User.make_superadmin(admin)

        if admin_form['is_disabled'] and not User.is_disabled(admin):
            User.disable_admin(admin)
        if not admin_form['is_disabled'] and User.is_disabled(admin):
            User.enable_admin(admin)

        # Add/Update the affiliations
        for affiliation in self.affiliations.valid_forms:
            affiliation.save(admin)
        admin.save()

class AddAdminForm(BaseAdminForm):

    def __init__(self, **kwargs):
        super(AddAdminForm, self).__init__(**kwargs)
        self.fields["crsid"].validators.append(self.validate_crsid)

    def clean(self):
        if (self.affiliations.is_valid() and
                not (self.cleaned_data["is_superadmin"] or
                    self.affiliations.valid_forms)):
            raise forms.ValidationError("An administrator must be associated "
                    "with at least one Faculty, or be a super admin.")
        return super(AddAdminForm, self).clean()

    @classmethod
    def validate_crsid(cls, crsid):
        try:
            cls._user_from_crsid(crsid)
        except RuntimeError:
            raise forms.ValidationError("This user is already an administrator,"
                    " edit them instead.")

    @classmethod
    def _user_from_crsid(cls, crsid):
        # We only let people "add" admins if they are not already superadmins
        # or affiliated with any org
        user, created = User.objects.get_or_create(username=crsid)
        # For the administration section we'll need to parameterise the call to 
        # is_admin to check for the active organisation.
        if not created and (User.is_superadmin(user) or User.is_admin(user)):
            raise RuntimeError("Attempted to add an existing user.")
        return user

    @classmethod
    def formset_class(cls):
        return OrganisationAffiliationFormSetAdd

class EditAdminForm(BaseAdminForm):

    def __init__(self, crsid, **kwargs):
        super(EditAdminForm, self).__init__(**kwargs)
        self._crsid = crsid
        # If the form is being used in an edit (rather than add) screen we need
        # to allow the admin to be disabled.
        self.fields["is_disabled"] = forms.BooleanField(label="Disable Admin",
                required=False)
        self.fields["crsid"].widget.attrs["readonly"] = True

    def clean_crsid(self):
        # Ensure that we ignore any crsid value in the POST as it's readonly
        return self._crsid

    @method_decorator(xact)
    def save(self):
        # Delete any org roles marked for deletion (if they exist in the db)
        for affiliation in self.affiliations.deleted_forms:
            # Ignore deleted forms which were created during the edit session 
            # (i.e. no db model exist)
            if not "model_id" in affiliation.initial:
                continue
            role = OrganisationRole.objects.get(
                    id=affiliation.initial["model_id"],
                    user__username=self._crsid)
            role.delete()

        return super(EditAdminForm, self).save()

    @classmethod
    def formset_class(cls):
        return OrganisationAffiliationFormSetEdit

    @classmethod
    def initial_from_user(cls, user):
        """Calculates a dict of initial values to use for the provided user."""

        return {
            "crsid": user.username,
            "is_disabled": User.has_disabled_perm(user),
            "is_superadmin": User.is_superadmin(user),
            "affiliations": OrganisationAffiliationForm.initial_list_from_user(
                    user)
        }

    @classmethod
    def _user_from_crsid(cls, crsid):
        # The user must already exist
        return User.objects.get(username=crsid)

class Administrators(SuperadminBaseView):
    PAGE_STATE_KEYS = (
                    ( HttpParameters.SEARCH_QUERY, ContextKeys.SEARCH_QUERY),
                    )

    # ORDER_FIELDS has the format of one or more of the following:
    #   'column name' : 'field to sort by'
    ORDER_FIELDS = {
        'name' : 'last_name',
        'crsid' : 'username',
        'email' : 'email'
        }

    # Include the Javascript for administrators:
    def js_main_module(self):
        return "superadmin/administrators"

    # Number of administrators to list per page:
    results_per_page = 20

    # Return a query set of administrators, optionally
    # filtered by a search term and / or sorted:
    @classmethod
    def list_administrators(cls, request, with_sort=True,
            initial_queryset=None):
        querySet = (initial_queryset if initial_queryset is not None
                else User.objects.all_admins())
        # Filter down results to those that match a passed in query. This is done by excluding
        # those entries in the query set for whom none of their searched columns match the
        # query parameter passed in:
        if "q" in request.GET:
            querySet = querySet.exclude(~Q(username__icontains=request.GET['q'].lower()),
                                        ~ Q(first_name__icontains=request.GET['q'].lower()),
                                        ~ Q(last_name__icontains=request.GET['q'].lower()),
                                        ~ Q(email__icontains=request.GET['q'].lower()))
        # Apply sorting 
        if with_sort:
            if "s" in request.GET and request.GET['s'] in Administrators.ORDER_FIELDS:
                ordering = Administrators.ORDER_FIELDS[request.GET["s"]]
                if "o" in request.GET and request.GET['o'] == 'd':
                    ordering = "-%s" % ordering
                querySet = querySet.order_by(ordering)
        return querySet

    def context(self, request):
        context = super(Administrators, self).context(request)

        querySet = self.list_administrators(request)
        pages = TimetablesPaginator(querySet,
                Administrators.results_per_page)
        page = pages.page_safe(request.GET.get(HttpParameters.PAGE, 1))

        context["administrators"] = page
        context["page_title"] = "Administrators"

        # pass sort query and order to the template:
        if HttpParameters.SEARCH_QUERY in request.GET:
            context[ContextKeys.SEARCH_QUERY] = request.GET[HttpParameters.SEARCH_QUERY]
        if HttpParameters.SORT in request.GET:
            context[ContextKeys.SORT] = request.GET[HttpParameters.SORT]
        if HttpParameters.SORT_ORDER in request.GET:
            context[ContextKeys.SORT_ORDER] = request.GET[HttpParameters.SORT_ORDER]

        # All query parameters that influence paging, these will be saved by
        context["page_state_keys"] = Administrators.PAGE_STATE_KEYS

        return context

    def get(self, request):
        context = self.context(request)

        return render(request, "superadministrator/1.0-Administrators.html",
                               context)

class AddAdministrators(SuperadminBaseView):

    def js_main_module(self):
        return "superadmin/administrators-add"

    def context(self, request):
        context = super(AddAdministrators, self).context(request)
        context["page_title"] = "Add New Administrator"
        return context

    @method_decorator(xact)    
    def post(self, request):
        form = AddAdminForm(data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                    reverse("superadmin administrators"))
        return self.get(request)

    def get(self, request):
        context = self.context(request)
        try:
            org = Organisation.objects.get(id=request.GET["yid"])
            form = AddAdminForm(data={
                            "form-TOTAL_FORMS" : "1",
                            "form-INITIAL_FORMS" : "0",
                            "form-0-year" : org.year.id,
                            "form-0-organisation" : org.id
                            })
        except:
            form = AddAdminForm()

        context["admin_form"] = form
        return render(request,
                "superadministrator/1.1-Administrators-Add.html", context)


class EditAdministrators(SuperadminBaseView):

    def js_main_module(self):
        return "superadmin/administrators-edit"

    def context(self, request, username):
        context = super(EditAdministrators, self).context(request)

        context["page_title"] = "Edit Administrator"
        context["editing_user"] = get_object_or_404(User, username=username)

        return context
    
    @method_decorator(xact)
    def post(self, request, username):
        context = self.context(request, username)
        user = context["editing_user"]
        initial = EditAdminForm.initial_from_user(user)
        form = EditAdminForm(user, data=request.POST, initial=initial)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                    reverse("superadmin administrators"))
        return self.get(request, username)

    def get(self, request, username):
        context = self.context(request, username)
        user = context["editing_user"]
        initial = EditAdminForm.initial_from_user(user)
        form = EditAdminForm(user, initial=initial)
        context["admin_form"] = form
        return render(request,
                "superadministrator/1.2-Administrators-Edit.html", context)

